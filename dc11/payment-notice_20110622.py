# -*- encoding: utf-8 -*-

import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message

body = """\
%(name)s,

%(msg_header)s

If you would like your attendee fee to be pro-rated, or a sponsored or
discount DebCamp, please contact the registration team.  The
Professional fee is designed to be the actual cost per week, and we
are happy to reduce the fee if you will stay for fewer days.

For payment information, please see
  http://debconf11.debconf.org/payments.xhtml

Payment items:
%(items)s

Total amount to pay: € %(total_cost)d

Amount paid so far:  € %(amount_paid)d

==========

The registration system URL is
  https://penta.debconf.org/penta/submission/dc11/person

If you have any questions or concerns, please contact us via
IRC:                  #debconf-team @ irc.oftc.net (real-time questions)
or mailing list:      debconf-team@lists.debconf.org (publicly archived)
or non-public email:  registration@debconf.org

Thank you,

--
The DebConf team
(person_id: %(person_id)4d)
"""

header_not_yet_paid = """\
According to our records, you registered for DebConf11 as a corporate
or professional attendee, so we are expecting a payment from you.

** Please pay as soon as possible, but no later than Friday 1 July, as
** we must be able to see the payment by the reconfirmation deadline."""

header_paid_already = """\
According to our records, you registered for DebConf11 as a corporate
or professional attendee, and have paid already.  Information on your
payment is below."""



People = dc_objs.get_people(where="dcvn.attend")

mbox = mailbox.mbox("dc11-paymentnotice_20110622.mbox")
mbox.clear() ; mbox.flush()

for P in People:

    if P.amount_to_pay is None:
        P.amount_to_pay = P.get_total_cost()

    #if not P.amount_to_pay \
    #   or P.amount_to_pay == P.amount_paid:
    #    continue
    if not P.amount_to_pay:
        continue

    if P.amount_paid and P.amount_paid >= P.amount_to_pay:
        P.msg_header = header_paid_already
    else:
        P.msg_header = header_not_yet_paid

    P.total_cost = P.get_total_cost()
    if P.amount_paid is None:
        P.amount_paid = 0

    items = [ ]
    for item in P.payment_items():
        items.append("    € %4d %s"%(item[1], item[0]))
    P.items = "\n".join(items)

    Body = body % P.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(P.name, P.email),
        Subject="DebConf11: Payment information",
        Body=Body,
        extraMsgid="dc11paymentnotice1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
