# Richard Darst, April 2010

import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


beginning = """\
Hello %(name)s,

Your DebConf11 registration confirmation is below.  Please verify the
information below and fix anything which is wrong.

%(mail_banner)s\
\
There is an accompanying announcement message with more information:
  http://wiki.debconf.org/wiki/DebConf11/Announcements/2011-06-15

If you have any questions, please contact us via
IRC:                  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:      debconf-team@lists.debconf.org (public list)
or non-public email:  registration@debconf.org

The registration system URL is
  https://penta.debconf.org/penta/submission/dc11/person

** If you make any changes, log out and back in to verify that your
** changes were committed!
"""

ending = """\
We look forward to seeing you soon in Banja Luka, 

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body = """%(beginning)s

Information about your registration
===================================

%(info)s

Registration errors and warnings
================================

%(errors)s


%(ending)s
"""


# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
People = dc_objs.get_people(where='dcvn.attend')
[ P.add_errors(errorcheck.Errors) for P in People ]
People = [ P for P in People if len(P.errors) > 0 ]
mbox = mailbox.mbox("dc11reconfirm-20110629.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    Errors = [e for e in person.errors
              if e.__class__.__name__ not in ('RegistrationCheck',
                                              'ThirteenYearOldAgeLimit',
                                              'DoubleCheckTravelSponsorshipNumbers',
                                              'NewlyEnabledFieldsInfo') ]
    Errors = [e for e in Errors
              if 'travel' not in e._class ]
    person.errors = Errors

    if len(Errors) > 0:
        print '='*40
        print person.person_id, person.name
        print person._info()


        
    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    # Don't send this person email if they have no errors.
    #importantErrors = [ e for e in AllErrors
    #                    if (e.level!='info' and \
    #                        e.__class__.__name__ not in
    #                          ('NoRoomPreference',
    #                           'NoCountry',
    #                           ))]

    #if len(importantErrors) == 0:
    #    continue
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    person.mail_banner = ""
    if not person.reconfirmed:
        person.mail_banner = """\
** You must reconfirm and provide the required information by 3 July
** or your registration will be canceled.  Please don't wait and do it
** TODAY.

"""


    Body = body%locals()
    Body = Body%person.__dict__
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf11 registration - reconfirmation or information needed",
        Body=Body,
        extraMsgid="dc11rc2",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
