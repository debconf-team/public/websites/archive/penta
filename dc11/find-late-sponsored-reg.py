# Richard darst, june 2010


import pentaconnect
db = pentaconnect.get_conn()
c = db.cursor()

c.execute("""\
select
    participant_mapping_id, person_type, participant_category
from
    debconf.dc_view_participant_map
;
""")
mapping = { }
for pm_id, status, category in c.fetchall():
    mapping[int(pm_id)] = status, category

c.execute("""\
select
    log_timestamp,
    ldcp.person_id as person_id,
    dcvn.name as name,
    dc_participant_category_id,
    llt.person_id as changer_id
from
    log.dc_conference_person ldcp
    left join log.log_transaction llt using (log_transaction_id)
    left join debconf.dc_view_numbers dcvn on (ldcp.person_id=dcvn.id)
where
    conference_id = 5
order by llt.log_timestamp asc
;
""")
import collections
people = collections.defaultdict(lambda : {'category': '' })
for line in c.fetchall():
    (d, p_id, name, pm_id, c_id) = line
    try:
        status, category = mapping[pm_id]
    except KeyError:
        print pm_id, sorted(mapping.keys())
    #if category is None: continue

    # Bypass people not changed by themselves - admins can do what
    # they want.
    #print repr(p_id), repr(c_id), p_id == c_id
    #if p_id != c_id:
    #    continue

    p = people[p_id]
    #if 'sponsored' in category.lower() \
    #   and 'sponsored' not in p['category'].lower():
    if p['category'] != category and 'sponsored' in category.lower():
        print d[:19], p_id, name, '(%d)'%c_id, p['category'], '->', category
    p['category'] = category
