# -*- encoding: utf-8 -*-

import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message

body = """\
%(name)s,

%(msg_header)s

For payment information, please see
  http://debconf11.debconf.org/payments.xhtml

Payment items:
%(items)s

Total amount to pay:  EUR %(total_cost)d

Amount paid so far:   EUR %(amount_paid)d

==========

The registration system URL is
  https://penta.debconf.org/penta/submission/dc11/person

If you have any questions or concerns, please contact us via
IRC:                  #debconf-team @ irc.oftc.net (real-time questions)
or mailing list:      debconf-team@lists.debconf.org (publicly archived)
or non-public email:  registration@debconf.org

Thank you,

--
The DebConf team
(person_id: %(person_id)4d, r:%(reconfirmed)s)
"""

header_not_yet_paid = """\
According to our records, you registered for DebConf11 as a corporate
or professional attendee, so we are expecting a payment from you.  (If
you have already contacted us about payment, that isn't indicated
here)

**                                                                       **
** The payment deadline is 1 July or your accommodation may be canceled. **
**                                                                       **

* If you can not pay by *1 July*, please contact the registration team
  ASAP.

* If you do not want to be registered in a paying category, or don't
  want to attend at all please correct your registration in Pentabarf
  or contact us.

If you would like your attendee fee to be pro-rated, or a sponsored or
discount DebCamp, please contact the registration team.  The
Professional fee is designed to be the actual cost per week, and we
are happy to reduce the fee if you will stay for fewer days."""

header_paid_already = """\
According to our records, you have *paid already*.  Information on
your payment is below.  (If this is the second time you are getting
this, we apologize.)"""



People = dc_objs.get_people(where="dcvn.attend")

mbox = mailbox.mbox("dc11-paymentnotice_20110629.mbox")
mbox.clear() ; mbox.flush()

for P in People:

    # Skip people who have already paid
    if P.amount_to_pay <= P.amount_paid:
        continue
    # Skip people who don't need to pay
    if not P.amount_to_pay:
        continue
    # Skip people who have promised to pay (or made other arrangements)
    if P.promised_to_pay:
        continue

    # Message header
    if P.amount_paid and P.amount_paid >= P.amount_to_pay:
        P.msg_header = header_paid_already
    else:
        P.msg_header = header_not_yet_paid

    # Set substitution variables for the text
    P.total_cost = P.amount_to_pay
    if P.amount_paid is None:
        P.amount_paid = 0

    items = [ ]
    for item in P.payment_items():
        items.append("    EUR %4d %s"%(item[1], item[0]))
    P.items = "\n".join(items)

    Body = body % P.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(P.name, P.email),
        Subject="DebConf11: Payment information",
        Body=Body,
        extraMsgid="dc11pmt2",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
