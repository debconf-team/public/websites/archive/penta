# Richard Darst, April 2010

import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


beginning = """\
Hello %(name)s,

Your current DebConf11 registration status is below.  Please read the
personalized information below and fix anything which is wrong.

** You must reconfirm and provide the required information by 3 July
** or your registration will be canceled.  Please don't wait and do it
** TODAY.

There is an accompanying announcement message with more information:
  http://wiki.debconf.org/wiki/DebConf11/Announcements/2011-06-15

If you have any questions, please contact us via
IRC:
  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

The registration system URL is

  https://penta.debconf.org/penta/submission/dc11/person

** If you make any changes, log out and back in to verify that your
** changes were committed!
"""

ending = """\
We look forward to seeing you soon in Banja Luka, 

-- 
The DebConf team
"""

body = """%(beginning)s

Information about your registration
===================================

%(info)s

Registration errors and warnings
================================

%(errors)s


%(ending)s
"""


# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
People = dc_objs.get_people(where='true')
[ P.add_errors(errorcheck.Errors) for P in People ]
People = [ P for P in People if len(P.errors) > 0 ]
mbox = mailbox.mbox("dc11reconfirm.mbox")

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    Errors = [e for e in person.errors
              if e.__class__.__name__ not in ('RegistrationCheck',
                                              'ThirteenYearOldAgeLimit',
                                              'DoubleCheckTravelSponsorshipNumbers',
                                              'NewlyEnabledFieldsInfo') ]
    Errors = [e for e in Errors
              if 'travel' not in e._class ]
    person.errors = Errors

    if len(Errors) > 0:
        print '='*40
        print person.person_id, person.name
        print person._info()

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    Body = body%locals()
    Body = Body%person.__dict__
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf11 registration status",
        Body=Body,
        extraMsgid="dc11reconfirm1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
