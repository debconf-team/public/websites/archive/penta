# Richard Darst, June 2010

"""
Run with
- len(sys.argv) < 2: Run, don't update last state.
- sys.argv[1]=='update'  --  if you don't do this, it won't save people.last.pickle
- sys.argv[1]=='new' -- don't try to load people.last.pickle, AND create people.last.pickle
- sys.argv[1]=='diff' -- diff sys.argv[2] and sys.argv[3] pickle files.
"""

import time
import pickle
import StringIO
import sys
sys.path.append('.')

import dc_objs
import dc_config

dirname = '/home/pentaread/saved-data/%s/'%dc_config.dcN

if len(sys.argv) < 2:
    mode = 'normal'
else:
    mode = sys.argv[1]

if mode in ('normal', 'update'):
    f = open(dirname+'people.last.pickle')
    oldPeople = pickle.load(f)
    f.close()
elif mode == 'diff':
    f = open(sys.argv[2])
    oldPeople = pickle.load(f)
    f.close()
else:
    oldPeople = [ ]
oldPeople_d = { }
for p in oldPeople:
    oldPeople_d[p.person_id] = p

#sys.stdout = StringIO.StringIO()

if mode != 'diff':
    people = dc_objs.get_people(where='true')
elif mode == 'diff':
    f = open(sys.argv[3])
    people = pickle.load(f)
    f.close()
else:
    raise Exception("Unknown mode")

def summary(p):
    print "Attend:        ", p.attend
    print "Reconfirmed:   ", p.reconfirmed
    print "Category:      ", p.participant_category
    print "Debian-role:   ", p.debian_role
    print "DebConf-role:  ", p.debconf_role
    print "DebCamp:       ", p.debcamp
    print "Food:          ", p.food
    print "Food select:   ", p.food_select
    print "Accommodation: ", p.accom
    if p.camping:
        print "-----Preferred camping", p.camping
    print "Arrival-date:  ", p.arrival_date
    print "Departure-date:", p.departure_date
    print "Room preferences:", p.room_preference
    if p.disabilities:
        print "*** Disabilities: *** ", p.disabilities
def differences(pOld, p):
    lines = []
    for attrname in ('attend',
                     'reconfirmed',
                     'participant_category',
                     'debconf_role',
                     'debcamp',
                     'food',
                     'food_select',
                     'accom',
		     'camping',
                     'arrival_date',
                     'departure_date',
                     'disabilities',
		     'room_preference'):
        if not hasattr(pOld, attrname):
            continue
        if getattr(pOld, attrname, None) != getattr(p, attrname):
            lines.append("--> %-14s: %s -> %s"%(attrname,
                                                getattr(pOld, attrname),
                                                getattr(p, attrname)))
    return lines


for p in people:
    if p.person_id not in oldPeople_d:
        print "NEW PERSON:"
        print p.personHeader()
        summary(p)
        print "\n\n"+"="*20
        continue
    pOld = oldPeople_d[p.person_id]
    diff = differences(pOld, p)
    if len(diff) > 0:
        print p.personHeader()
        summary(p)
        print "\n".join(diff)
        print "\n\n"+"="*20
        continue

# Do we need to look for deleted people?  No, you can't delete them
# from the table.


if mode in ('update', 'new'):
    f = open(dirname+'people.last.pickle', 'w')
    pickle.dump(people, f)
    f.close()
f = open(dirname+'people.%s.pickle'%time.strftime("%F.%T"), 'w')
pickle.dump(people, f)
f.close()
