# Richard Darst, October 2009

# A script to print out a pretty text-based listing of all talks.

import pentaconnect
import dc_objs

db = pentaconnect.get_conn()

# Get all speakers names
c2 = db.cursor()
# Now, what's the speaker's name?
c2.execute("""select name, event_id
from event_person ep
     join view_person p using (person_id) join event e using (event_id)
where conference_id=%d and event_role='speaker'
order by last_name;""" % dc_objs.conference_id)
people = c2.fetchall()
c2.close()

c = db.cursor()
# Optionally add: where event_state='accepted'
c.execute("""select title, subtitle, conference_track, event_type, event_id,
                 abstract
             from view_event
             where conference_id=%d and translated='en'
                 and event_state='accepted'
             order by event_type,conference_track,title""" % dc_objs.conference_id)

old_track = None
old_type = None
for title, subtitle, c_track, e_type, e_id, abstract in c.fetchall():
    if e_type != old_type:
        #print "  %s"%e_type
        print "\n\n\n****** %s\n"%e_type
        old_type = e_type
    if c_track != old_track:
        print "\n\n***", c_track
        old_track = c_track
    if len(title) < 30 and subtitle:
        #print "    * %s: %s"%(title, subtitle)
        print "%s: %s"%(title, subtitle)
    else:
        #print "    * %s"%title
        print "%s"%title
    # Now, what's the speaker's name?
    # (SQL is too slow over the net to execute a new query each time)
    speakers = [ x[0] for x in people if x[1]==e_id ]
    if len(speakers) == 0 or not ("".join(speakers)).strip():
        # No listed speakers
        print '<no speakers listed>'
    else:
        #print "      %s"%(', '.join(speakers))
        print "%s"%(', '.join(speakers))
    if abstract and isinstance(abstract, (str,unicode)) and abstract.strip():
        print abstract
    else:
        #print '<no abstract>'
        pass
    print
    print
    

c.close()
