# Richard Darst, June 2010

import csv
import os
import sys

import dc_objs

WHERE = os.environ.get('WHERE', 'dcvn.attend')
people = dc_objs.get_people()

if len(sys.argv) > 1:
    fname = sys.argv[1]
else:
    fname = "attendee-data.csv"

f = open(fname, "w")
csvfile = csv.writer(f)

def gender(p):
    if p.gender is True: return "male"
    if p.gender is False: return "female"
    return ""

fields = ("person_id",
          "name",
          "email",
          "status",
          "participant_category",
          "debcamp",
          "reconfirmed",
          "food",
          "accom",
          "arrival_date",
          "arrival_time",
          "departure_date",
          "departure_time",
          "room_preference",
          "country",
          )
csvfile.writerow(fields + ('gender', ))
for person in people:
    row = [ getattr(person, name) for name in fields  ]
    row.append(gender(person)),
    csvfile.writerow(row)


