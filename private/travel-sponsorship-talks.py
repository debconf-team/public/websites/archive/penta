# Richard Darst, May 2010
import collections
import time

import dc_objs
import dc_util
import pentaconnect

db=pentaconnect.get_conn()
query="""
select
    person_id, event_id
from view_event_person ep
where event_role='speaker'
    and translated='en'
"""
c=db.cursor()
c.execute(query)
rows = dc_util.rowToDict(c.fetchall(), c)
speakers_d = collections.defaultdict(list)
for row in rows:
    speakers_d[row['person_id']].append(row['event_id'])

people = dc_objs.get_people()
talks = dc_objs.get_talks()
talks_d = dict((t.event_id, t) for t in talks)

print time.ctime()
print


people = [ p for p in people if p.need_travel_cost ]
people = sorted(people, key=lambda p: p.mean_rating, reverse=True)
for person in people:
    print "="*20
    print "(%(person_id)4d) %(name)s"%person.__dict__
    print "Rating:  %3d"%person.mean_rating
    print
    if person.person_id in speakers_d:
        for event_id in speakers_d[person.person_id]:
            if event_id in talks_d:
                talk = talks_d[event_id]
                print "(%(event_id)3d) %(title)s  (rating: %(rating)3d)"%talk.__dict__
    print
    print
    print

    
#from fitz import interactnow
