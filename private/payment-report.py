# Richard Darst, April 2010

import os
import time

import dc_objs
import dc_config

WHERE = os.environ.get('WHERE', 'dcvn.attend')
People = dc_objs.get_people(where=WHERE)

total_cost = sum(P.get_total_cost() for P in People)
conference_id = dc_config.conference_id

print time.ctime()
print
print "Limit:", WHERE
print "Only including food costs in the upper report."

print "Total attendee fees:", total_cost
print
print
print


#import parse_attendee_payments

#payments = parse_attendee_payments.get_payments()

#for P in People:
#    if P.person_id in payments:
#        P.amount_paid = payments[P.person_id]['amount_paid']
#    if P.amount_to_pay is None:
#        P.amount_to_pay = P.get_total_cost()

def formatnum(x, fmt, width=7):
    if x is None:
        return " "*len(fmt%0)
    else:
        return fmt%x

def print_people(People):
    not_yet_paid = [ ]
    paid_up_people = [ ]
    for P in People:
        if not hasattr(P, 'amount_paid'):
            print "empty amount_paid", P.person_id
            P.amount_paid = 0
            
        if P.amount_paid == P.amount_to_pay:
            paid_up_people.append(P)
        else:
            not_yet_paid.append(P)

    _print_people(not_yet_paid)
    print
    print "Has paid:"
    print
    _print_people(paid_up_people)



def _print_people(People):
    for P in People:
        amount_paid = P.amount_paid
        amount_to_pay = P.amount_to_pay
        print P.regSummary(), \
              formatnum(amount_paid, "%7.2f") if not P.promised_to_pay else "   P   ", \
              formatnum(amount_to_pay, "%7s"), \
              "  ", P.personHeader()

# Categorize people into types:

has_payment = [ ]
no_payment = [ ]
for P in People:
    if P.amount_to_pay == 0 or P.amount_to_pay is None:
        no_payment.append(P)
    else:
        has_payment.append(P)

print "="*40
print """\
Has a payment due.

These people have amount_to_pay nonzero.  The first block is
amount_to_pay != amount_paid (mount_paid comes from the spreadsheet).
In this case, all of these people probably need to make a payment.
Sometimes, this is caused by other errors, such as amount_to_pay being
set wrong.  Due to the logic, if someone pays too much, they also
appear here, just for error-checking purposes.  If there is a 'P'
under the amount_paid column, that means they have a promise on
record.
"""
print_people(has_payment)
print
print


print "="*40
print """\
No payment people.

These people have amount_to_pay as zero.
"""
print_people(no_payment)
print
print


print "\n"*10


### Update amount_to_pay
print "="*40
print """\
Validating amount_to_pay.

Step two is figuring out if amount_to_pay is set correctly.  The
section ignores the amount_to_pay field in Penta, calculates how much
they should pay based on their registration (Prof, Corp, pay-by-night,
if we are doing that this year), and then if that value doesn't match
penta's amount_to_pay, then print them both.  This lets you check the
cases where amount_to_pay is set wrong (unless a special deal has been
made, but this checks that).  You can use the 'Remarks' box at the top
of Penta to make notes, that will show up here.
"""
calculated_total_fees = 0.
penta_total_fees = 0.
for P in People:
    # skip people who have null amount_to_pay and don't need to pay.
    if (P.amount_to_pay is None or P.amount_to_pay == 0 ) \
           and P.get_total_cost(nocustom=True) == 0:
        continue
    # Now, amount_to_pay should match.
    if P.amount_to_pay != P.get_total_cost(nocustom=True):
        print "="*20
        print P.personHeader()
        print P.regSummary()
        print "Current-amount_to_pay:   ", P.amount_to_pay
        print "Calculated-amount_to_pay:", P.get_total_cost(nocustom=True)
        print "Calculated-Items:"
        for item in P.payment_items(nocustom=True):
            print "    %3d %s"%(item[1], item[0])
        print "amount_paid:             ", P.amount_paid
        if P.promised_to_pay:
            print "    P   promised to pay"
        if P.remark:
            print "Notes:", P.remark

        print ("UPDATE debconf.dc_conference_person set "
               "amount_to_pay='%d' where person_id=%d and "
               "conference_id='%d' ; "%(P.get_total_cost_noFood(), P.person_id,
                                        conference_id))
        calculated_total_fees += P.get_total_cost(nocustom=True)
        if P.amount_to_pay is not None:
            penta_total_fees += P.amount_to_pay
        print
        print
print "="*20
print "calculated sum(amount_to_pay):", calculated_total_fees
print "penta sum(amount_to_pay):     ", penta_total_fees

print "\n"*10

############

print "="*40
print """\
Payment itemization.

This itemizes payments for all recipients, as per above.
"""
print

for P in People:
    if P.amount_to_pay == 0 or P.amount_to_pay is None:
        continue
    print "="*20
    print P.personHeader()
    print P.regSummary()
    for item in P.payment_items():
        print "    %3d %s"%(item[1], item[0])
    print
    print "    %3s %s"%(formatnum(P.amount_paid, "%3d"), 'amount_paid')
    if P.promised_to_pay:
        print "    P   promised to pay"

    print


