# Richard Darst, April 2010

import csv
import os
import sys
import time

import dc_objs
import dc_util

WHERE = os.environ.get('WHERE', 'dcvn.attend AND dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)

def formatnum(x, fmt, width=7):
    if x is None:
        return " "*len(fmt%0)
    else:
        return fmt%x

def _print_people(People):
    for P in People:
        amount_paid = 0 # P.amount_paid
        amount_to_pay = P.total_costs # P.amount_to_pay
        print P.regSummary(), \
		"  ", P.personHeader()
#              formatnum(amount_paid, "%7.2f") if not P.promised_to_pay else "   P   ", \
#              formatnum(amount_to_pay, "%7s"), \
#              "  ", P.personHeader()


#People = [p for p in People if
#          p.cat_spons or p.cat_pro or p.cat_cor) ]
People = [ p for p in People if p.accom_yes ]

People.sort(key=lambda p: p.last_name)

_print_people(People)

if len(sys.argv) > 1:
    fname = sys.argv[1]
else:
    fname = "reg-data.csv"
f = open(fname, "w")
csvfile = csv.writer(f)

csvfile.writerow(('person_id',
                   'name',
                   'email',
		   'arrival_date',
	           'departure_date',
                   'participant_category',
                   'debcamp',
                   'reconfirmed',
                   'food',
		   'food_select',
                   'accom',
                   'camping',
                   'com_accom',
                   'country',
                   'gender',
		   'room_preference',
                   ))


# people with "yes"
for person in People:
    csvfile.writerow((person.person_id,
                      person.name,
                      person.email,
                      person.arrival_date,
                      person.departure_date,
                      person.participant_category,
                      person.debcamp,
                      person.reconfirmed,
                      person.food,
		      person.food_select,
                      person.accom,
                      person.camping,
                      person.com_accom,
                      person.country,
                      dc_util.gender(person),
		      person.room_preference,
                      ))


