# Richard Darst, June 2010


import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message

People = dc_objs.get_people()
[ P.add_errors(errorcheck.Errors) for P in People ]
#People = [ P for P in People if len(P.errors) > 0 ]


for person in People:

    [ e.email() for e in person.errors ]

    Errors = [e for e in person.errors
              if e.__class__.__name__ not in ('NoRoomPreference',
                                              'NoArrivalDate',
                                              'NoDepartureDate') ]
    Errors = [e for e in Errors
              if 'travel' not in e._class ]
    Errors = [e for e in Errors
              if e.level not in ('debug', 'info', 'warn') ]
    person.errors = Errors

    if len(Errors) == 0:
        continue
    print person.personHeader()
    print person._info()

    print "\n\n"+"="*20
