# Richard Darst, April 2010

import collections
import datetime
import pgdb
import time

import dc_util
import dc_objs

Talks = dc_objs.get_talks()

for talk in Talks:
    abstract = dc_util.wrap_text(talk.abstract)
    desc = dc_util.wrap_text(talk.description)
    submmission_notes = dc_util.wrap_text(talk.submission_notes)
    
    print '='* 20
    if talk.subtitle:
        print "%s: %s"%(talk.title, talk.subtitle)
    else:
        print talk.title
    print "(%s, %s)"%(talk.event_id, talk.event_type)
    print "State:", talk.event_state, talk.event_state_progress
    if talk.conference_track: print "Conference-track:", talk.conference_track
    for speakertype, speakers in sorted(talk.people.iteritems()):
        print "%s:"%speakertype, ", ".join(s[1] for s in speakers)
    #print "Submitters:", ", ".join(submitters[talk['event_id']])
    #print "Speakers:", ", ".join(speakers[talk['event_id']])
    print
    print abstract
    print
    print desc
    print
    print
    print
