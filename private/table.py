# Richard Darst, June 2012

import os
import string
import sys

import dc_objs
import dc_config

conference_id = int(os.environ.get('C_ID', dc_config.conference_id))

#import pentaconnect
#
#db = pentaconnect.get_conn()
#c = db.cursor()

if len(sys.argv) > 1:
    people = [int(x) for x in sys.argv[-1].split(',')]
else:
    people = [ 1310 ]

def sanitize(s):
    return "".join(x for x in s if x in string.ascii_letters+string.digits+'_'+'.')
if len(sys.argv) > 2:
    query = sys.argv[1:-1]
else:
    query = ['person_id', 'name', 'participant_category', 'debcamp', 'accom']
#queryFields = [ sanitize(x) for x in query ]
#queryFields = ", ".join(queryFields)

People = dc_objs.get_people()
rows = [ ]
for p in People:
    if p.person_id not in people:
        continue
    rows.append([getattr(p, name) for name in query])

rowwidths = [ max(len(str(x)) for x in column) for column in zip(*rows) ]
for row in rows:
    for i, item in enumerate(row):
        print str(item).rjust(rowwidths[i]), "  ",
    print
