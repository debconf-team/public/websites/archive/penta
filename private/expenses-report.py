
import csv
import sys

import dc_objs

if len(sys.argv) > 1:
    f = open(sys.argv[1], 'w')
else:
    f = sys.stdout

people = dc_objs.get_people()
people = (p for p in people if (p.need_travel_cost or p.cantfund or p.total_cost))
people = sorted(people, key=lambda p: p.mean_rating, reverse=True)

csvfile = csv.writer(f)
#csvfile = csv.DictWriter(f, 'id name email total_cost cantfund contributor_level requested_amount mean_rating remarks'.split())
#csvfile.writeheader()
csvfile.writerow('id name email need_travel_cost total_cost cantfund contributor_level requested_amount mean_rating remarks'.split())
for p in people:
    csvfile.writerow((p.person_id,
                      p.name,
                      p.email,
                      p.need_travel_cost,
                      "%1.2f"%p.total_cost if p.total_cost is not None else 'None',
                      "%1.2f"%p.cantfund if p.cantfund is not None else 'None',
                      "%1.2f"%p.contributor_level,
                      "%1.2f"%p.requested_amount,
                      "%1.2f"%p.mean_rating,
                      ";; ".join(p.ratings_remarks)))



