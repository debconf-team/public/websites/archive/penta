# Richard Darst, June 2010

import collections
import time

import dc_util
import dc_objs
#import pentaconnect
#db = pentaconnect.get_conn()
#c = db.cursor()

where = 'dcvn.attend'

import dc_objs
People = dc_objs.get_people(where=where)

print time.ctime()
print

for P in People:
    if P.arrival_date is not None and P.departure_date is not None \
           and P.reconfirmed == True:
        continue

    print "="*20
    print "(%4d) %s <%s>"%(P.person_id, P.name, P.email)
    print "Status:     ", P.status
    print "Category:   ", P.participant_category
    print "Accom:      ", P.accom
    print "Attend:     ", P.attend
    print "Reconfirmed:", P.reconfirmed
    print "Dates:       %s - %s"%(P.arrival_date, P.departure_date)
    if True: # debcamp status (blocked for ease of reading only)
        if P.debcamp in dc_objs.DC_NOPLAN:
            print "DebCamp:     No workplan"
        if P.debcamp in dc_objs.DC_PLAN:
            print "DebCamp:     Has workplan"
        else:
            print "DebCamp:    ", P.debcamp
    print
    print
    
