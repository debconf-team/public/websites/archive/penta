# Richard Darst, May 2010

# Some sample queries
"""
select log_timestamp, ldccp.person_id, participant_category
from log.log_transaction llt
    left join log.dc_conference_person ldccp using (log_transaction_id)
    left join debconf.dc_view_participant_map dcvpm
        on (dc_participant_category_id=participant_mapping_id)
where (ldccp.person_id=1557) and conference_id=4
order by log_timestamp desc;

select log_timestamp, tablename.*
from log.log_transaction llt
left join TABLE_NAME tablename
    using (log_transaction_id)
where (lcp.person_id=person_id1557)
order by log_timestamp desc;

"""


import os
import string
import sys

import dc_config

conference_id = int(os.environ.get('C_ID', dc_config.conference_id))

import pentaconnect

db = pentaconnect.get_conn()
c = db.cursor()

if len(sys.argv) > 1:
    people = [int(x) for x in sys.argv[-1].split(',')]
else:
    people = [ 1310 ]
queryPeople = ["(ldccp.person_id=%d or cp_cpt.person_id=%d)"%(p,p) for p in people ]
queryPeople = "(" + " or ".join(queryPeople) + ")"

def sanitize(s):
    return "".join(x for x in s if x in string.ascii_letters+string.digits+'_'+'.')
if len(sys.argv) > 2:
    req_query = sys.argv[1:-1]
else:
    req_query = ['lcp.reconfirmed', 'attend', 'participant_category', 'debcamp_option', 'accom', 'food', 'dcfs.description']
queryFields = [ sanitize(x) for x in req_query ]
queryFields = ", ".join(queryFields)


query = """\
select
    log_timestamp,
    ldccp.person_id,
    llt.person_id as changer_id,
    FIELDS
from log.log_transaction llt
    full join log.conference_person lcp using (log_transaction_id)
    full join log.dc_conference_person ldccp using (log_transaction_id)
    left join debconf.dc_view_participant_map dcvpm
        on (dc_participant_category_id=participant_mapping_id)
    left join debconf.dc_debcamp using (debcamp_id)
    left join debconf.dc_accomodation using (accom_id)
    left join debconf.dc_food_preferences using (food_id)
    left join debconf.dc_food_select dcfs on (food_select=food_select_id)
    full join log.conference_person_travel lcpt
        using (log_transaction_id)
    left join conference_person cp_cpt
            on (lcpt.conference_person_id = cp_cpt.conference_person_id)
    left join debconf.dc_debian_role dcdr using (debian_role_id)
    left join debconf.dc_debconf_role dcdcr using (debconf_role_id)
where (ldccp.conference_id=%(conference_id)s or cp_cpt.conference_id=%(conference_id)s )
    and WHERE
order by log_timestamp desc;
""".replace('WHERE', queryPeople).replace('FIELDS', queryFields)
#print query
c.execute(query, {'conference_id':conference_id})
#print c.fetchall()
#            on (ldccp.debian_role_id = dcdr.debian_role_id)
#            on (ldccp.debconf_role_id = dcdcr.debconf_role_id)

#print [x[0] for x in c.description ]
for line in c.fetchall():
    #print line
    if not line[1]:
	continue
    print "%s (person_id: %4s, changed_by: %4s)"%(line[0][:19], line[1], line[2])
    i = 3
    for item in line[3:]:
        print " "*5, c.description[i][0], ":", str(item)
        i += 1

