# Giacomo Catenazzi, July 2012

import os
import sys
import time
import datetime

import dc_config
from dc_config import *
import dc_objs
import dc_util

WHERE = os.environ.get('WHERE', 'dcvn.attend and dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)

### ROOMS ###
#============

def parse_date(x):
    dt = datetime.datetime.strptime(x, '%Y-%m-%d')
    return dt.date()

# persons[id] -> [ (hotel, room, arrive, depart) ]
room_persons = {}
room_person_name = {}
# rooms[hotel+":"+room] -> [ (person_id, arrive, depart) ]
rooms = {}


def parse_spreadsheet(fname):
    import readsxc
    f = readsxc.OOSpreadData(fname)

    names = None
    numpeople = 0
    for row in f:
        # Automatically create columns
        if not names:
            names = [ n.lower().replace(' ', '_').replace('-', '_')
                      for n in row ]
            continue
        if not row or row[0].lower() == "end" or (not row[0] and not row[1]):
            break
        numpeople += 1
        # Row is a dict of row labels
        row = dict(zip(names, row))
        #print row
        #print row['person_id']

        # Ignore empty names (free bed)
        if not row.has_key('name') and not row.has_key('person_id'):
            continue
        person_id = int(row['person_id'])

        if not room_persons.has_key(person_id):
            room_persons[person_id] = []
            room_person_name[person_id] = row['name']

        # Get room information
        roomName = row['room']  # row['room_alloc']
        if roomName.startswith('n--'):
            continue

        roomNameCamp = row['roomcamp']

        hotelName = "DebConf"
        hotelNameCamp = "DebCamp"

        if roomNameCamp:
            room_persons[person_id].append( (hotelNameCamp, roomNameCamp, arrive, min(dc_objs.DEBCONF_START, depart)) )
            if not rooms.has_key(hotelNameCamp + ":" + roomNameCamp):
                rooms[hotelNameCamp + ":" + roomNameCamp] = []
            rooms[hotelNameCamp + ":" + roomNameCamp].append( (person_id, arrive, min(dc_objs.DEBCONF_EARLIEST_ARRIVAL, depart)) )

        room_persons[person_id].append( (hotelName, roomName, max(arrive, dc_objs.DEBCONF_EARLIEST_ARRIVAL), depart) )
        if not rooms.has_key(hotelName + ":" + roomName):
            rooms[hotelName + ":" + roomName] = []
        rooms[hotelName + ":" + roomName].append( (person_id, max(arrive, dc_objs.DEBCONF_EARLIEST_ARRIVAL), depart) )

    print "People processed:", numpeople
    return

#parse_spreadsheet("/home/cate/room6.ods")




today_date = datetime.date.today()
tomorrow_date = today_date + datetime.timedelta(days=1)


missings = []
today_arrivals = []
tomorrow_arrivals = []
today_departures = []
tomorrow_departures = []


def cat(person):
    self = person
    if    self.food_reg:   food = "REG"
    elif  self.food_veg:   food = "VEG"
    elif  self.food_vegan: food = "VEGAN"
    elif  self.food_other: food = "OTHER"
    elif  self.food_no:    food = "NOFOOD"
    else:                  food = "FOOD_ERROR"

    if   self.accom_sponsored:  accom = "Sponsored-Communal"
    elif self.accom_communal:   accom = "Paid-Communal"
    elif self.accom_camping:    accom = "Paid-Camping"
    elif self.accom_eight:      accom = "paid<=8"
    elif self.accom_two:        accom = "paid<=2"
    elif self.accom_upgrade:    accom = "upgrade<=4"
    elif self.accom_no:         accom = "no-accommodation"
    else:                       accom = "ACCOMMODATION_ERROR"
    
    return "%s / %s" % (food, accom)

def print_person(person):
    return "%12s %s (%s-%s) %s\n" % (
        person.arrival_time,
	person.personHeader(),
        person.arrival_date_d,
        person.departure_date_d,
        cat(person)
    )


# check people

for p in People:
    arrival_date, departure_date = p.effective_dates()
    
    if arrival_date < today_date and not p.arrived:
        missings.append(p)
    elif arrival_date == today_date:
        today_arrivals.append(p)
    elif arrival_date == tomorrow_date:
        tomorrow_arrivals.append(p)

    if departure_date == today_date:
        today_departures.append(p)
    elif departure_date == tomorrow_date:
        tomorrow_departures.append(p)

# build report

msg = "Arrival and departure report for %s\n\n" % today_date.strftime("%Y-%m-%d")

msg += "\n=== missing people ===\n\n"
for p in missings:
    msg += print_person(p)

msg += "\n=== today arrivals ===\n\n"
for p in today_arrivals:
    msg += print_person(p)

msg += "\n=== tomorrow arrivals ===\n\n"
for p in tomorrow_arrivals:
    msg += print_person(p)

msg += "\n=== today departures ===\n\n"
for p in today_departures:
    msg += print_person(p)

msg += "\n=== tomorrow departures ===\n\n"
for p in tomorrow_departures:
    msg += print_person(p)

print msg


