# Giacomo Catenazzi, March 2014, based on Richard Darst scripts

usage = """\
usage:
   python penta/private/penta_dump.py <mode> <field>...
 or
   python penta/private/penta_dump.py

 Without arguments the program gives the list of mode and fields.
 With arguments the program gives a csv list (with header) on stdout.
 Optional WHERE environmental variable could be used to filter results

 Available mode:
   cp: conference person  # links also to 'p'
   p: person              # 
   ep: event person	   # links also to 'e' and 'p' and 'cp'
   e: event               #

 for available field (with type and constrain), please look down in the code
"""

import sys
import os

import dc_config
import pentaconnect


bs = {False: 'n', True: 'y', None: ''}

def from_integer(key):
    if key:
        return '%i' % key
    else:
        return ''
def from_numeric(key):
    if key:
        return '%f' % key
    else:
        return ''
def from_dict(dictionary, key):
    return dictionary.get(key, "")
def from_boolean(key):
    return from_dict(bs, key)
def from_text(key):
    "long text. Possibly quoted"
    if key:
        return '"%s"' % key
    else:
        return '""'
def from_str(key):
    "short string"
    if key:
        return '%s' % key
    else:
        return ''
def from_date(key):
    if key:
        return '%s' % key
    else:
        return ''
def from_time(key):
    if key:
        return '%s' % key
    else:
        return ''
def from_interval(key):
    if key:
        return '%s' % key
    else:
        return ''



fields = {
# conference_person cp
'cp.conference_person_id':         from_integer , # not null default nextval
'cp.conference_id':                from_integer , # not null  -> public.conference c
'cp.person_id':                    from_integer , # not null  -> public.person p
'cp.abstract':                     from_text    , # [ description.abstract. used in dc7, dc8, dc9 ]
'cp.description':                  from_text    , #
'cp.remark':                       from_text    , #
'cp.email':                        from_text    , # [ prefer p.email ]
'cp.arrived':                      from_boolean , # not null default false
'cp.reconfirmed':                  from_boolean , # not null default false

# public.person p
'p.person_id':                     from_integer , # not null default nextval
'p.title':                         from_text    , # [ prepended to names, like "Mr." ]
'p.gender':                        from_boolean , #
'p.first_name':                    from_text    , #
'p.last_name':                     from_text    , #
'p.public_name':                   from_text    , #
'p.nickname':                      from_text    , #
'p.email':                         from_text    , #
'p.spam':                          from_boolean , # not null default false
'p.address':                       from_text    , #
'p.street':                        from_text    , #
'p.street_postcode':               from_text    , #
'p.po_box':                        from_text    , #
'p.po_box_postcode':               from_text    , #
'p.city':                          from_text    , #
'p.country':                       from_text    , #
'p.iban':                          from_text    , #
'p.bic':                           from_text    , #
'p.bank_name':                     from_text    , #
'p.account_owner':                 from_text    , #

# debconf.dc_conference_person dccp
'dccp.person_id':                  from_integer , # not null
'dccp.conference_id':              from_integer , # not null
'dccp.accom_id':                   from_integer , # -> dca.accom_id
'dccp.daytrip_id':                 from_integer , # -> debconf.dc_daytrip dcdt
'dccp.computer_id':                from_integer ,
'dccp.assassins':                  from_boolean , # not null default false
'dccp.public_data':                from_boolean , # not null default false
'dccp.wireless':                   from_boolean , # default false
'dccp.badge':                      from_boolean , # not null default false
'dccp.foodtickets':                from_boolean , # not null default false
'dccp.nsh':                        from_boolean , # not null default false
'dccp.paid_amount':                from_text    , #
'dccp.dc7_paid_for_days':          from_integer , #
'dccp.cancelled':                  from_boolean , # not null default false [used only in dc7]
'dccp.bag':                        from_boolean , # not null default false
'dccp.shirt':                      from_boolean , # not null default false
'dccp.hostel':                     from_text    , #
'dccp.proceeded':                  from_boolean , #
'dccp.paiddaytrip':                from_boolean , # not null default false
'dccp.googled':                    from_boolean , # not null default false
'dccp.drunken':                    from_boolean , # not null default false
'dccp.gotdaytrip':                 from_boolean , # not null default false
'dccp.proceedings':                from_boolean , #
'dccp.t_shirt_sizes_id':           from_integer , #
'dccp.food_id':                    from_integer , #
'dccp.attend':                     from_boolean , # not null default false
'dccp.person_type_id':             from_integer , #
'dccp.dc_participant_category_id': from_integer , #
'dccp.debcamp_id':                 from_integer , # not null default 1
'dccp.debcamp_reason':             from_text    , #
'dccp.travel_to_venue':            from_boolean , # not null default false
'dccp.travel_from_venue':          from_boolean , # not null default false
'dccp.room_preference':            from_text    , #
'dccp.has_paid':                   from_boolean , # not null default false
'dccp.amount_to_pay':              from_text    , #
'dccp.has_to_pay':                 from_boolean , # not null default false
'dccp.debianwork':                 from_text    , #
'dccp.disabilities':               from_boolean , # not null default false
'dccp.photo_or_film_ok':           from_boolean , # not null default false # not used: prefer: dcp.photo_or_film_ok
'dccp.debconf_role_id':            from_integer , #
'dccp.debian_role_id':             from_integer , #
'dccp.coffee_mug':                 from_boolean , # not null default false
'dccp.has_sim_card':               from_boolean , # not null default false
'dccp.debcampdc13':                from_boolean , # not null default false
'dccp.camping':                    from_boolean , # not null default false
'dccp.com_accom':                  from_boolean , #
'dccp.food_select':                from_integer , #
'dccp.debconfbenefit':             from_text    , #
'dccp.whyrequest':                 from_text    , #

# public.conference_person_travel cpt
'cpt.conference_person_id':        from_integer , # not null
'cpt.arrival_transport':           from_text    , # not null
'cpt.arrival_from':                from_text    , #
'cpt.arrival_to':                  from_text    , #
'cpt.arrival_number':              from_text    , #
'cpt.arrival_date':                from_date    , #
'cpt.arrival_time':                from_time    , # time(0) with time zone #
'cpt.arrival_pickup':              from_boolean , # not null default false
'cpt.departure_pickup':            from_boolean , # not null default false
'cpt.departure_transport':         from_text    , # not null
'cpt.departure_from':              from_text    , #
'cpt.departure_to':                from_text    , #
'cpt.departure_number':            from_text    , #
'cpt.departure_date':              from_date    , #
'cpt.departure_time':              from_time    , # time(0) with time zone #
'cpt.travel_cost':                 from_numeric , # numeric(16,2) #
'cpt.travel_currency':             from_text    , # not null
'cpt.accommodation_cost':          from_numeric , # numeric(16,2) #
'cpt.accommodation_currency':      from_text    , # not null default 'USD'::text
'cpt.accommodation_name':          from_text    , #
'cpt.accommodation_street':        from_text    , #
'cpt.accommodation_postcode':      from_text    , #
'cpt.accommodation_city':          from_text    , #
'cpt.accommodation_phone':         from_text    , #
'cpt.accommodation_phone_room':    from_text    , #
'cpt.fee':                         from_numeric , # numeric(16,2) #
'cpt.fee_currency':                from_text    , # not null
'cpt.need_travel_cost':            from_boolean , # not null default false
'cpt.need_accommodation':          from_boolean , # not null default false
'cpt.need_accommodation_cost':     from_boolean , # not null default false
'cpt.arrived':                     from_boolean , # not null default false # always false. use cp.arrived

# debconf.dc_daytrip dcdt,
'dcdt.daytrip_id':                 from_integer , # not null
'dcdt.daytrip_option':             from_text    , # character varying(60) #
'dcdt.conference_id':              from_integer , # not null default 1

# debconf.dc_person dcp  # note: there is also "debconf.dc_person_damn_cold" and "debconf.dc_person_data_frozen"
'dcp.person_id':                   from_integer , # not null
'dcp.emergency_name':              from_text    , #
'dcp.emergency_contact':           from_text    , #
'dcp.photo_or_film_ok':            from_boolean , # not null default true

# public.conference c
'c.conference_id':                 from_integer , # not null default nextval
'c.acronym':                       from_str     , # text # not null
'c.title':                         from_text    , # not null
'c.subtitle':                      from_text    , #
# ...

# debconf.dc_accomodation dca
'dca.accom_id':                    from_integer , # not null
'dca.accom':                       from_text    , # character varying(120) #
'dca.conference_id':               from_integer , # not null default 1

# debconf.dc_t_shirt_sizes dctss
'dctss.t_shirt_sizes_id':          from_integer , # not null
'dctss.t_shirt_size':              from_text    , #

# debconf.dc_food_preferences dcfp
'dcfp.food_id':                    from_integer , # not null
'dcfp.food':                       from_text    , # character varying(60) #
'dcfp.conference_id':              from_integer , # not null default 1

# debconf.dc_computer dcc
'dcc.computer_id':                 from_integer , # not null
'dcc.computer':                    from_text    , # character varying(60) #

# debconf.dc_person_type dcpt
'dcpt.person_type_id':             from_integer , # not null
'dcpt.description':                from_text    , # character varying(50) #

# debconf.dc_participant_category dcpc
'dcpc.participant_category_id':    from_integer , # not null default nextval
'dcpc.participant_category':       from_text    , # character varying(50) #
'dcpc.conference_id':              from_integer , # 

# debconf.dc_debcamp dcdc
'dcdc.debcamp_id':                 from_integer , # not null default nextval
'dcdc.debcamp_option':             from_text    , #

# debconf.dc_debconf_role dcr
'dcr.debconf_role_id':             from_integer , # not null
'dcr.description':                 from_text    , # 
'dcr.conference_id':               from_integer , # not null default 6

# debconf.dc_debian_role dcdr
'dcdr.debian_role_id':             from_integer , # not null
'dcdr.description ':               from_text    , #

# debconf.dc_food_select dcfs
'dcfs.food_select_id':             from_integer , # not null
'dcfs.description':                from_text    , #

# public.event_person ep
'ep.event_person_id':              from_integer , # not null default nextval
'ep.event_id':                     from_integer , # not null
'ep.person_id':                    from_integer , # not null
'ep.event_role':                   from_text    , # not null
'ep.event_role_state':             from_text    , # 
'ep.remark':                       from_text    , # 
'ep.rank':                         from_integer , # 

# public.event e
'e.event_id':                      from_integer , # not null default nextval
'e.conference_id':                 from_integer , # not null
'e.tag':                           from_text    , # 
'e.title':                         from_text    , # not null
'e.subtitle':                      from_text    , # 
'e.conference_track':              from_text    , # 
'e.conference_team':               from_text    , # 
'e.event_type':                    from_text    , # 
'e.duration':                      from_interval, # not null default '01:00:00'::interval
'e.event_origin':                  from_text    , # not null default 'idea'::text
'e.event_state':                   from_text    , # not null default 'undecided'::text
'e.event_state_progress':          from_text    , # not null default 'new'::text
'e.language':                      from_text    , # 
'e.conference_room':               from_text    , # 
'e.start_time':                    from_interval, # 
'e.abstract':                      from_text    , # 
'e.description':                   from_text    , # 
'e.resources':                     from_text    , # 
'e.public':                        from_boolean , # not null default false
'e.paper':                         from_boolean , # 
'e.slides':                        from_boolean , # 
'e.remark':                        from_text    , # 
'e.submission_notes':              from_text    , # 
'e.conference_day':                from_date    , # 

}

query_join = {'cp': (
# keep sorted: reference only previous tables
('cp',   'FROM conference_person cp'),
('p',    'LEFT JOIN public.person p ON (p.person_id = cp.person_id)'),
('dccp', 'LEFT JOIN debconf.dc_conference_person dccp ON (dccp.person_id = cp.person_id AND dccp.conference_id = cp.conference_id)'),
('cpt',  'LEFT JOIN conference_person_travel cpt ON (cpt.conference_person_id = cp.conference_person_id)'),
('dcdt', 'LEFT JOIN debconf.dc_daytrip dcdt ON (dcdt.daytrip_id = dccp.daytrip_id AND dcdt.conference_id = dccp.conference_id)'),
('dcp',  'LEFT JOIN debconf.dc_person dcp ON (dcp.person_id = cp.person_id)'),
('c',    'LEFT JOIN public.conference c ON (c.conference_id = cp.conference_id)'),
('dca',  'LEFT JOIN debconf.dc_accomodation dca ON (dca.accom_id = dccp.accom_id)'),
('dctss','LEFT JOIN debconf.dc_t_shirt_sizes dctss ON (dctss.t_shirt_sizes_id = dccp.t_shirt_sizes_id)'),
('dcfp', 'LEFT JOIN debconf.dc_food_preferences dcfp ON (dcfp.food_id = dccp.food_id)'),
('dcc',  'LEFT JOIN debconf.dc_computer dcc ON (dcc.computer_id = dccp.computer_id)'),
('dcpt', 'LEFT JOIN debconf.dc_person_type dcpt ON (dcpt.person_type_id = dccp.person_type_id)'),
('dcpc', 'LEFT JOIN debconf.dc_participant_category dcpc ON (dcpc.participant_category_id = dccp.dc_participant_category_id)'),
('dcdc', 'LEFT JOIN debconf.dc_debcamp dcdc ON (dcdc.debcamp_id = dccp.debcamp_id)'),
('dcr',  'LEFT JOIN debconf.dc_debconf_role dcr ON (dcr.debconf_role_id = dccp.debconf_role_id)'),
('dcdr', 'LEFT JOIN debconf.dc_debian_role dcdr ON (dcdr.debian_role_id = dccp.debian_role_id)'),
('dcfs', 'LEFT JOIN debconf.dc_food_select dcfs ON (dcfs.food_select_id = dccp.food_select)'),
),
'p': (
('p',    'FROM public.person p'), 
),
'ep': (
('ep',   'FROM public.event_person ep'),
('p',    'LEFT JOIN public.person p ON (p.person_id = ep.person_id)'),
('e',    'LEFT JOIN public.event e ON (e.event_id = ep.event_id)'),
('c',    'LEFT JOIN public.conference c ON (c.conference_id = e.conference_id)'),
),
'e': (
('e',    'FROM public.event e'),
('c',    'LEFT JOIN public.conference c ON (c.conference_id = e.conference_id)'),
)
}

query_end = {
'cp': """\
  WHERE cp.conference_person_id > 1 %s
  ORDER BY cp.conference_person_id;
""",
'p': """\
  WHERE p.person_id > 1 %s
  ORDER BY p.person_id;
""",
'ep': """\
  WHERE ep.event_person_id > 1 %s
  ORDER BY ep.event_person_id;
""",
'e': """\
  WHERE e.event_id > 1 %s
  ORDER BY p.event_id; 
""",
}

if len(sys.argv) <= 2:
    print usage
    print "Available modes:"
    for mode in query_join.keys():
	print "  mode '%s'" % (mode)
        print "    with table identification:" % map(lambda t: t[0], query_join.keys[mode])
    print
    print "Available fields:"
    keys = map(lambda d: d[0], fields)
    keys.sort()
    print "Available keys:"
    print " ".join(keys)
    sys.exit(0)

mode = sys.argv[1]
WHERE = os.environ.get('WHERE', "")
if WHERE: WHERE = " AND " + WHERE
    
query_join_dict = dict(query_join[mode])
keys = []
tables_set = set((mode,))
transforms = []
for key in sys.argv[2:]:
    keys.append(key)
    tables_set.add(key.split(".")[0])
    transforms.append(fields[key])

# let's have JOINs in the original (query_join) order.
actual_query_join = [j for t, j in query_join[mode] if t in tables_set]

query = ( "SELECT\n  " + ",\n    ".join(keys) + "\n  " +
          "\n    ".join(actual_query_join) +
          "\n" + (query_end[mode] % WHERE)
        )


db = pentaconnect.get_conn()
c = db.cursor()
c.execute(query)


# print header of the csv file
print ",".join(keys)

data = {}
for row in c.fetchall():
    line = []
    if not row[0]:
	continue
    for i in range(len(transforms)):
        line.append(transforms[i](row[i]))
        data[keys[i]] = row[i]
    print ",".join(line)


