# -*- coding: utf-8 -*-

import datetime

Y = 2013
N = "13"
DCN = "DC13"
dcN = 'dc13'
DebConfN = "DebConf13"
conference_id = 7


DEBCAMP_START            = datetime.date(2013, 8, 6)
DEBCONF_START            = datetime.date(2013, 8, 11)
DEBCONF_EARLIEST_ARRIVAL = datetime.date(2013, 8, 10)
DEBCONF_END              = datetime.date(2013, 8, 18)


COST_FOOD_DAILY = 25
COST_ROOM_DAILY = 53
#COST_FOOD_PER_MEAL = 10
#COST_MEALS_PER_DAY = 2
#COST_FOOD_DAILY = ( COST_FOOD_PER_MEAL * COST_MEALS_PER_DAY )
COST_ROOM_COMM_DAILY = 20
COST_ROOM_CAMP_DAILY = 20
COST_ROOM_EIGHT_DAILY = 30
COST_ROOM_TWO_DAILY = 40
COST_PRO = 200
COST_COR = 500
COST_DEBCAMP = 1234567


# p.participant
CAT_BAF = set(('Basic; want sponsored food and accommodation',
               'Basic; want both sponsored food and accommodation',
               'Sponsored registration'))
CAT_BF  = set(('Basic; want sponsored food',))
CAT_BA  = set(('Basic; want sponsored accommodation',))
CAT_B   = set(('Basic; no sponsored food or accommodation',
               'Basic registration (no accommodation provided)',
               'Basic registration (free)'))
CAT_PRO = set(('Professional (650 USD per full or partial week)',
               'Professional registration (€450)',
	       'Professional registration (US$650)',
	       'Professional registration (200 CHF)'))
CAT_COR = set(('Corporate (1300 USD per full or partial week)',
               'Corporate registration (€1000)',
	       'Corporate registration (US$1300)',
               'Corporate registration (500 CHF)'))
CAT_NONE = set(("--- please select one ---",
                "--- Please select one ---",
		' --- Please select one --- '))

# p.debcamp
DEBCAMP_NO     = set(("I won't be attending DebCamp",))
DEBCAMP_PLAN   = set(("I have a specific work plan for DebCamp",))
DEBCAMP_NOPLAN = set(("I don't have a specific plan for DebCamp (week payment (650 USD) required)",
                 "I don't have a specific plan for DebCamp (week payment (€450) required)",
		 "I don't have a specific plan for DebCamp (week payment (US$650) required)",
		 "I don't have a specific plan for DebCamp (payment required)"))

# p.food
FOOD_REG  = set(("No dietary restrictions",))
FOOD_VEG  = set(("Vegetarian",))
FOOD_VEGAN= set(("Vegan (strict vegetarian)",))
FOOD_OTHER= set(("Other (contact organizers)",))
FOOD_NO   = set(("Not eating with the group",))

# p.dc_food_select.description
FOOD_OUTSIDE = set(('I will care for my own food and understand that there are no shops or restaurants nearby.',))
FOOD_PAID    = set(('I wish to pay for food at the conference (25 CHF/day)',))
FOOD_SPONS   = set(('I request sponsored food',))

# o.accom
ACCOM_NO        = set(("I will arrange my own accommodation",
                       "I will arrange my own accomodation",
                       'I will arrange my own accomodation off-site (no fee)'))
ACCOM_SPONSORED = set(('I request sponsored accommodation (Not for Professional/Corporate)',))
ACCOM_COMMUNAL  = set(('self-paid communal accommodation (20 CHF/day) ',
                       'self-paid communal accommodation (20 CHF/day)'))
ACCOM_CAMPING   = set(('self-paid camping (camper van or tent; 20 CHF/day, limited availability) ',
                       'self-paid camping (camper van or tent; 20 CHF/day, limited availability)'))
ACCOM_EIGHT     = set(('self-paid accommodation in room with up to 8 beds (30 CHF/day) ',
                       'self-paid accommodation in room with up to 8 beds (30 CHF/day)'))
ACCOM_TWO       = set(('self-paid accommodation in room with 2 beds (40 CHF/day/person, only for Professional/Corporate)',))
ACCOM_UPGRADE   = set(('upgrade to 4-bed room (20 CHF/day/person, only sponsored)',))
ACCOM_YES = set(('Hostel, I (or my company) will pay (about 20EUR per night)',
                 'Hotel, I (or my company) will pay',
                 'Hostel, I need sponsorship',
                 'Single Room (extra payment required)',
                 'Junior Suite (extra payment required)',
                 'Regular Room',
                 'Regular room',
                 "On-campus room",
                 "Regular DebConf accomodation"))

STATUS_SPONS = set(('Debian Project Member (all DDs)',
		    'Debian Maintainer',
		    'Debian Contributor (including artist, translator, etc.)',
                    'Otherwise involved in Debian',
                    'Not yet involved but interested',))
STATUS_NOSPONS = set(('Accompanying a Debian participant',
                      'Press',
                      'Sponsor',))

DEBIAN_ROLE_ALL = set(('Debian Project Member (all DDs)',
                     'Debian Maintainer',
                     'Debian contributor (including artist, translator, etc.)',
                     'Otherwise involved in Debian',
                     'Not yet involved but interested',
                     'Accompanying a Debian participant',
                     'None', ))

DEBCONF_ROLE_SPONS = set(('Regular attendee',
                        'Volunteer',
                        'Organizer', ))
DEBCONF_ROLE_UNSPONS = set(('Sponsor',
                            'Press',
                            'Accompanying a DebConf participant',
                            'None', ))
DEBCONF_ROLE_ALL = DEBCONF_ROLE_SPONS|DEBCONF_ROLE_UNSPONS


del datetime

