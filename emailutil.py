# -*- coding: utf-8 -*-

"""How darst sends the mboxes:

formail -s /usr/sbin/sendmail -ti < xxxxxxxx.mbox
"""

import email
import email.utils
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.header import Header
import mailbox
import re
import smtplib
import time

#smtpArgs = {'host':"127.0.0.1", 'port':1025}  # smtp (host, port) tuple
#smtpArgs = {'host':'127.0.0.1','port':None,}# 'local_hostname':'ramanujan')

class Email(object):
    def __init__(self, name, email):
        if not name:
            self.name = email
        else:
            self.name = name
        
        self.email = email
    def __str__(self):
        if self.name == self.email:
            return self.email
        #name = Header(self.name, 'utf8')
        #print name
        #name=self.name.decode('ascii', 'ignore')
        try:
            name = self.name.decode('us-ascii')
        except:
            name = Header(self.name, 'utf-8')
        if isinstance(name, (str, unicode)) and "," in name:
            name = name.replace('"', '')
            name = '"%s"'%name
        #if self.email.find('dato') != -1:
        #    from fitz import interactnow
        #print name
        return '%s <%s>'%(name, self.email)

def make_list(obj):
    if isinstance(obj, Email):
        return (obj, )
    elif isinstance(obj, (str, unicode)):
        return Email(None, obj)
    return obj
    
class Message(object):
    def __init__(self, From, To, Subject, Body,
                 Cc=None, Bcc=None, ReplyTo=None,
                 extraMsgid=None):
        self.emails = [ ]

        
        msg = MIMEText(Body)#, _charset='utf-8')
        msg["From"] = str(From)
        self.fromEmail = From.email
        #msg.set_unixfrom(From.email)
        # To
        To = make_list(To)
        msg["To"] = ", ".join(str(x) for x in To)
        self.emails.extend(x.email for x in To)
        # Subject
        msg["Subject"] = Subject
        # Cc
        if Cc:
            Cc = make_list(Cc)
            msg["Cc"] = ", ".join(str(x) for x in Cc)
            self.emails.extend(x.email for x in Cc)
        # Bcc
        if Bcc:
            Bcc = make_list(Bcc)
            self.emails.extend(x.email for x in Bcc)
        # Reply-To
        if ReplyTo:
            ReplyTo = make_list(ReplyTo)
            msg["Reply-To"] = ", ".join(str(x) for x in ReplyTo)
        # Extra stuff
        msg["User-Agent"] = "debconf python-stdlib scripts"
        msg["Message-Id"] =  email.utils.make_msgid(extraMsgid)
        msg["Date"] = email.utils.formatdate(localtime=True)
        msg.set_param('charset', 'utf-8')
        self.msg = msg
    def __str__(self):
        return self.msg.as_string(True)

class MessageWithAttachments(object):
    def __init__(self, From, To, Subject, Body,
                 Cc=None, Bcc=None, ReplyTo=None,
                 extraMsgid=None):
        self.emails = [ ]


	msg = MIMEMultipart()
        msg["From"] = str(From)
        self.fromEmail = From.email
        #msg.set_unixfrom(From.email)
        # To
        To = make_list(To)
        msg["To"] = ", ".join(str(x) for x in To)
        self.emails.extend(x.email for x in To)
        # Subject
        msg["Subject"] = Subject
        # Cc
        if Cc:
            Cc = make_list(Cc)
            msg["Cc"] = ", ".join(str(x) for x in Cc)
            self.emails.extend(x.email for x in Cc)
        # Bcc
        if Bcc:
            Bcc = make_list(Bcc)
            self.emails.extend(x.email for x in Bcc)
        # Reply-To
        if ReplyTo:
            ReplyTo = make_list(ReplyTo)
            msg["Reply-To"] = ", ".join(str(x) for x in ReplyTo)
        # Extra stuff
        msg["User-Agent"] = "debconf python-stdlib scripts"
        msg["Message-Id"] =  email.utils.make_msgid(extraMsgid)
        msg["Date"] = email.utils.formatdate(localtime=True)
        msg.set_param('charset', 'utf-8')

        msg.attach(MIMEText(Body))
        self.msg = msg

    def attach(self, part):
	self.msg.attach(part)
    def __str__(self):
	return self.msg.as_string(True)

def extractEmail(s):
    """Extract just the email part of a 'xxx <yyy@zzz>' string."""
    return re.search("<(.*?)>", s).group(1)

def recordSending(msg):
    """This provides a record of what we send, to allow us to
    gracefully recover from exceptions.
    """
    sent_log = open('sent-messages', 'a')
    if msg is None:
        sent_log.write("-- \n")
        return
    for email in msg.emails:
        sent_log.write("%s %s\n"%(time.time(), email))
    f.close()


def sendMessage(msg, archive=None, sent_log=None):
    """Send a single message.
    """
    S = smtplib.SMTP(**smtpArgs)
    ret = S.sendmail(msg.fromEmail, msg.emails, str(msg))
    # XXX This raises an exception if *no* recipients were accepted.
    if ret:
        print "could not send:"
        for k,v in ret.iteritems():
            print "    %s %s"%(k,v)
    print "sending to:", msg.emails
    # write email addresses sent to in a file.  Serves to recover from
    # catastrophic errors
    recordSending(msg)
    # archive in a mbox
    if archive is not None:
        archive.add(str(msg))
        archive.flush()
    S.quit()

def sendMessages(msgs, archive=None):
    """Send a batch of messages.
    """
    S = smtplib.SMTP(**smtpArgs)
    for msg in msgs:
        ret = S.sendmail(msg.fromEmail, msg.emails, str(msg))
        # XXX This raises an exception if *no* recipients were accepted.
        if ret:
            print "could not send:"
            for k,v in ret.iteritems():
                print "    %s %s"%(k,v)
        # write email addresses sent to in a file.  Serves to recover from
        # catastrophic errors
        recordSending(msg)
        # archive in a mbox
        if archive is not None:
            archive.add(str(msg))
            archive.flush()
    S.quit()



    
if __name__ == "__main__":

    body = """%(name)s:

You can has %(item)s.

- The Utahraptor
"""

    mbox = mailbox.mbox("emailutil-test.mbox")
    mbox.clear()

    for name, emailaddress, item in (("Alpha", "alpha@debian.org", "Aiur"),
                              ("Beta", "beta@debian.org", "etesc();"),
                              ("Gamma", "gamma@debian.org", "hdpmc"),
                              ("Delta, Firstname", "delta@debian.org", "utahraptor"),):
        Body = body%locals()
        
        msg = Message(
            # The Email(real_name, email_address) object aids in
            # making email addresses.  If you don't have a real name,
            # use it as Email(None, email_address)
            From=Email("The Cabal", "cabal@debconf.org"),
            To=Email(name, emailaddress),
            Subject="The Meme Cabal has decided what you can has",
            Body=Body,
            extraMsgid="dc10memeallocation",  # some string to put in msgid
            ReplyTo=Email("Dev Null", "devnull@debconf.org"),
            # Anything can besides "From" can be a python list of Email objects.
            Cc=[Email("Cabal Member #1", "nkocrk1@debconf.org"),
                Email("Cabal Member #2", "rk64ank@debconf.org"),],
        )
        mbox.add(str(msg))
        # Look at the mbox to see if everything looks right.  Send with
        # formail -s /usr/sbin/sendmail -ti < emailutil-test.mbox
