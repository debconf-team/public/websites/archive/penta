# Richard Darst, May 2010

import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message

from dc_objs import CAT_BAF, CAT_BA, CAT_BF, DEBCAMP_NO, DEBCAMP_NOPLAN, DEBCAMP_PLAN
from dc_objs import CAT_SPONS


body = """\
Hello %(name)s,

%(items)s

The registration system URL is
  https://penta.debconf.org/penta/submission/dc12/person

If you have any questions, please contact us via IRC:
  #debconf-team @ irc.oftc.net (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

# Each of these is a pair: (message_text, condition_for_text_to_be_relevant)
classes = {
    # Case A: yes debconf, no debcamp
#    "A":("""\
#We are happy to inform you that after reviewing the information you
#provided, we are able to grant you sponsored accommodation for the
#upcoming DebConf 12 in Managua!
#
#However, we are sorry to say that we can not grant you the requested
#sponsored food and accommodation during DebCamp which precedes DebConf
#itself. We have set your registration status to \"DebCamp without
#workplan - $650/week\", if you would not like to attend DebCamp,
#please change this to \"I won't be attending DebCamp\" (link below).
#
#You have also been approved for food sponsorship during DebConf
#itself.""",
#         lambda p: (p.participant_category in CAT_BAF
#                    and p.debcamp not in DEBCAMP_PLAN)
#         ),
   "A":("""\
Your request for sponsored food and accomodation for DebConf12 has
passed the initial review process, and will be granted if this year's
budget allows. We hope to be able to finalize this decision soon.

We are sorry to say that we can not grant you the requested
sponsored food and accommodation during DebCamp which precedes DebConf
itself. We have set your registration status to \"DebCamp without
workplan - $650/week\" . If you would prefer to only attend the main
conference, please change this to \"I won't be attending DebCamp\" (link
below).""",
         lambda p: (p.participant_category in CAT_BAF
                    and p.debcamp not in DEBCAMP_PLAN)
         ),
    # Case B: Granted all
    "B":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 12 and DebCamp (if you applied for DebCamp) in
Managua!

You have also been approved for food sponsorship.""",
         lambda p: (p.participant_category in CAT_BAF
                    and p.debcamp not in DEBCAMP_NOPLAN)
         ),
    # Case C: asked only food, got it
    "C":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored food for the upcoming
DebConf 12 and DebCamp (if you applied for DebCamp) in Managua!""",
         lambda p: (p.participant_category in CAT_BF
                    and p.debcamp not in DEBCAMP_NOPLAN)
         ),
    # Case D: asked only accom, got it
    "D":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 12 and DebCamp (if you applied for DebCamp) in
Managua!""",
         lambda p: (p.participant_category in CAT_BA
                    and p.debcamp not in DEBCAMP_NOPLAN)
         ),
    # Case E: denied
    "E":("""\
We are sorry to inform you that after reviewing the information you
provided, we are not able to grant you sponsored food and accommodation
for the upcoming DebConf 12 in Managua.

We do hope you still will be able to attend DebConf. If this is still
the case, please go to your pentabarf page (link below) and update the
\"Category\" field with the correct information (if you will get
accommodation and food on your own, or if you will pay and stay with
others attendees in selected hotels)""",
         lambda p: (p.participant_category not in CAT_SPONS
                    and p.debcamp not in DEBCAMP_PLAN) ),

    
    }

people = dc_objs.get_people()
mbox = mailbox.mbox("dc12-sponsorstatus_20120606-BCD.mbox")
mbox.clear() ; mbox.flush()

# defines person_classes: A dictionary that maps Class -> penta_ids in
# that class.  Classes define different types of sponsorship
# requested/granted.
#execfile("dc12/sponsorship-status_20120606.dat", globals(), locals())
person_classes = dict((c, set()) for c in 'ABCDE')
for line in open("dc12/sponsorship-status_20120606.dat"):
    if not line.strip(): continue
    c, p_id = line.split()
    p_id = int(p_id)
    person_classes[c].add(p_id)

people_processed = set()

for person in people:
    # skip unsponsored and prof/corp.
    items = [ ]
    for classname in classes:
        if person.person_id in person_classes[classname]:
            # Do we want to send this class of mails right now?
            if classname in ('A', 'E'):
                continue

            items.append(classes[classname][0])
            # Check to make sure we are sending teh right messages.
            # This will print an error if we sent a message to a
            # person, but the message talks about sponsorhsip types
            # they did not request.  For example, sending an email
            # saying "You got no sponorship" to someone who did not
            # request sponsorship will print an error here.
            if not classes[classname][1](person):
                print "check", person.person_id, classname
    if len(items) > 1:
        print "Person is in too many classes:", person.person_id
    if len(items) == 0:
        continue
    people_processed.add(person.person_id)

    person.items = "\n\n".join(items)

    Body = body % person.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf12: Sponsorship status",
        Body=Body,
        extraMsgid="dc12sponsorshipstatus1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))

# This checks for people who are are in person_classes (they should
# get a message), but didn't actually get a message.  This usually
# means that get_people didn't return them - is conference_id wrong?
print "Checking for missed people:"
for classname in classes:
    if len(set(person_classes[classname]) - people_processed) > 0:
        print classname, "missed people", \
              set(person_classes[classname]) - people_processed

# Who did we not send an email to?
allPeople = set(p.person_id for p in people)
print "These people aren't in any class:", allPeople - (
    reduce(lambda x,y: x|y, person_classes.itervalues())
    | set(p.person_id for p in people if p.participant_category in dc_objs.CAT_UNSPONS)
#    | set(p.person_id for p in people if p.participant_category in dc_objs.CAT_NONE)
    )
print "These people didn't get an email:", allPeople - people_processed
