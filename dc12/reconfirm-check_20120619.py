# Richard Darst, April 2010

import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


beginning = """\
Hello %(name)s,%(mail_banner)s

This (automated) message is being sent to attendees to remind you
about reconfirmation of your attendance, and to let you know of any
errors in the registration, or other things which you should know
about.  Please look over the items below, and correct any problems
which may be present.  Some things are just warnings or notes, and
won't apply to most people.  These are clearly labeled, if one doesn't
apply to you, please ignore it.

Because this is an automated message, perhaps there are false
positives on the errors or warnings listed here.  If you know that
something does not apply to you or you have contacted us previously,
please disregard.

If you have any questions, please contact us via
  IRC:          #debconf-team @ irc.oftc.net   (real-time questions)
  or mailing list:      debconf-team@lists.debconf.org (public list)
  or non-public email:  registration@debconf.org

The registration system URL is
  https://penta.debconf.org/penta/submission/dc12/person

Please check that everything is updated by the reconfirmation deadline
of 20 June, thus NOW!  Please see below for information
about food and lodging sponsorship.  Even if you aren't being
sponsored, registration estimates are required for sponsorship and
budgeting planning."""

reconfirm_warning = """

** You must reconfirm and provide the required information by 20 June
** or your registration will be canceled.  Please don't wait and do it
** TODAY."""

ending = """\
Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body = """%(beginning)s

Information about your registration
===================================

%(info)s

Registration errors and warnings
================================

%(errors)s


%(ending)s
"""

# Get our list of errors:
Errors = errorcheck.Errors
Errors = [e for e in Errors
          if e.__class__.__name__ not in ('RegistrationCheck',
                                          'ThirteenYearOldAgeLimit',
                                          'DoubleCheckTravelSponsorshipNumbers',
                                          'NewlyEnabledFieldsInfo') ]
Errors = [e for e in Errors
          if 'travel' not in e._class ]
errorcheck.printErrors(Errors)
print
print

# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
People = dc_objs.get_people()
[ P.add_errors(Errors) for P in People ]
People = [ P for P in People if len(P.errors) > 0 ]
mbox = mailbox.mbox("dc12reconfirm-last.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    #Errors = [e for e in person.errors
    #          if e.__class__.__name__ not in ('RegistrationCheck',
    #                                          'ThirteenYearOldAgeLimit',
    #                                          'DoubleCheckTravelSponsorshipNumbers',
    #                                          'NewlyEnabledFieldsInfo') ]
    #Errors = [e for e in Errors
    #          if 'travel' not in e._class ]
    #person.errors = Errors

    #if len(Errors) > 0:
    #    print '='*40
    #    print person.person_id, person.name
    #    print person._info()


    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    subject_extra = ""
    if not person.reconfirmed:
	subject_extra += ", please reconfirm NOW"

    person.mail_banner = ""
    if not person.reconfirmed:
        person.mail_banner = reconfirm_warning

    Body = body%locals()
    Body = Body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf12 registration check, LAST CHANCE to change data" + subject_extra,
        Body=Body,
        extraMsgid="dc12reconfirm-last",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


errorcheck.printPeopleErrors(People)
print
print
errorcheck.printErrorsByClass(People)
