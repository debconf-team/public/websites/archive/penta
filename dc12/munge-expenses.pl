#!/usr/bin/perl

# Copyright 2012 David Bremner
# licensed under the WTFPL v2 
# http://sam.zoy.org/wtfpl/COPYING for more details.

use Text::CSV;
use utf8;
binmode STDOUT,":utf8";
my @rows;

my $contrib_weight=shift(@ARGV) || 0.7;
my $limit=shift(@ARGV) || 1000000;
my $use_names=shift(@ARGV) || 0;

my $csv = Text::CSV->new ( { binary => 1 } )
  or die "Cannot use CSV: ".Text::CSV->error_diag ();

# in case someone is missing here, but shows up in penta expense report, 
# see if they unchecked "want to attend"
open my $fh, "<:encoding(utf8)", "expenses-report_werkeka.csv" or die "$!";

my $firstline=$csv->getline($fh);

$csv->column_names(@$firstline);

while ( my $row = $csv->getline_hr( $fh ) ) {
  next if ($row->{need_travel_cost} ne 'True');
  # HEURISTIC!
  if ($row->{contributor_level} == 0.0 and $row->{requested_amount} == 0.0) {
    printf STDERR "skipping dcid %d\n",$row->{id};
    next;
  }
  $row->{NewScore}= $contrib_weight * $row->{contributor_level} + 
    (1- $contrib_weight) * $row->{requested_amount};

  push @rows, $row;
}
$csv->eof or $csv->error_diag();
close $fh;

my $sum=0;

foreach my $row (sort { $b->{NewScore} <=> $a->{NewScore} } @rows) {
  $sum+=$row->{cantfund};
  printf "%d\t%s\t%f\t%d\n", $row->{id}, $use_names ? $row->{name} : '',
    $row->{NewScore},$sum;
  last if ($sum>= $limit);
}

