# Richard Darst, May 2010

from emailutil import Email, Message
import mailbox
import sys
sys.path.append('.')

import emailutil
print emailutil.__file__

body_sponsored = """Hello %(name)s,

You requested travel sponsorship for the upcoming DebConf12 in Managua.
  Total cost:       USD %(total_cost)s
  Amount requested: USD %(amount_requested)s

We pleased to inform you that we are able to sponsor your entire 
amount requested.

To make it more likely that we will be able to fulfill your request
(as well as everybody else's), please make sure the amount you have
requested is the minimum you /really/ need in order to join us at
DebConf.

Finally, this is a great opportunity to mention that DebConf needs help with
its fundraising efforts!  Just stop by #debconf-team on irc.debian.org and ask
to get invited to #debconf-sponsors. Also, if you know anyone able to make a
financial contribution, please consider pointing them to the donation
information here: http://debconf12.debconf.org/payments.xhtml

Thank you,

-- 
The DebConf12 Travel Sponsorship Team
"""

body_undecided = """Hello %(name)s,

We had a technical/human malfunction with sponsorship email, and you
might have received a message in error about sponsorship, with
somebody else's name it.  This followup message is to clarify that
your request for sponsorship has neither been rejected nor accepted at
this time. We are still waiting for more budget information, and will
let you know more as soon as we can.

Thank you, and sorry for the uncertainty/confusion.

-- 
The DebConf12 Travel Sponsorship Team
"""

body_unsponsored = """%(name)s,

You had requested travel sponsorship for the upcoming DebConf12 in Managua.
  Total cost:       USD %(total_cost)s
  Amount requested: USD %(amount_requested)s

At this time we do not have the budget to accommodate all the requests
for sponsorship we have received and we regret to inform you that we
are unable to grant your request.

We hope you can arrange your travel through alternative funding
channels, and look forward to seeing you at DebConf12.

Thank you,

-- 
The DebConf12 Travel Sponsorship Team
"""

if len(sys.argv) < 3:
    print "Usage: %s <input_csv> <output_mbox>"%sys.argv[0]
    exit(1)
datfile = open(sys.argv[1])
mbox = mailbox.mbox(sys.argv[2])


for line in datfile:
    queue, name, email, queue_position, total_cost, amount_requested, \
        score_contrib, score_travel, score = \
           line.split(',')

    output=False

    if queue in ("A"):
        Body = body_sponsored%locals()
        output=True
    elif queue in ("B"):
        print "Mind the queue B", name
        Body = body_undecided%locals()
        output=True
    elif queue in ("C", ):
        output=True
        Body = body_unsponsored%locals()
    else:
        raise Exception("Person %s has unknown queue %s"%(name, queue))
        
    if output:
        msg = Message(
            # The Email(real_name, email_address) object aids in
            # making email addresses.  If you don't have a real name,
            # use it as Email(None, email_address)
            From=Email("DebConf12 Travel Sponsorship Team", "herb@debconf.org"),
            To=Email(name, email),
            Subject="DebConf12 Travel Sponsorship",
            Body=Body,
            extraMsgid="dc12travelsponsorship1",  # some string to put in msgid
            ReplyTo=Email("DebConf12 Travel Sponsorship Team", "herb@debconf.org"),
            # Anything can besides "From" can be a python list of Email objects.
            Cc=[Email("DebConf12 Travel Sponsorship Team", "herb@debconf.org"),],
            )
        mbox.add(str(msg))
    # Look at the mbox to see if everything looks right.  Send with
    # formail -s /usr/sbin/sendmail -ti < emailutil-test.mbox

