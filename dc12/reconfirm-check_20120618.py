# Richard Darst, April 2010

import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message

 
body = """\
Hello %(name)s,

This is your last chance to update some critical items in your DebConf
registration before the reconfirmation deadline, 20 June:


%(items)s


The registration system URL is
  https://penta.debconf.org/penta/submission/dc12/person

If you have any questions, please contact us via
IRC:
  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
"""

not_reconfirmed = """\
- You are currently not reconfirmed.  Please reconfirm before the
  deadline - this is required for sponsored people.
    Pentabarf -> Reconfirm Attendance"""
dates_needed = """\
- You are currently missing one or both dates.  Please enter your
  dates of attendance by 20 June."""
#  (There will be opportunities to
#  change them until 30 June, but it will help our planning if you can
#  enter them sooner)
#    Pentabarf -> Travel"""
dates_please = """\
- Please try enter your dates of attending DebConf by 20 June.  If
  you are a day visitor, attending intermitently, please enter the
  first and last day you might be around so that we can keep track of
  things.  (Since you aren't sponsored and not staying in our
  accommodations, there will be opportunities to change them later)
    Pentabarf -> Travel"""


# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
People = dc_objs.get_people()
mbox = mailbox.mbox("dc12reconfirm_20120618.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    items = [ ]
    if not person.reconfirmed:
        items.append(not_reconfirmed)
    if person.arrival_date is None or person.departure_date is None:
        if person.participant_category in dc_objs.CAT_SPONS or \
               person.accom not in dc_objs.ACCOM_NO:
            items.append(dates_needed)
        #else:
        #    items.append(dates_please)
    if len(items) == 0:
        # No reminders needed here
        continue

    person.items = "\n\n".join(items)
    Body = body%person.__dict__

    
    Body = Body%person.__dict__
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf12 reconfirmation (by 20 June)",
        Body=Body,
        extraMsgid="dc12reconfirm3",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))

mbox.flush()


