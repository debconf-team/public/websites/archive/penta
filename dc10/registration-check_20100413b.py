# Richard Darst, April 2010

import mailbox
from textwrap import dedent

import errorcheck
import emailutil
from emailutil import Email, Message
import dc_objs

People = dc_objs.get_people()
People = [ p for p in people if "please" in p.participant_category ]

body = """\
%(name)s,

This (automated) message is to remind you that you need to set your
participant categor in Pentabarf, if you want sponsorship.  Your
current particupant category is:

  %(participant_category)s

The last day to select one of the sponsored categories is TODAY, April
15th.  (There is no deadline for the paid or unsponsored categories.)

Thank you,

-- 
Richard Darst
On behalf of the DebConf team
"""

#mbox = mailbox.mbox("dc10-registration_20100415.mbox")

for person in People:
    print '='*40
    print person.person_id, person.name
    print person.participant_category

    Body = body%person.__dict__
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("Richard Darst", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="IMPORTANT: DebConf10 sponsorship deadline",
        Body=Body,
        extraMsgid="dc10sponsdeadline1",
        ReplyTo=Email("DebConf registration", "registration@debconf.org"),
        Cc=Email("DebConf registration", "registration@debconf.org"),
        )
    #mbox.add(str(msg))
