# Richard Darst, April 2010

import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message

 
body = """\
Hello %(name)s,

This (automated) message is being sent to you because you have not
yet provided your Arrival and Departure dates in the DebConf
registration system at:
  https://penta.debconf.org/penta/submission/dc10/person

Because you are staying in our accommodations or are sponsored for
food, we need to know the dates you will be attending by July 6th.

If you have decided not to use the conference housing, please mark
yourself as 'I will arrange my own accommodation'.

If you decide to change your dates after July 1st, please mail us at
registration@debconf.org .  If you can not determine your dates before
then, please mail us at that address and let us know about your
situation.


If you have any questions, please contact us via
IRC:
  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
"""


# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
People = dc_objs.get_people()
mbox = mailbox.mbox("dc10enter-dates_20100629.mbox")
mbox.clear() ; mbox.flush()

for person in People:
    # This parts deals with printing.
    items = [ ]
    if not (person.attend and person.reconfirmed):
        continue
    if (person.arrival_date is None or person.departure_date is None) \
        and (person.accom not in dc_objs.ACCOM_NO
            or
            person.participant_category in dc_objs.CAT_SPONS):

        
        Body = body%person.__dict__

        msg = emailutil.Message(
            From=Email("DebConf Registration Team", "registration@debconf.org"),
            To=Email(person.name, person.email),
            Subject="DebConf10: Dates of attendance needed",
            Body=Body,
            extraMsgid="dc10dates2",
            ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
            Cc=Email("DebConf Registration Team", "registration@debconf.org"),
            )
        mbox.add(str(msg))
