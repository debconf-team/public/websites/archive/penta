# Richard Darst, April 2010

import collections
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message

 
body = """\
Dear %(name)s--

We're sorry to see that you did not reconfirm your attendance at
DebConf10 as of the sponsorship deadline today.  (As we noted in
earlier reminder e-mails to you directly, and in public announcements,
we can only provide sponsored food, travel, or accommodation to
attendees who reconfirmed by June 10th.)

Thus, you have been moved into the 'Basic, no sponsored food or
accommodation' category for DebConf10.

If you haven't reconfirmed because you can't make the conference this
year, we hope to see you next year at DebConf11 in Bosnia!

If you just hadn't made up your mind yet, it's still not too late to
decide to attend DebConf10 without sponsorship: please go ahead and
reconfirm at
  https://penta.debconf.org/penta/submission/dc10/person


If you have any questions, please contact us via
IRC:
  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""


import pentaconnect
db = pentaconnect.get_conn()
c = db.cursor()
# There is a sub-table which has one ID representing two different
# things.  I'm not sure what the best way to deal with this is, so
# here is a hack to do manual lookups and find the right
# participant_category_id.
#
# Nomenclature:
# FIELD NAMES
# participant_category - Basic, Professional, etc
# status (description in dc_conf_pers) - Developer, Volunteer, Press, etc.
#
# dccp.dc_participant_category_id - an ID representing BOTH category and status fields.
#                                   Note that this is misnamed!
# 
# debconf.dc_view_participant_map - view which goes from dc_participant_category_id
#                                   to category + status
# debconf.dc_person_type -          used by dcvpm view to lookup status names
# debconf.dc_participant_category - used by dcvpm view to lookup category names
#
#
# A better way to handle this would be welcome.

c.execute("""select * from debconf.dc_view_participant_map;""")
person_status_category = collections.defaultdict(dict)
for participant_mapping_id, person_type, participant_category in c.fetchall():
    person_status_category[person_type.strip()][participant_category.strip()] = \
                                                           participant_mapping_id
person_status_category = dict(person_status_category) # make immutable now
# use the person_status_category like this:
#   person_status_category[status][category] -> dccp.dc_participant_category_id
#                                               representing this combination

# debugging printing:
#for x in sorted(person_status_category):
#    for y in sorted(person_status_category[x]):
#        print x, y, person_status_category[x][y]



People = dc_objs.get_people()
mbox = mailbox.mbox("dc10endreconfirm_20100610.mbox")
mbox.clear() ; mbox.flush()
sql_file = open("dc10endreconfirm_20100610.sql", "w")
print >> sql_file, "BEGIN;"



for person in People:
    # This parts deals with printing.
    items = [ ]
    if person.reconfirmed:
        # ignore reconfirmed people
        continue
    if person.participant_category not in dc_objs.CAT_SPONS:
        # Don't mess with not sponsored people
        continue

    status = person.status
    old_category = person.participant_category
    new_category = "Basic; no sponsored food or accommodation"
    new_participant_mapping_id = person_status_category[status][new_category]


    print "%-30s %-40s %-s"%(person.name, person.participant_category, person.reconfirmed)

    print >> sql_file, """UPDATE debconf.dc_conference_person SET dc_participant_category_id=%d WHERE person_id=%d and conference_id=4;"""%(new_participant_mapping_id, person.person_id)


    person.items = "\n\n".join(items)
    Body = body%person.__dict__

    
    Body = Body%person.__dict__
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf10: reconfirmation deadline passed, sponsorship removed",
        Body=Body,
        extraMsgid="dc10endreconfirm1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


print >> sql_file, "COMMIT;"
