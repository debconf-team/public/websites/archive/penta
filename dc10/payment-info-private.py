# Richard Darst, April 2010

import time

import dc_objs

People = dc_objs.get_people(where="dcvn.attend and dcvn.reconfirmed")

total_cost = sum(P.get_total_cost() for P in People)

print time.ctime()
print
print "Limit: attend and reconfirmed."
print "Only including food costs in the upper report."

print "Total attendee fees:", total_cost


import parse_attendee_payments

payments = parse_attendee_payments.get_payments()

for P in People:
    if P.person_id in payments:
        P.amount_paid = payments[P.person_id]['amount_paid']
    else:
        P.amount_paid = 0.0
        

def print_people(People):
    not_yet_paid = [ ]
    paid_up_people = [ ]
    for P in People:
        if P.amount_paid == P.amount_to_pay:
            paid_up_people.append(P)
        else:
            not_yet_paid.append(P)

    _print_people(not_yet_paid)
    print
    print "Paid up:"
    print
    _print_people(paid_up_people)



def _print_people(People):
    for P in People:
        amount_paid = P.amount_paid
        if P.amount_to_pay is not None:
            amount_to_pay = "%7.2f"%P.amount_to_pay
        else:
            amount_to_pay = None
        
        print P.regSummary(), "%7.2f %7s"%(amount_paid, amount_to_pay), \
              "  ", P.personHeader()

# Categorize people into types:

high_priority = [ ]
food_only = [ ]
no_payment = [ ]
for P in People:
    if P.amount_to_pay == 0 or P.amount_to_pay is None:
        no_payment.append(P)
    elif P.food_cost() and P.get_total_cost() == P.food_cost():
        food_only.append(P)
    else:
        high_priority.append(P)

print "="*40
print "High priority people (accom, professional, or DebCamp):"
print
print_people(high_priority)
print
print


print "="*40
print "Food only people:"
print
print_people(food_only)
print
print


print "="*40
print "No payment people:"
print
print_people(no_payment)
print
print


print "\n"*10


### Update amount_to_pay
print "="*40
print "Validating amount_to_pay (NON-FOOD ONLY):"
for P in People:
    # skip people who have null amount_to_pay and don't need to pay.
    if P.amount_to_pay is None and P.get_total_cost_noFood() == 0:
        continue
    # Now, amount_to_pay should match.
    if P.amount_to_pay != P.get_total_cost_noFood():
        print "="*20
        print P.personHeader()
        print P.regSummary()
        print "Current-amount_to_pay:   ", P.amount_to_pay
        print "Calculated-amount_to_pay:", P.get_total_cost_noFood()
        print "Items:"
        for item in P.payment_items():
            print "    %3d %s"%(item[1], item[0])
        if P.remark:
            print "Notes:", P.remark
        
        print ("UPDATE debconf.dc_conference_person set "
               "amount_to_pay='%d' where person_id=%d and "
               "conference_id=4 ; "%(P.get_total_cost_noFood(), P.person_id))
        print
        print

print "\n"*10

############
    
print "="*40
print "Listing all payment itemizations:"
print

for P in People:
    if P.get_total_cost() == 0:
        continue
    print "="*20
    print P.personHeader()
    print P.regSummary()
    print
    for item in P.payment_items():
        print "    %3d %s"%(item[1], item[0])
    print
    print "    %3d %s"%(P.amount_paid, 'amount_paid')
    print


