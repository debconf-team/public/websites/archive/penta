# Jimmy Kaplowitz, June 2010, based on Richard Darst's work

import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message

body = """\
Hello %(name)s,

According to our records, you have selected registration options for DebConf10
which require payment. This can be for several reasons:

* Registration as a Professional or Corporate attendee
* Attending DebCamp with sponsored accommodation but no suitable work plan
* Staying in our accommodation at your own expense

In this email, you will find the itemized amounts to pay, payment instructions,
and the due date.

PLEASE NOTE:

* This email does NOT account for amounts you may have already paid. If you
  have already paid the total amount below, please accept our thanks and
  disregard this message.

* If you are a Professional or Corporate attendee, accommodation and
  food charges for DebCamp will be charged on a per-day or per-meal basis in
  lieu of the full-week registration fee if you are attending no more than 2
  days of DebCamp or using alternative lodging.

* If you have requested a double room for yourself with no second occupant,
  the $25/night difference in cost will be billed later in a separate notice.

* Per-meal food payments are not being billed at this time. Further
  instructions regarding these payments will be provided once catering
  arrangements are finalized.

==============================================================================

SPECIFIC LINE ITEMS (all prices in US Dollars):

%(items)s

TOTAL AMOUNT YOU MUST PAY: $%(total_cost)d (to the extent not yet paid)

PAYMENT INSTRUCTIONS: http://debconf10.debconf.org/payments.xhtml

DUE DATE: Upon receipt, but no later than July 6

The registration system URL is
  https://penta.debconf.org/penta/submission/dc10/person

If you have any questions or concerns, please contact us via IRC:
  #debconf-team @ irc.oftc.net (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publicly archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

--
The DebConf team
(person_id: %(person_id)4d)
"""

People = dc_objs.get_people(where="dcvn.attend and dcvn.reconfirmed")

mbox = mailbox.mbox("dc10-paymentnotice_20100609.mbox")
mbox.clear() ; mbox.flush()

people_processed = set()


for P in People:

    P.total_cost = 0

    P.items = ""

    for item in P.payment_items():
        # We are not billing for food at this time
        if "food" in item[0].lower():
                continue
        P.total_cost += item[1]
        P.items += "   %-50s $%4d\n"%(item[0], item[1])

    if P.total_cost == 0:
        print "Person has no non-food costs, skipping: %d" % P.person_id
        continue

    people_processed |= set((P.person_id,))

    Body = body % P.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Team", "registration@debconf.org"),
        To=Email(P.name, P.email),
        Subject="DebConf10: Payment Notice",
        Body=Body,
        extraMsgid="dc10paymentnotice1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
