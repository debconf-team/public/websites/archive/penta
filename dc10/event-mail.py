# Richard Darst, June 2010

import mailbox
import time

import dc_util
import dc_objs
from emailutil import Email, Message

body_accepted = """\
Congratulations, %(names)s!

Your proposal,
  %(title)s,
has been accepted to be scheduled for a slot in one of the two main
rooms at DebConf 10. We will let you know the exact location and
schedule on or before June 15th.

In order to verify that you still plan to attend and give your talk,
be sure that you reconfirm your attendance in Penta by June 10th, as
previously announced. So that we can schedule your talk, please be
sure to specify your arrival and departure dates on the Travel tab.%(not_marked_attend)s

Thanks for your contribution to DebConf!

--
The DebConf Talks team
"""

not_marked_attend = """\

As a side note, you are currently not marked as attending DebConf.
You should go to
  https://penta.debconf.org/penta/submission/dc10/person
and at least check
  General -> DebConf -> 'I want to attend this conference'
and
  General -> DebConf -> 'Reconfirm Attendance'.
You should also recheck all other information, since if you aren't
marked attend then you probably didn't get the reconfirmation reminder
email."""


mbox = mailbox.mbox("event-accept1.mbox")
mbox.clear() ; mbox.flush()


people = dc_objs.get_people()
people = dict((p.person_id, p) for p in people )

talks = dc_objs.get_talks()
#talks = [ t for t in talks if

import pentaconnect
db = pentaconnect.get_conn()
c = db.cursor()
c.execute("""
select person_id, email
from person;""")
people_emails = { }
for person_id, email in c.fetchall():
    people_emails[person_id] = email



for talk in talks:

    names = [ ]
    To = [ ]
    #email_to = [ ]
    for event_role in ('Submitter', 'Speaker', 'Coordinator'):
        # We have to check multiple roles in order to 
        if event_role not in talk.people:
            continue
        for person_id, name in talk.people[event_role]:
            if person_id in people:
                p = people[person_id]
                if p.email is None:
                    print p.name
                    continue
                To.append(Email(p.name, p.email))
                names.append(p.name)
            else:
                if people_emails[person_id] is None:
                    print person_id
                    continue
                To.append(Email(None, people_emails[person_id]))
        if len(To) > 0:
            break
    #if len(email_to) == 0:
    #    print talk, "has no people we can email"
    #    continue

    names = ", ".join(names)
    talk.names = names
    if len(names) == 0:
        talk.not_marked_attend = not_marked_attend
    else: talk.not_marked_attend = ""

    # Selection logic: what to do
    if talk.event_state == 'accepted' \
           and ( talk.conference_track != 'Unscheduled'):
        #print talk, talk.conference_track, talk.event_state
        Body = body_accepted%talk.__dict__
        if len(To) == 0:
            print "Talk %s has no people we can email: %s"%(talk.event_id, talk.people)
        print To
    else:
        # No message here
        continue
    #from fitz import interactnow

    

    
    msg = Message(
        From=Email("DebConf Talks Team", "talks@debconf.org"),
        To=To,
        Subject="Your DebConf10 event status",
        Body=Body,
        extraMsgid="dc10events1",  # some string to put in msgid
        #ReplyTo=Email("Dev Null", "devnull@debconf.org"),
        # Anything can besides "From" can be a python list of Email objects.
        Cc=[Email("DebConf10 Talks Team", "talks@debconf.org"),]
#        Bcc=[Email("DebConf10 Talks Team", "talks@debconf.org"),]
        )
    mbox.add(str(msg))
    # Look at the mbox to see if everything looks right.  Send with
    # formail -s /usr/sbin/sendmail -ti < emailutil-test.mbox
