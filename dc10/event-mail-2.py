# Richard Darst, June 2010

import mailbox
import time

import dc_util
import dc_objs
from emailutil import Email, Message

body_accepted = """\
Congratulations, %(name)s!

Your proposal,
  %(title)s,
has been accepted for DebConf 10. We will post a schedule and let you
know the exact location and time on or before June 15th.

So that we can schedule your talk, please be sure to specify your
arrival and departure dates on the Travel tab.%(extra_info)s

If you have any questions, please write to us at
  talks@debconf.org

Thanks for your contribution to DebConf!

--
The DebConf Talks team
"""

body_unscheduled = """\
Hello %(name)s,

Thanks for submitting your proposal event
 %(title)s
for DebConf10.

We appreciate your interest and the time you invested in putting
together your proposed event. As you may know, we had a large number
of submissions for talks, and a limited number of resources. After a
long selection process your event was not selected for this year's
Debconf.

Thank you very much for taking the time to submit your event, we
enjoyed having it as an option.

We hope this doesn't discourage you from attending Debconf, in fact
there will be opportunities for ad-hoc events to occur, if you should
wish to still put it on. Details on how that will work will be
available at the conference.%(extra_info)s

If you have any questions, please write to us at
  talks@debconf.org

Sincerely,

--
The DebConf Talks team
"""

not_marked_attend = """\


As a side note, you are currently not marked as attending DebConf.
You should go to
  https://penta.debconf.org/penta/submission/dc10/person
and at least check
  General -> DebConf -> 'I want to attend this conference'
and
  General -> DebConf -> 'Reconfirm Attendance'.
You should also recheck all other information, since if you aren't
marked attend then you probably didn't get the reconfirmation reminder
email.  Please do this by June 10th."""

not_marked_reconfirmed = """\


As a side note, you are currently not marked as reconfirmed.
You should go to
  https://penta.debconf.org/penta/submission/dc10/person
and check
  General -> DebConf -> 'Reconfirm Attendance'.
You should also recheck all other information and ensure that it is up
to date.  Please do this by June 10th."""

mbox = mailbox.mbox("event-accept1.mbox")
mbox.clear() ; mbox.flush()


people = dc_objs.get_people()
people = dict((p.person_id, p) for p in people )

talks = dc_objs.get_talks()

# Some people are not 
import pentaconnect
db = pentaconnect.get_conn()
c = db.cursor()
c.execute("""
select person_id, email
from person;""")
people_emails = { }
for person_id, email in c.fetchall():
    people_emails[person_id] = email


def make_mails(talk, body):
    numPeopleMailed = 0
    #email_to = [ ]
    for event_role in ('Submitter', 'Speaker', 'Coordinator'):
        # We have to check multiple roles in order to find one that
        # actually has someone to mail.
        if event_role not in talk.people:
            continue
        for person_id, name in talk.people[event_role]:
            person_not_attending = False
            person_not_reconfirmed = False
            if person_id in people:
                p = people[person_id]
                try:
                    email = p.find_email()
                except dc_objs.EmailError:
                    print "%s %s lacking email"%(event_role, p.name)
                    continue
                if not p.reconfirmed:
                    person_not_reconfirmed = True
                To = Email(p.name, p.email)
                name = p.name
            else:
                # Some people aren't marked as attend, which means
                # that everything breaks.
                person_not_attending = True
                if people_emails[person_id] is None:
                    print "This person_id %s can't be found at all"%person_id
                    continue
                To = Email(None, people_emails[person_id])
                name = ""

            if person_not_attending:
                talk.extra_info = not_marked_attend
            elif person_not_reconfirmed:
                talk.extra_info = not_marked_reconfirmed
            else:
                talk.extra_info = ""
            talk.name = name
                
            Body = body%talk.__dict__
            msg = Message(
                From=Email("DebConf Talks Team", "talks@debconf.org"),
                To=To,
                Subject="Your DebConf10 event status",
                Body=Body,
                extraMsgid="dc10events1",  # some string to put in msgid
                #ReplyTo=Email("Dev Null", "devnull@debconf.org"),
                # Anything can besides "From" can be a python list of Email objects.
                Cc=[Email("DebConf10 Talks Team", "talks@debconf.org"),]
                #Bcc=[Email("DebConf10 Talks Team", "talks@debconf.org"),]
                )
            mbox.add(str(msg))
            numPeopleMailed += 1
        if numPeopleMailed > 0:
            break

    if numPeopleMailed == 0:
        print "Talk %s has no people we can email: %s"%(talk.event_id, talk.people)

dont_mail_ids = (641, 645, 639, 646, 644, 605, 599, 530, 534, 533)

for talk in talks:
    print "%4d %15s %15s"%(talk.event_id, talk.event_state, talk.event_state_progress),
    if talk.event_id > 647 or talk.event_id in dont_mail_ids:
        print "excluded via ID"
        continue
    if talk.conference_track == 'Unscheduled':
        print "excluded"
        continue
    
    # Selection logic: what to do
    if talk.event_state == 'accepted':
        print "body_accepted:    %s"%talk.title
        make_mails(talk, body_accepted)

    elif talk.event_state == 'undecided' and talk.event_state_progress == 'candidate':
        print "body_unscheduled: %s"%talk.title
        make_mails(talk, body_unscheduled)
    else:
        # No message here
        print "has no message!"
    #from fitz import interactnow




    

    
