# Richard Darst, May 2010

# select log_timestamp, ldccp.person_id, participant_category from log.dc_conference_person ldccp right join log.log_transaction using (log_transaction_id) left join debconf.dc_view_participant_map dcvpm on (dc_participant_category_id=participant_mapping_id) where ldccp.conference_id=4 and (ldccp.person_id=91) order by log_timestamp desc;

import dc_objs

import pentaconnect
db = pentaconnect.get_conn()
#db = pentaconnect.get_conn('pentatest')
c = db.cursor()

def queryToDict(c):
    #rownames = c.rownames
    rownames = [ x[0] for x in c.description ]
    data = c.fetchall()
    return [ dict(zip(rownames, row)) for row in data ]

c.execute("""
select distinct on (ldccp.person_id)
    dcvn.id as person_id,
    name,
    dcvn.email,
    dcvpm.participant_category
    
from log.dc_conference_person ldccp
    join log.log_transaction llt using (log_transaction_id)
    join debconf.dc_view_participant_map dcvpm on
        (dc_participant_category_id=participant_mapping_id)
    left join person p on (p.person_id=ldccp.person_id)
    left join debconf.dc_view_numbers dcvn on (p.person_id=dcvn.id)
where dcvn.attend
    and ldccp.conference_id=4
    and log_timestamp<'2010-04-16 12:00:00'
order by ldccp.person_id, log_timestamp desc;
;"""%locals())
#c.execute("""
#select distinct on (dcvpc.person_id)
#    person_id,
#    dcvpc.name,
#    email,
#    dcvpc.participant_category
#from debconf.dc_view_participant_changes dcvpc
#    left join debconf.dc_view_numbers dvn on (dvn.id = dcvpc.person_id)
#where dcvpc.attend
#    and log_timestamp<'2010-04-16 04:00:00'
#order by dcvpc.person_id, log_timestamp desc;
#;"""%locals())
peopleBefore = queryToDict(c)
peopleBefore = dict((d['person_id'], d) for d in peopleBefore)


c.execute("""
select distinct on (ldccp.person_id)
    dcvn.id as person_id,
    name,
    dcvn.email,
    dcvpm.participant_category
    
from log.dc_conference_person ldccp
    join log.log_transaction llt using (log_transaction_id)
    join debconf.dc_view_participant_map dcvpm on
        (dc_participant_category_id=participant_mapping_id)
    left join person p on (p.person_id=ldccp.person_id)
    left join debconf.dc_view_numbers dcvn on (p.person_id=dcvn.id)
where dcvn.attend
    and ldccp.conference_id=4
order by ldccp.person_id, log_timestamp desc;
;"""%locals())
#c.execute("""
#select distinct on (dcvpc.person_id)
#    person_id,
#    dcvpc.name,
#    email,
#    dcvpc.participant_category
#from debconf.dc_view_participant_changes dcvpc
#    left join debconf.dc_view_numbers dvn on (dvn.id = dcvpc.person_id)
#where dcvpc.attend
#order by dcvpc.person_id, log_timestamp desc;
#;"""%locals())
peopleNow = queryToDict(c)
peopleNow = dict((d['person_id'], d) for d in peopleNow)

for pid in peopleNow:
    new = peopleNow[pid]

    # skip all people now in unsponsored categories, they never need
    # to be worried about.
    if new['participant_category'] in dc_objs.CAT_UNSPONS:
        continue
    # Print new people's information
    if pid not in peopleBefore:
        print "(%d) %s <%s>"%(pid, new['name'], new['email'])
        print "   ", "(New person)"
        print "   ", new['participant_category']
        continue

    old = peopleBefore[pid]
    if old['participant_category'] == new['participant_category']:
        continue

    print "(%d) %s <%s>"%(pid, new['name'], new['email'])
    print "   ", old['participant_category']
    print "   ", new['participant_category']


