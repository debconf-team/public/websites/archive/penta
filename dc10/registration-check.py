# Richard Darst, April 2010

import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


people = errorcheck.get_rows()
People = dc_objs.get_people()
[ P.add_errors(errorcheck.Errors) for P in People ]
People = [ P for P in People if len(P.errors) > 0 ]
#for d in people:
#    p = errorcheck.Person(Errors=errorcheck.Errors, **d)
#    if len(p.errors) == 0:
#        continue
#    People.append(p)

beginning = """\
%(name)s,

This (automated) message is being sent to attendees to let you know of
any errors in the registration, or other things which you should know
about.  Please look over the items below, and correct any problems
which may be present.  Some things are just warnings or notes, and
won't apply to most people.  These are clearly labeled, if one doesn't
apply to you, please ignore it.

Because this is an automated message, perhaps there are false
positives on the errors or warnings listed here.  If you know that
something does not apply to you or you have contacted us previously,
please disregard.  If you have any questions, please contact us via
IRC:
  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

The registration system URL is
  https://penta.debconf.org/penta/submission/dc10/person
The notes below each item give hints on where to look in our
registration for the relevant fields for each item.

Please check everything is updated by April 15th, the sponsorship
deadline!  Even if you aren't being sponsored, registration estimates
are required for sponsorship and budgeting planning."""

ending = """\
Thank you,

-- 
Richard Darst
On behalf of the DebConf team
"""

body = """%(beginning)s

Information about your registration
===================================

%(info)s

Registration errors and warnings
================================

%(errors)s


%(ending)s
"""

#mbox = mailbox.mbox("dc10-registration.mbox")

def make_items(messages):
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages

for person in People:
    Errors = [e for e in person.errors
              if e.__class__.__name__ not in ('RegistrationCheck',
                                              'ThirteenYearOldAgeLimit',
                                              'DoubleCheckTravelSponsorshipNumbers') ]
    person.errors = Errors

    if len(Errors) > 0:
        print '='*40
        print person.person_id, person.name
        print person._info()

    info   = [ e for e in Errors if e.level=='info' ]
    errors = [ e for e in Errors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"
    
    Body = body%locals()
    Body = Body%person.__dict__
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("Richard Darst", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf10 registration check (action by 15 April)",
        Body=Body,
        extraMsgid="dc10check1",
        ReplyTo=Email("DebConf registration", "registration@debconf.org"),
        Cc=Email("DebConf registration", "registration@debconf.org"),
        )
    #mbox.add(str(msg))
