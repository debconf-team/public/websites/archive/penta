import collections
import time

import dc_util
import dc_objs
#import pentaconnect
#db = pentaconnect.get_conn()
#c = db.cursor()

where = 'attend'


print time.ctime()
print

import dc_objs

unsponsored_ids = open("dc10/sponsorship-not-yet-2010-06-01").read().split()
unsponsored_ids = set(int(x) for x in unsponsored_ids)

removed_ids = open("dc10/sponsorship-removed").read().split()
removed_ids = set(int(x) for x in removed_ids)


people = dc_objs.get_people()
people = [ p for p in people if p.person_id in unsponsored_ids ]
people = [ p for p in people if p.person_id not in removed_ids ]

print "Pending penta IDs:", tuple(sorted(unsponsored_ids))
print "Removed penta IDs:", tuple(sorted(removed_ids))
print
print



for P in people:
    print "="*20
    print "(%4d) %s (%s)"%(P.person_id, P.name, P.email)
    print "Status:     ", P.status
    print "Category:   ", P.participant_category
    print "Accom:      ", P.accom
    print "Food:       ", P.food
    print "Reconfirmed:", P.reconfirmed
    if True: # debcamp status (blocked for ease of reading only)
        if P.debcamp in dc_objs.DC_NOPLAN:
            print "DebCamp:     Yes, No workplan"
        elif P.debcamp in dc_objs.DC_PLAN:
            print "DebCamp:     Yes, Has workplan"
        else:
            print "DebCamp:     No, ", P.debcamp
    print "Dates:       %s - %s"%(P.arrival_date, P.departure_date)
    print

    print dc_util.wrap_text(P.debianwork)
    print

    if P.debcamp_reason:
        print "Debcamp Reason:"
        print
        print dc_util.wrap_text(P.debcamp_reason)
        print
    print
    print
