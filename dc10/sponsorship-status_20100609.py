# Richard Darst, May 2010

import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message

from dc_objs import CAT_BAF, CAT_BA, CAT_BF, DC_NO, DC_NOPLAN, DC_PLAN
from dc_objs import CAT_SPONS


body = """\
Hello %(name)s,

%(items)s

The registration system URL is
  https://penta.debconf.org/penta/submission/dc10/person

If you have any questions, please contact us via IRC:
  #debconf-team @ irc.oftc.net (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

classes = {
    # Case A: yes debconf, no debcamp
    "A":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 10 in New York City!

However, we are sorry to say that we can not grant you the requested
sponsored food and accommodation during DebCamp which precedes DebConf
itself. We have set your registration status to \"DebCamp without
workplan - $650/week\", if you would not like to attend DebCamp,
please change this to \"I won't be attending DebCamp\" (link below).

You have also been approved for food sponsorship during DebConf
itself, and it is very likely that we will also be able to also
provide it. However we are unable to commit to food sponsorship
requests yet. We are working hard on continued fund-raising efforts
and hope to be able to commit to this soon.

If, in the unlikely event that we are unable to offer food
sponsorship, you will be able to purchase meals at cost, likely
$10-$15 per meal, or obtain food locally on your own.""",
         lambda p: (p.participant_category in CAT_BAF
                    and p.debcamp not in DC_PLAN)
         ),
    # Case B: Granted all
    "B":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 10 and DebCamp (if you applied for DebCamp) in New
York City!

You have also been approved for food sponsorship, and it is very
likely that we will also be able to also provide it. However we are
unable to commit to food sponsorship requests yet. We are working hard
on continued fund-raising efforts and hope to be able to commit to
this soon.

If, in the unlikely event that we are unable to offer food
sponsorship, you will be able to purchase meals at cost, likely
$10-$15 per meal, or obtain food locally on your own.""",
         lambda p: (p.participant_category in CAT_BAF
                    and p.debcamp not in DC_NOPLAN)
         ),
    # Case C: asked only food, got it
    "C":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored food for the upcoming
DebConf 10 and DebCamp (if you applied for DebCamp) in New York City!""",
         lambda p: (p.participant_category in CAT_BF
                    and p.debcamp not in DC_PLAN)
         ),
    # Case D: asked only accom, got it
    "D":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 10 and DebCamp (if you applied for DebCamp) in New
York City!""",
         lambda p: (p.participant_category in CAT_BA
                    and p.debcamp not in DC_PLAN)
         ),
    # Case E: denied
    "E":("""\
We are sorry to inform you that after reviewing the information you
provided, we are not able to grant you sponsored food and accommodation
for the upcoming DebConf 10 in New York City.

We do hope you still will be able to attend DebConf. If this is still
the case, please go to your pentabarf page (link below) and update the
\"Category\" field with the correct information (if you will get
accommodation and food on your own, or if you will pay and stay with
others attendees at Columbia)""",
         lambda p: (p.participant_category not in CAT_SPONS
                    and p.debcamp not in DC_PLAN) ),

    
    }

people = dc_objs.get_people()
mbox = mailbox.mbox("dc10-sponsorstatus_20100609.mbox")
mbox.clear() ; mbox.flush()

# defines person_classes:
execfile("dc10/sponsorship-status_20100609.dat", globals(), locals())

people_processed = set()

for person in people:
    # skip unsponsored and prof/corp.
    items = [ ]
    for classname in classes:
        if person.person_id in person_classes[classname]:
            items.append(classes[classname][0])
            # Check to make sure it works properly:
            if not classes[classname][1](person):
                print "check", person.person_id, classname
    if len(items) > 1:
        print "Person is in too many classes:", person.person_id
    if len(items) == 0:
        continue
    people_processed |= set((person.person_id,))

    person.items = "\n\n".join(items)

    Body = body % person.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf10: Sponsorship status",
        Body=Body,
        extraMsgid="dc10sponsorshipstatus1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))

print "Checking for missed people:"
for classname in classes:
    if len(set(person_classes[classname]) - people_processed) > 0:
        print classname, "missed people", \
              set(person_classes[classname]) - people_processed
