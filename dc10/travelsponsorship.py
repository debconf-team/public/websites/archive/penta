# Richard Darst, May 2010

from emailutil import Email, Message
import mailbox
import sys
sys.path.append('.')

import emailutil
print emailutil.__file__

body_sponsored = """Hello %(name)s,

You requested travel sponsorship for the upcoming DebConf10 in New
York.
  Total cost:       USD %(total_cost)s
  Amount requested: USD %(amount_requested)s

We are happy to inform you that we have reviewed your request and
consider it eligible for support.

However, we are not positive that we will have enough money for
sponsorships.  While we will try our best to make it possible, there
is no guarantee that we will get enough funding, in fact right now the
money situation is quite tight.

After much discussion, we have created two queues of people, so you
know how things look for you. Your current queue and position are:
  Queue:    %(queue)s
  Position: %(queue_position)s

Queue description:
  Queue A: - we will work on fulfilling the requests.
  Queue B: - we will work on fulfilling the requests but not necessarily
             at 100%% (although we will try hard to work with you to
             ensure your attendance).

To make it more likely that we will be able to fulfill your request
(as well as everybody else's), please make sure the amount you have
requested is the minimum you /really/ need in order to join us at
DebConf.

Finally, this is a great opportunity to mention that DebConf needs help with
its fundraising efforts!  Just stop by #debconf-team on irc.debian.org and ask
to get invited to #debconf-sponsors. Also, if you know anyone able to make a
financial contribution, please consider pointing them to the donation
information here: http://debconf10.debconf.org/payments.xhtml

Thank you,

-- 
The DebConf10 Travel Sponsorship Team
"""

body_unsponsored = """%(name)s,

You had requested travel sponsorship for the upcoming DebConf10 in New
York.
  Total cost:       USD %(total_cost)s
  Amount requested: USD %(amount_requested)s

At this time we do not have the budget to accommodate all the requests
for sponsorship we have received and we regret to inform you that we
are unable to grant your request.

We hope you can arrange your travel through alternative funding
channels, and look forward to seeing you at DebConf10.

Thank you,

-- 
The DebConf10 Travel Sponsorship Team
"""

if len(sys.argv) < 3:
    print "Usage: %s <input_csv> <output_mbox>"%sys.argv[0]
    exit(1)
datfile = open(sys.argv[1])
mbox = mailbox.mbox(sys.argv[2])


for line in datfile:
    queue, name, email, queue_position, total_cost, amount_requested = \
           line.split(',')

    if queue in ("A", "B"):
        Body = body_sponsored%locals()
    elif queue in ("C", ):
        Body = body_unsponsored%locals()
    else:
        raise Exception("Person %s has unknown queue %s"%(name, queue))
        
    msg = Message(
        # The Email(real_name, email_address) object aids in
        # making email addresses.  If you don't have a real name,
        # use it as Email(None, email_address)
        From=Email("DebConf10 Travel Sponsorship Team", "herb@debconf.org"),
        To=Email(name, email),
        Subject="DebConf10 Travel Sponsorship",
        Body=Body,
        extraMsgid="dc10travelsponsorship1",  # some string to put in msgid
        ReplyTo=Email("DebConf10 Travel Sponsorship Team", "herb@debconf.org"),
        # Anything can besides "From" can be a python list of Email objects.
        Cc=[Email("DebConf10 Travel Sponsorship Team", "herb@debconf.org"),],
        )
    mbox.add(str(msg))
    # Look at the mbox to see if everything looks right.  Send with
    # formail -s /usr/sbin/sendmail -ti < emailutil-test.mbox
