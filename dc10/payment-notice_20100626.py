# Jimmy Kaplowitz, June 2010, based on Richard Darst's work

import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message

body = """\
Hello %(name)s,

We see that your are staying in our accommodations and/or have a
required payment.  We need this payment by JULY 6th in order to
reserve accommodations for you.

Required advance payments (Accom/Corp/Prof):

%(items)s

Right now, we see you as having paid:
  $ %(amount_paid)7.2f USD

The amount you need to pay total is:
  $ %(amount_to_pay)7.2f USD

%(paymentmemo)s

If you have any questions about this, please mail
  registration@debconf.org.

The registration system URL is
  https://penta.debconf.org/penta/submission/dc10/person

=====

- If you are paying for food by-day, those costs are not included
  here.  These costs will be reconciled later.

- There may be updates in required payments as we finalize our exact
  arrangements.

- FFIS payments may have up to a week delay before appearing to us, if
  you do not see your FFIS payment reflected above please wait a week
  or so before contacting us.

- We are including all corporate and professional attendees in this
  email, even if you are not staying in our accommodations.  Reasons
  for getting this mail should include:
  * Registration as a Professional or Corporate attendee
  * Attending DebCamp with sponsored accommodation but no suitable work plan
  * Staying in our accommodation at your own expense

- Payments do _not_ automatically appear in Pentabarf.


Please do not hesitate to contact us with any questions.

Thank you,

--
The DebConf team
(person_id: %(person_id)4d, %(amount_to_pay)7.2f, %(amount_paid)7.2f)
"""

People = dc_objs.get_people(where="dcvn.attend and dcvn.reconfirmed")

mbox = mailbox.mbox("dc10-paymentnotice_20100626.mbox")
mbox.clear() ; mbox.flush()

people_processed = set()


for P in People:

    P.total_cost = 0

    P.items = ""
    if not P.get_total_cost_noFood():
        continue

    for item in P.payment_items_noFood():
        # We are not billing for food at this time
        #P.total_cost += item[1]
        P.items += "  $ %4d  %-50s \n"%(item[1], item[0])

    if P.amount_to_pay != P.get_total_cost_noFood():
        #print P.amount_to_pay, P.get_total_cost_noFood()
        print "Errr: (%4d) %4s, %4f, %4s"%(P.person_id, P.amount_to_pay,
                                           P.get_total_cost_noFood(),
                                           P.amount_paid)
        P.items = "  $ %4d  Custom DebConf and/or DebCamp Registration\n"%P.amount_to_pay

    # This is now handled above
    #if P.total_cost == 0:
    #    print "Person has no non-food costs, skipping: %d" % P.person_id
    #    continue

    # Default memo saying how to pay
    P.paymentmemo = """\
You must pay by JULY 6th or else we will not be able to reserve
accommodations for you.

For payment instructions, please see
  http://debconf10.debconf.org/payments.xhtml
"""

    # Thanking them for paying.
    if P.amount_to_pay == P.amount_paid:
        print "Paid: (%4d) %4d %4d %s "%(P.person_id, P.amount_to_pay,
                                         P.amount_paid, P.name)
        P.paymentmemo = """\
Thank you for paying.  We look forward to seeying at DebConf."""

    people_processed |= set((P.person_id,))

    if P.amount_paid is None: P.amount_paid = 0
    if P.amount_to_pay is None: P.amount_to_pay = 0

    Body = body % P.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Team", "registration@debconf.org"),
        To=Email(P.name, P.email),
        Subject="DebConf10: Payment Information",
        Body=Body,
        extraMsgid="dc10paymentnotice1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
