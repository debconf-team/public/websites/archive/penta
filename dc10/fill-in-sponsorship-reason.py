# Richard Darst, May 2010

import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message


body = """\
%(name)s,

The DebConf team is reviewing the attendees signed up for food or
accommodation sponsorship.  In order to help us, could you please log
in to Pentabarf at
  https://penta.debconf.org/penta/submission/dc10/person
and go to
  Travel -> 'What are you doing for Debian?'
and fill in your work, not just with Debian but with the broader free
software community.

Again, be broad here!  We want as many interested community members to
attend, so let us know what your other projects are.  If you are just
interested in Debian, that's fine too - let us know why you are
interested.

If you are in any way connected to Debian or interested, don't worry -
it is almost guaranteed that you'll get sponsorship.

Thank you,

-- 
The DebConf team
"""

people = dc_objs.get_people()
mbox = mailbox.mbox("dc10-fillinsponsorship.mbox")


for person in people:
    # skip unsponsored and prof/corp.
    if p.participant_category in dc_obsj.CAT_B|dc_obsj.CAT_PROF|dc_obsj.CAT_CORP:
        continue

    Body = body % person.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf10: Please fill in sponsorship information",
        Body=Body,
        extraMsgid="dc10fillinsponsorship1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))
