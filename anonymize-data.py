# Richard Darst, December 2009

import collections
import os
import random
import re
import string
import sys

##
## Configure here
##

# Look at pentaconnect.py for information on how to set this up so
# that you can automatically connect.
#
# The name= argument specifies the name of the database you want to
# connect to.  You can adjust this if you want to connect to a
# different database (see pentaconnect.py for info).
import pentaconnect
db = pentaconnect.get_conn(name='pentatest') #never run this on production!

# Set this to False to do make it run the UPDATE commands.
dryRun = True
runClear = True
runTruncate = True

##
## Probably don't need to look below here
##

# These are all generators that return random data to be used for
# replacing columns.
def email():
    return 'pentatest@debconf.org'
def money():
    return int(random.uniform(10, 500))
def address():
    return "%s Main Street"%int(random.uniform(10, 500))
def phone():
    return "+%02d %04d-%04d-%04d"%(random.randint(10,99),random.randint(0,999),
                                   random.randint(0,999),random.randint(0,9999)
                                   )
def url():
    return "http://example.com/"
def text():
    return "".join([random.choice(string.ascii_lowercase) for x in range(10) ])
def firstname():
    return random.choice(('Alice','Bob','Carol','Dave','Eve','Isaac',
                          'Justin', 'Mallory', 'Oscar', 'Pat', 'Victor',
                          'Steve', 'Trent', 'Walter', 'Zoe'))
def lastname():
    return random.choice(('Smith', 'Brown', 'Jones'))
def name():
    return " ".join((firstname(), lastname()))
def nick():
    return "".join([random.choice(string.ascii_lowercase) for x in range(6) ])
class randint:
    def __init__(self, *args): self.args = args
    def __call__(self): return random.randint(*self.args)
postcode = randint(10000,99999)
    
def bool_():
    return random.choice((True, False))
def null():
    return None

# Some personids are missing, so we need to be able to select only
# valid ones properly.
c = db.cursor()
c.execute('select person_id from person;')
personid_all = [x[0] for x in c.fetchall()]
c.close()
def personid():
    return random.choice(personid_all)

# evaluator_id in the person_rating table needs to be unique (is part
# of the primary key)!  There are a lot of constraints here, and thus
# a lot of code handling it, making sure that we don't try to set a
# non-unique or invalid primary key.
c = db.cursor()
c.execute('select person_id,evaluator_id from person_rating;')
person_evaluator_d = collections.defaultdict(set)
for person_id, evaluator_id in c.fetchall():
    person_evaluator_d[person_id].add(evaluator_id)
c.close()
def personevaluatorid(keys_d):
    person_id = keys_d['person_id']
    all_people = set(personid_all)
    #print "person_id", person_id
    #print person_evaluator_d[person_id] #who has already evaluated the person?
    available_evaluators = all_people - person_evaluator_d[person_id]
    if len(available_evaluators) == 0:
        raise Exception("No available evaluators for person %d"%person_id)
    evaluator_id = random.choice(tuple(available_evaluators))
    person_evaluator_d[person_id].add(evaluator_id)
    #print evaluator_id
    return evaluator_id

# Some mails have to be unique.  This hashes the primary keys:
unique_mails = set()
def email_unique(keys_d):
    return 'pentatest+%d@debconf.org'%hash(tuple(keys_d.items()))
def loginname(keys_d):
    name = "".join((firstname(), lastname()))
    name = name + "%d"%keys_d['account_id']
    return name.lower()
        


# Exhaustive list of columns which need to be replaced.
clearColumns = [
    ('conference_person.email', email),
    ('conference_person.abstract', text),
    ('conference_person.description', text),
    ('conference_person_link.url', url),
    ('conference_person_link.title', text),
    
    ('conference_person_travel.arrival_from', text),
    ('conference_person_travel.arrival_to', text),
    ('conference_person_travel.arrival_number', text),
    ('conference_person_travel.departure_from', text),
    ('conference_person_travel.departure_to', text),
    ('conference_person_travel.departure_number', text),
    ('conference_person_travel.travel_cost', money),
    ('conference_person_travel.accommodation_cost', money),
    ('conference_person_travel.accommodation_name', text),
    ('conference_person_travel.accommodation_street', text),
    ('conference_person_travel.accommodation_postcode', postcode),
    ('conference_person_travel.accommodation_city', text),
    ('conference_person_travel.accommodation_phone', phone),
    ('conference_person_travel.accommodation_phone_room', phone),
    ('conference_person_travel.fee', money),
    ('conference_person_travel.need_travel_cost', bool_),
    ('conference_person_travel.need_accommodation', bool_),
    ('conference_person_travel.need_accommodation_cost', bool_),

    ('event.submission_notes', text),

    ('event_rating.relevance',  randint(1,5)),
    ('event_rating.actuality',  randint(1,5)),
    ('event_rating.acceptance', randint(1,5)),
    ('event_rating.remark',     text),


    ('person.gender'         , bool_),
    ('person.first_name'     , firstname),
    ('person.last_name'      , lastname),
    ('person.public_name'    , name),
    ('person.nickname'       , name),
    ('person.email'          , email),
    ('person.address'        , address),
    ('person.street'         , address),
    ('person.street_postcode', randint(10000,99999)),
    ('person.po_box'         , randint(1000,9999)),
    ('person.po_box_postcode', randint(10000,99999)),
    ('person.city'           , text),
    ('person.iban'           , randint(1e9,1e10)),
    ('person.bic'            , randint(1e9,1e10)),
    ('person.bank_name'      , text),
    ('person.account_owner'  , name),

#    ('person_availability.person_id', personid),

    # see person.* below, for person.* + person_logging.* replacements
    ('person_logging.password', text),
    ('person_logging.email_contact', email),
    ('person_logging.address', address),
    ('person_logging.street', address),
    ('person_logging.street_postcode', randint(10000,99999)),
    ('person_logging.po_box', randint(1,500)),
    ('person_logging.po_box_postcode', randint(10000,99999)),
    ('person_logging.city', text),
    ('person_logging.email_contact', address),
    ('person_logging.iban', randint(1e9,1e10)),
    ('person_logging.bic', randint(1e9,1e10)),
    ('person_logging.bank_name', text),
    ('person_logging.account_owner', name),
    #('person_logging.gpg_key', ),
    #('person_logging.preferences', ),
    #('person_logging.last_login', ),
    #('person_logging.last_modified', ),
    ('person_logging.emergency_contact', phone ),
    ('person_logging.emergency_name', name ),

#    ('person_im.person_id', text)
    ('person_im.im_address', text),

    ('person_image.image', text),  # xxx should be binary type

#    ('person_language.{person_id|language}', text),

    ('person_phone.phone_number', phone),

    ('person_rating.evaluator_id', personevaluatorid, {'keyArgs':True}),
                                              # altering primary keys *could*
                                              # work but requires care.
    ('person_rating.speaker_quality', randint(1,5)),
    ('person_rating.competence', randint(1,5)),
    ('person_rating.remark', text),

    # person_transaction lists when things were changed, but no
    # details about what.

#    ('radcheck.username', text),
    ('radcheck.value', text),  # seems to be password hashes

    # reconfirm_day is a copy of a table - should be stomped

    ('reconfirm_day.email', email),
    ('reconfirm_day.travel_cost', money),
    ('reconfirm_day.cant_fund', money),

    ('debconf.dc_conference_person.paid_amount', money),
    ('debconf.dc_conference_person.dc7_paid_for_days', randint(1,10)),
    ('debconf.dc_conference_person.t_shirt_sizes_id', randint(1,5)),
    ('debconf.dc_conference_person.debcamp_reason', text),
    ('debconf.dc_conference_person.room_preference', text),
    ('debconf.dc_conference_person.amount_to_pay', money),
    ('debconf.dc_conference_person.debianwork', text),

    ('debconf.dc_person.emergency_name', name),
    ('debconf.dc_person.emergency_contact', phone),

    ('debconf.dc_press.association', text),
    ('debconf.dc_press.name', name),
    ('debconf.dc_press.email', email_unique, {'keyArgs':True}),
    ('debconf.dc_press.phone', phone),
    ('debconf.dc_press.street', address),
    ('debconf.dc_press.zip', randint(10000,99999)),
#    ('debconf.dc_press.city', ),
    ('debconf.dc_press.fax', phone),
    ('debconf.dc_press.url', url),
    ('debconf.dc_press.id_check', text),
    ('debconf.dc_press.disabled', bool_, {'replaceAll':True}),


    # Auth schema
    ('auth.account.login_name', loginname, {'keyArgs':True}),
    ('auth.account.email', email),
    ('auth.account.salt', text),
    ('auth.account.password', text),
#    ('auth.account.person_id', personid),

#    ('auth.account_role.role')

    #('', ),
    #('', ),
    ]

# These tables should be completly truncated.  Note that these should go in order from first truncated (
truncateTables = [
    'person_logging',
    'person_image_logging',
    'conference_person_logging',
    'debconf.dc_person_damn_cold',
    'debconf.dc_person_data_frozen',
    'reconfirm_day', # This seems to be a backup copy of some table,
                     # presumably made on the reconfirmation deadline.

    'auth.account_activation',
    'auth.account_password_reset',
    'radcheck',
    ]

    
pkeyTable = {'conference_person': 'conference_person_id',
             'conference_person_travel': 'conference_person_id',
             'conference_person_link':'conference_person_link_id',
             'event': 'event_id',
             'event_rating': ('person_id', 'event_id'),
             'person': 'person_id',
             'person_logging': 'person_logging_id',
             'person_language': ('person_id', 'language'),
             'person_im': 'person_im_id',
             'person_image': 'person_id',
             'person_phone': 'person_phone_id',
             'person_rating': ('person_id','evaluator_id'),
             'debconf.dc_person_data_frozen': 'id',
             'debconf.dc_person_damn_cold': 'id',
             'radcheck': 'id',
             'reconfirm_day': 'id',
             'debconf.dc_conference_person': ('person_id','conference_id'),
             'debconf.dc_person': 'person_id',
             'debconf.dc_person_damn_cold': 'id',
             'debconf.dc_person_data_frozen': 'id',
             'debconf.dc_press': 'press_id',

             'auth.account': 'account_id',
             }



#if not dryRun:
#    c = db.cursor()
#    c.execute('SELECT * from log.activate_logging();')
#    db.commit()
#    c.close()

if runClear:
    for clearData in clearColumns:
        column = clearData[0]
        replace_func = clearData[1]
        # If there is a third argument, it is a dictionary, and has
        # various options specific to this column.
        if len(clearData) > 2:
            otherParams = clearData[2]
        else:
            otherParams = { }
        print 'Running column:', column
    
        # Break up into table and column name, find the primary keys for
        # the column
        table, column = column.rsplit('.', 1)
        # There can be multiple primary keys.  If this is the case, we
        # have to handle this specially.
        primaryKeys = pkeyTable[table]
        if isinstance(primaryKeys, tuple):
            pkeyLookup = ','.join(primaryKeys)
            pkeySet = ' and '.join(('%s=%%(key%d)s'%(column,i))
                                   for i,column in enumerate(primaryKeys))
        else:
            pkeyLookup = primaryKeys
            pkeySet = '%s=%%(key0)s'%primaryKeys
            primaryKeys = (primaryKeys, )
    
        # new value
        newValueGenerator = clearData[1]

        # Replacements 
        repl = {'column':column,
                'table':table,
                'pkeyLookup':pkeyLookup,
                'pkeySet':pkeySet}

        # For some things, we want to replace *all* values, not just
        # non-null rows:
        if 'replaceAll' in otherParams:
            select = """SELECT %(pkeyLookup)s,%(column)s FROM %(table)s
            ;"""%repl
        else:
            select = """SELECT %(pkeyLookup)s,%(column)s FROM %(table)s
            WHERE %(column)s IS NOT null ;"""%repl
    
        c = db.cursor()
        c.arraysize = 2000
        c.execute(select)
        rows = c.fetchall()
        
        # Each row contains a list of the primary keys, ended by the
        # old value.  Go thorugh and make the replacements 
        rowReplacements = [ ]
        for row in rows:
            rowRepl = { }
            rowReplacements.append(rowRepl)
    
            keys, oldValue = row[:-1], row[-1]
            keys_d = dict(zip(primaryKeys, keys))
            
            for i,keyValue in enumerate(keys):
                rowRepl['key%d'%i] = keyValue
            # Some replace functions need to know what we are replacing:
            if 'keyArgs' in otherParams:
                rowRepl['newval'] = replace_func(keys_d)
            else:
                rowRepl['newval'] = replace_func()
    
        # Set certain things (like column names) via normal Python
        # string formatting.  The actual parameters to replace
        # (e.g. key0, newVal), are then substitud in using the pgdb
        # replacement (with SQL quoting) operations.
        update = """UPDATE %(table)s
        SET %(column)s=%%(newval)s
        WHERE %(pkeySet)s"""%repl
        
        # Debug status information:
        print re.sub('\s{2,}', ' ', update)
        if rowReplacements:    print rowReplacements[0]
        else:                  print rowReplacements

        if not dryRun:
            c.executemany(update, rowReplacements)
            print "... done."
        db.commit()
        c.close()
        print '-'*10


# truncate logs
# 15:57 < Ganneff> for the log zapping, you want a TRUNCATE log.* (you have
#  to name the tables, but truncate is fast)
if runTruncate:
    c = db.cursor()

    # "*_logging" in the main namespace
    c.execute("select tablename from pg_tables ;")
    logtables = c.fetchall()
    logtables = [ x[0] for x in logtables ]
    #print [ x for x in logtables if 'log' in x]
    logtables = [ x for x in logtables if x[-8:]=='_logging' ]
    #print logtables
    # XXX: should we truncate these, too?
    truncateTables.extend(logtables)
    

    # All tables in "log.*"  We want to do these last of all.
    c.execute("select tablename from pg_tables where schemaname='log' ;")
    logtables = c.fetchall()
    logtables = [ x[0] for x in logtables ]
    logtables = [ "log.%s"%table for table in logtables ]
    truncateTables.extend(logtables)
    
    # Do the truncation
    for table in truncateTables:
        truncate = "truncate %s cascade;"%table
        print truncate
        if not dryRun:
            c.execute(truncate)

    #print "GRANT TRUNCATE ON TABLE %s TO mrbeige;"%', '.join(truncateTables)
    c.close()
    db.commit()

