import logging
import mechanize
import random
import re
import sys
import dc_objs
from BeautifulSoup import BeautifulSoup
from ClientForm import compress_text

FRONTACCOUNTING_URL = "https://debconf10.kaplowitz.org/account"
HTTP_USERNAME = "XXXXXX"
HTTP_PASSWORD = "XXXXXX"
APP_USERNAME = "XXXXXX"
APP_PASSWORD = "XXXXXX"

CAT_MAPPINGS = { }
CAT_MAPPINGS.update(dict.fromkeys(dc_objs.CAT_B, 'Pays Own Bed & Food'))
CAT_MAPPINGS.update(dict.fromkeys(dc_objs.CAT_BA, 'Sponsored Bed Only'))
CAT_MAPPINGS.update(dict.fromkeys(dc_objs.CAT_BF, 'Sponsored Food Only'))
CAT_MAPPINGS.update(dict.fromkeys(dc_objs.CAT_BAF, 'Sponsored Bed & Food'))
CAT_MAPPINGS.update(dict.fromkeys(dc_objs.CAT_PRO, 'Professional'))
CAT_MAPPINGS.update(dict.fromkeys(dc_objs.CAT_COR, 'Corporate'))

def clean_html(br):
        data = br.response().get_data()
        soup = BeautifulSoup(data)
        response = mechanize.make_response(str(soup), 
          [("Content-Type", "text/html; charset=utf-8")], br.geturl(), 200, "OK")
        br.set_response(response)

# Implements the weird character conversion/encoding specified at
# http://en.dklab.ru/lib/JsHttpRequest/protocol.html. Yay JavaScript weirdness!
# Assumes txt is already 8-bit-encoded UTF-8.
def js_enc(txt):
        utxt = unicode(txt, "utf-8")
        ret = ""
        for c in utxt:
                if ord(c) > 127:
                        ret += "%%u%04X" % ord(c)
                elif c == u'+':
                        ret += "%%2B"
                # The next case checks for URL-allowed characters
                elif c.lower() in u"0123456789abcdefghijklmnopqrstuvwxyz$-_.+!*'()":
                        ret += "%c" % unicode.encode(c)
                else:
                        ret += "%%%02X" % ord(c)
        return ret

def ajax_submit(br, name=None):
        br.form.action += "?JsHttpRequest=0-xml"
        req = mechanize.Request(br.form.action)
        req.add_header('Content-Type', 'application/octet-stream; charset=utf-8')
        pairs = br.form.click_pairs()
        body = "&".join(["%s=%s" % (js_enc(k),js_enc(v)) for (k,v) in br.form.click_pairs(name=name)])
        if not "&submit=" in body:
                body += "&submit=1"
        body += "&_random=%f" % (random.random() * 1234567)
        req.add_header('Content-Length', len(body))
        req.add_data(body)
        br.open(req)

def ajax_select(br, label, name):
        br.set_value_by_label(label, name=name)
        update_name = "_%s_update" % name
        br.submit(name=update_name)
        clean_html(br)
        br.select_form(nr=0)

br = mechanize.Browser()
br.add_password(FRONTACCOUNTING_URL, HTTP_USERNAME, HTTP_PASSWORD)
br.open(FRONTACCOUNTING_URL)

br.select_form(name="loginform")
br["user_name_entry_field"] = APP_USERNAME
br["password"] = APP_PASSWORD
br.submit()

People = dc_objs.get_people(where="dcvn.attend and dcvn.reconfirmed")

for P in People:
        br.open(FRONTACCOUNTING_URL)
        br.follow_link(text="Add and Manage Customers")

        br.select_form(nr=0)
        br["CustName"] = compress_text(P.name[0:40])
        br["cust_ref"] = compress_text(P.name[0:30])
        br["email"] = compress_text(P.email)
        br["notes"] = "person_id: %d" % P.person_id
        br.set_value_by_label([CAT_MAPPINGS[P.participant_category]], name="sales_type")
        br.set_value_by_label(["Net 30"], name="payment_terms")
        br.set_value_by_label(["Conference Registration"], name="dimension_id")

        ajax_submit(br)

        br.open(FRONTACCOUNTING_URL)
        br.follow_link(text="Customer Branches")
        clean_html(br)
        br.select_form(nr=0)
        ajax_select(br, [compress_text(P.name[0:30])], name="customer_id")
        ajax_submit(br, name="ADD_ITEM")

# This part isn't working
#br.open(FRONTACCOUNTING_URL)
#br.follow_link(text="Direct Invoice")
#clean_html(br)
#br.select_form(nr=0)
#ajax_select(br, ["Test Monkey"], name="customer_id") #XXX NAME
##ajax_select(br, ["Residence Hall Bed"], name="stock_id") #XXX line_item
##br["qty"] = 6 #XXX line_item
