#!/usr/bin/python
# -*- coding: utf8 -*-

# generic interface to debconf data in summit or penta 

import sys
import os
import codecs
import datetime

# let's handle timezone
import pytz
# (status 2014) pgdb still don't support indexing fields by fieldnames, so we do it "manually"
import pgdb
# also this is stupid, but needed if we pipe outputs (and also in case of being in C locale).
sys.stdout = codecs.getwriter('utf8')(sys.stdout)


def init(debconf_number=14, timezone="US/Pacific", database=None):
    "mandatory initialization of the module"

    global db, c, dc, tz_str, db_engine, sql_this_conference, utc, local_time
    dc = debconf_number
    tz_str = timezone

    if dc >= 14:
	db_engine=2  # summit
        sql_this_conference = "summit_id=%d" % (dc-13)
        if not database:
	    database = "summit"
	db = pgdb.connect(database=database)
	c = db.cursor()

    elif dc >= 7:
	db_engine=1  # penta
        if not database:
            database = "penta"

    else:
	db_engine=0  # none

    utc = pytz.utc
    local_time = pytz.timezone(timezone)



def get_persons(WHERE=None, ORDER=None):
    "conference person, return a tuple: dictionary of persons, and a list of ordered id"
    if not WHERE:
        WHERE = os.environ.get('WHERE', 'sa.id IS NOT NULL  AND  up.attendee_ptr_id IS NOT NULL')
    if not ORDER:
        ORDER = os.environ.get('ORDER', 'up.attendee_ptr_id')

    return get_persons_summit(WHERE, ORDER)


def get_talks(WHERE=None):
    "conference talk, return a tuple: dictionary of talks, and a list of ordered id"
    if not WHERE:
        WHERE = os.environ.get('WHERE', 'talk.id IS NOT NULL')

    return get_talks_summit(WHERE)


def link_person_talk(person, talk):
    if "-1" not in person:
          person["-1"] = {}
    for pid, p in person.iteritems():
	p["talks_as_scribe"] = []
        p["talks_as_approver"] = []
        p["talks_as_assignee"] = []
        p["talks_as_drafter"] = []

    for tid, t in talk.iteritems():
            if t["scribe_id"]:
		person[t["scribe_id"]]["talks_as_scribe"].append(tid)
	    else:
		person["-1"]["talks_as_scribe"].append(tid)
            if t["approver_id"]:
                person[t["approver_id"]]["talks_as_approver"].append(tid)
            else:
                person["-1"]["talks_as_approver"].append(tid)
            if t["assignee_id"]:
                person[t["assignee_id"]]["talks_as_assignee"].append(tid)
            else:
                person["-1"]["talks_as_assignee"].append(tid)
            if t["drafter_id"]:
                person[t["drafter_id"]]["talks_as_drafter"].append(tid)
            else:
                person["-1"]["talks_as_drafter"].append(tid)


### internal part ###


person_schema_summit = (

('schedule_attendee', 'sa',
  'FROM %s',
  (('id',                'id'),
   ('summit_id',         'summit_id'),
   ('user_id',           'user_id'),
   ('start',             'arrival_utc'),
   ('crew',              'crew'),
   ('end',               'departure_utc'),
   ('from_launchpad',    'from_launchpad'),)),

('debconf_website_userprofile', 'up',
  'LEFT JOIN %s ON (up.attendee_ptr_id = sa.id)',
  (('attendee_ptr_id',   'attendee_id'),
   ('badge_full',        'badge_full'),
   ('badge_nick',        'badge_nick'),
   ('sex_id',		 'sex_id'),
   ('public_image',      'public_image'),
   ('role_debian_id',    'role_debian_id'),
   ('role_debconf_id',   'role_debconf_id'),
   ('recording_ok',      'recording_ok'),
   ('attend',            'attend'),
   ('reconfirm',         'reconfirm'),
   ('registration_level_id', 'registration_level_id'),
   ('daytrip',           'daytrip'),
   ('assassins',         'assassins'),
   ('child_care',        'child_care'),
   ('subscribe',         'subscribe'),
   ('contact_email',     'contact_email'),
   ('public_email',      'public_email'),
   ('telephone',         'telephone'),
   ('address',           'address'),
   ('city_state',        'city_state'),
   ('postcode',          'postcode'),
   ('country',           'country'),
   ('emergency_name',    'emergency_name'),
   ('emergency_contact', 'emergency_contact'),
   ('arrival_needs',     'arrival_needs'),
   ('departure_needs',   'departure_needs'),
   ('arrived',           'arrived'),
   ('received_tshirt',   'received_tshirt'),
   ('received_bag',      'received_bag'))),
('debconf_website_debconfrole', 'debconfrole',
  'LEFT JOIN %s ON (up.role_debconf_id = debconfrole.id)',
  (('role',              'debconf_role'),)),
('debconf_website_debianrole', 'debianrole',
  'LEFT JOIN %s ON (up.role_debian_id = debianrole.id)',
  (('role',              'debian_role'),)),
('debconf_website_diet', 'diet',
  'LEFT JOIN %s ON (up.diet_id = diet.id)',
  (('preference',        'diet_preference'),)),
('debconf_website_registrationlevel', 'registrationlevel',
  'LEFT JOIN %s ON (up.registration_level_id = registrationlevel.id)',
  (('level',             'registration_level'),)),
('debconf_website_sex', 'sex',
  'LEFT JOIN %s ON (up.sex_id = sex.id)',
  (('sex',               'sex'),)),
('debconf_website_tshirtsize', 'tshirt',
  'LEFT JOIN %s ON (up.tshirt_size_id = tshirt.id)',
  (('size',              'tshirt_size'),)),

('auth_user', 'au',
  'LEFT JOIN %s ON (sa.user_id = au.id)',
  (('id',                'auth_user_id'),
   ('username',          'username'),
   ('first_name',        'first_name'),
   ('last_name',         'last_name'), 
   ('email',             'email'),
   # plus other not so relevant here
   )),

('debconf_website_sponsorship', 'wss',
  'LEFT JOIN %s ON (up.attendee_ptr_id = wss.userprofile_id)',
  (('sponsorship_ptr_id', 'sponsorship_ptr_id'),
   ('needs_food',        'needs_food'),
   ('use',               'sss_use'),
   ('benefit',           'sss_benefit'),
   ('rationale',         'sss_rationale'),
   ('userprofile_id',    'sss_userprofile_id'),
   ('roommate',          'roommate'),
   ('costs',             'sss_costs'),
   ('needed',            'sss_needed'),
  )),

('sponsor_sponsorship', 'sss',
  'LEFT JOIN %s ON (sa.user_id = sss.user_id AND  sa.summit_id = sss.summit_id)',
  (('about',             'sss_about'),
   ('needs_accomodation', 'needs_accomodation'),
   ('needs_travel',      'needs_travel'),
   ('diet',              'sss_diet'),
   ('real_name',         'sss_real_name'),
  )),


)


talk_schema_summit = (
('schedule_meeting', 'talk',
  'FROM %s',
  (('status',             'status'),
   ('pad_url',            'pad_url'),
   ('description',        'description'),
   ('wiki_url',           'wiki_url'),
   ('title',              'title'),
   ('scribe_id',          'scribe_id'),   # -> schedule_attendee(id)        
   ('approver_id',        'approver_id'), # -> schedule_attendee(id)
   ('private',            'private'),
   ('priority',           'priority'),
   ('assignee_id',        'assignee_id'), # -> schedule_attendee(id)
   ('drafter_id',         'drafter_id'),  # -> schedule_attendee(id)
   ('slots',              'slots'),
   ('spec_url',           'spec_url'),
   ('type',               'type'),
   ('id',                 'id'),
   ('name',               'name'),
   ('requires_dial_in',   'requires_dial_in'),
   ('video',              'video'),
   ('private_key',        'private_key'),
   ('approved',           'approved'),
   ('override_break',     'override_break'),
   ('virtual_meeting',    'virtual_meeting'),
   ('hangout_url',        'hangout_url'),
   ('broadcast_url',      'broadcast_url'),
   ('urls',               'urls'),)),
)


def unicodify(obj, encoding='utf-8', errors="ignore"):
     if isinstance(obj, basestring):
         if not isinstance(obj, unicode):
             obj = unicode(obj, encoding, errors=errors)
     return obj


def get_persons_summit(WHERE, ORDER):
    global person, person_order
    person = {}
    person_order = []

    # build the query
    query_keys = []
    result_keys = []
    places = []
    for access, tname, place, fields in person_schema_summit:
        places.append(place % ("%s %s" % (access, tname)))
        for field, as_name in fields:
            query_keys.append("%s.%s AS %s" % (tname, field, as_name))
            result_keys.append(as_name)

    query = (u"SELECT " +  ", ".join(query_keys)  +
         "\n"  + "\n".join(places) +
         "\nWHERE " + WHERE + " AND sa." + sql_this_conference + 
         "\nORDER BY " + ORDER)

    n_keys = len(result_keys)

    # extract data
    c.execute(query)
    for row in c.fetchall():

        data = {}
        for i in range(n_keys):
              data[result_keys[i]] = unicodify(row[i])

        data['arrival'] = datetime.datetime.strptime(data['arrival_utc'], "%Y-%m-%d %H:%M:%S+00").replace(tzinfo=local_time)
        data['departure'] = datetime.datetime.strptime(data['departure_utc'], "%Y-%m-%d %H:%M:%S+00").replace(tzinfo=local_time)
        data['arrival_string'] = data['arrival'].astimezone(local_time).strftime("%c")
        data['departure_string'] = data['departure'].astimezone(local_time).strftime("%c")

        person[data['attendee_id']] = data
        person_order.append(data['attendee_id'])
    return (person, person_order)


def get_talks_summit(WHERE):
    global talk, talk_order
    talk = {}
    talk_order = []

    # build the query
    query_keys = []
    result_keys = []
    places = []
    for access, tname, place, fields in talk_schema_summit:
        places.append(place % ("%s %s" % (access, tname)))
        for field, as_name in fields:
            query_keys.append("%s.%s AS %s" % (tname, field, as_name))
            result_keys.append(as_name)

    query = (u"SELECT " +  ", ".join(query_keys)  +
         "\n"  + "\n".join(places) +
         "\nWHERE " + WHERE + " AND talk." + sql_this_conference +
         "\nORDER BY talk.id")

    n_keys = len(result_keys)

    c.execute(query)
    for row in c.fetchall():

        data = {}
        for i in range(n_keys):
              data[result_keys[i]] = unicodify(row[i])

        talk[data['id']] = data
        talk_order.append(data['id'])
    return (talk, talk_order)

