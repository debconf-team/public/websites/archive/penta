#!/usr/bin/python
# -*- coding: utf8 -*-
#coding: utf8

# usage (without WHERE for all people):
# WHERE='sa.id=35' python dc14status1.py

import sys
import os
import datetime
import pytz

import dc_data

LOC_TZ="US/Pacific"  # local timezone of DebConf

WHERE = os.environ.get('WHERE', 'attend IS true')
ORDER = os.environ.get('ORDER', 'up.attendee_ptr_id')


dc_data.init(debconf_number=14, timezone=LOC_TZ)


import codecs
import locale
import sys

def stringfy(obj):
    if isinstance(obj, unicode):
        return obj
    elif isinstance(obj, basestring):
	return unicode(obj, "utf-8", "ignore")
    else:
	return unicode(obj)

person, person_order = dc_data.get_persons(WHERE, ORDER)


fields = sys.argv[1:]
print(", ".join(fields))

count = 0
for id in person_order:
    count += 1
    data = person[id]
    res = map(lambda f: stringfy(data[f]), fields)
    print(u", ".join(res))
    #sys.stdout.write(u", ".join(res) + "\n")
    

print("-- total: %s users" % (count))

