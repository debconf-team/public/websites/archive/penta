#!/usr/bin/python
# -*- coding: utf8 -*-

# usage (without WHERE for all people):
# WHERE='sa.id=35' python dc14status1.py

import sys
import os
import datetime
import pytz

import dc_data

LOC_TZ="US/Pacific"  # local timezone of DebConf

WHERE = os.environ.get('WHERE', 'attend IS true')

dc_data.init(debconf_number=14, timezone=LOC_TZ)

def stringfy(obj):
    if isinstance(obj, unicode):
        return obj
    elif isinstance(obj, basestring):
        return unicode(obj, "utf-8", "ignore")
    else:
        return unicode(obj)

person, person_order = dc_data.get_persons(WHERE)


out = {}

def inc_data(table, field):
    if not table in out:
	out[table] = {}
    if not field:
	field = "none"
    if not field in out[table]:
	out[table][field] = 1
    else:
	out[table][field] += 1

def inc_data_matrix(table, field1, field2):
    if not table in out:
        out[table] = {}
    if not field1:
        field1 = "none"
    if not field2:
        field2 = "none"

    if not field1 in out[table]:
        out[table][field1] = {}
    if not field2 in out[table][field1]:
        out[table][field1][field2] = 1
    else:
	out[table][field1][field2] += 1

def inc_data_if(table, field, condition):
    if condition:
	inc_data(table, field)



utc = pytz.utc
local_time = pytz.timezone(LOC_TZ)


for id in person_order:
    data = person[id]
 
    inc_data('0: registration stats', 'in system')
    inc_data_if('0: registration stats', 'attend', data['attend'])
    inc_data_if('0: registration stats', 'reconfirm', data['reconfirm'])
    inc_data_if('0: registration stats', 'assassins', data['assassins'])
    inc_data_if('0: registration stats', 'child_care', data['child_care'])
    inc_data_if('0: registration stats', 'daytrip', data['daytrip'])
  
    inc_data('5: t-shirt', data['tshirt_size'])
    role = data['debconf_role']
    inc_data_if('5b: t-shirt - volunteer+organizers', data['tshirt_size'], role=="Organizer" or role=="Volunteer")

    inc_data('6: sex', data['sex'])
    inc_data('2: DebConf role', data['debconf_role'])
    inc_data('1: Registration level', data['registration_level'])


keys = out.keys()
keys.sort()
print "filter:", WHERE
print

for table in keys:
    print table
    print "=====" * 5
    subkeys = out[table].keys()
    subkeys.sort()
    for name in subkeys:
	print "%3i  %s" % (out[table][name], name) 
    print


