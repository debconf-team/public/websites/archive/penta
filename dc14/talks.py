#!/usr/bin/python
# -*- coding: utf8 -*-

# usage (without WHERE for all people):
# WHERE='up.attendee_id=35' WHICH='talk.id>0' python talks.py
# formail -s /usr/sbin/sendmail -ti < dc14reconfirm-first


import os
import datetime
import pytz
import email
import email.mime.text
import email.header
import email.utils
import mailbox

import dc_data

DC_MSG_ID="dc14talk-1"
LOC_TZ="US/Pacific"  # local timezone of DebConf


WHERE = os.environ.get('WHERE', None)
WHICH = os.environ.get('WHIHC', None)


dc_data.init(debconf_number=14, timezone=LOC_TZ)

# let prefer quoted-printable instead of default BASE64 on emails
email.Charset.add_charset('utf-8', email.Charset.QP, email.Charset.QP, 'utf-8')


def generateEmailAddress(name, email_address):
    "format email address for message headers (allowing i18n)"
    try:
        name = name.decode('us-ascii')
    except:
        name = email.header.Header(name, 'utf-8')
    if isinstance(name, (str, unicode)) and "," in name:
        name = name.replace('"', '')
        name = '"%s"'%name
    return '%s <%s>' % (name, email_address)


def bool2yesno(value):
    if value:
	return "Yes"
    return "No"


person, person_order = dc_data.get_persons(WHERE)
talk, talk_order = dc_data.get_talks(WHICH)


mbox = mailbox.mbox(DC_MSG_ID + ".mbox")
mbox.clear()
mbox.flush()

for tid in talk_order:
    data = talk[tid]

    print data['id'], data['status'], "'"+data['title']+"'", person[data['drafter_id']]['badge_full']

mbox.flush()
mbox.close()

