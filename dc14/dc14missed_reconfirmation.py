#!/usr/bin/python
# -*- coding: utf8 -*-

# usage (without WHERE for all people):
# WHERE='up.attendee_ptr_id = 254' python dc14/dc14status3.py
# formail -s /usr/sbin/sendmail -ti -f registration@debconf.org < dc14reconfirm-first

import os
import datetime
import pytz
import email
import email.mime.text
import email.header
import email.utils
import mailbox

import dc_data

DC_MSG_ID="dc14missed-reconfirm"
LOC_TZ="US/Pacific"  # local timezone of DebConf


WHERE = os.environ.get('WHERE', 'attend IS true AND reconfirm IS NOT true')


dc_data.init(debconf_number=14, timezone=LOC_TZ)

# let prefer quoted-printable instead of default BASE64 on emails
email.Charset.add_charset('utf-8', email.Charset.QP, email.Charset.QP, 'utf-8')


def generateEmailAddress(name, email_address):
    "format email address for message headers (allowing i18n)"
    try:
        name = name.decode('us-ascii')
    except:
        name = email.header.Header(name, 'utf-8')
    if isinstance(name, (str, unicode)) and "," in name:
        name = name.replace('"', '')
        name = '"%s"'%name
    return '%s <%s>' % (name, email_address)

def bool2yesno(value):
    if value:
	return "Yes"
    return "No"


person, person_order = dc_data.get_persons(WHERE)


mbox = mailbox.mbox(DC_MSG_ID + ".mbox")
mbox.clear()
mbox.flush()

count = 0
for id in person_order:
    count += 1
    data = person[id]

    subject = "DebConf14 [%i]: missed reconfirmation deadline. Changing your status" % data['attendee_id']

    # =====================================
    # =====================================
    body = u"""\
Dear %(badge_full)s

You have missed the reconfirmation deadline for DebConf14 (21 July 2014),
so we are marking you as not attending the conference.

If you would still like to attend, you can still mark yourself as attending
and reconfirmed in the registration system at
https://summit.debconf.org/debconf14/registration/
 ->  Registration  ->  Conference  ->  I want to attend DebConf14
 ->  Registration  ->  Conference  ->  Reconfirm attendance


Your talk submissions and sponsorship requests will be automatically
cancelled, but we recommend that you contact
registration@debconf.org and talks@debconf.org (if you submitted talks) or
registration@debconf.org and bursaries@debconf.org (if you requested
sponsorship), so that you are not penalized in ratings next year.

If you have any questions or concerns, please don't hesitate to contact us:
  IRC:                  #debconf-team @ irc.oftc.net   (real-time questions)
  public mailing list:  debconf-team@lists.debconf.org
  or non-public email:  registration@debconf.org

Thank you,

-- 
The DebConf team
(debconf person_id: %(attendee_id)d)
"""

    # =====================================
    # =====================================

    body = body.replace("Accomodation", "Accommodation")
    body = body.replace("accomodation", "accommodation")

    body = body % data

    msg = email.mime.text.MIMEText(body, 'plain', 'utf-8')
    msg['Subject'] = subject
    msg['From'] = generateEmailAddress("DebConf Registration Team" , "registration@debconf.org")
    msg['Reply-To'] = generateEmailAddress("DebConf Registration Team" , "registration@debconf.org")
    msg['To'] = generateEmailAddress(data['badge_full'], data['contact_email'])
    msg['Cc'] = generateEmailAddress("DebConf Registration Team" , "registration@debconf.org")
    msg['Message-Id'] = email.utils.make_msgid(DC_MSG_ID + "_%i" % data['attendee_id'])
    msg["User-Agent"] = "debconf python scripts"
    msg["Date"] = email.utils.formatdate(localtime=True)

    mbox.add(msg.as_string(True))

mbox.flush()
mbox.close()

print "ready to sent %s mails in %s" % (count, DC_MSG_ID + ".mbox")
print "please check output:  less %s" % (DC_MSG_ID + ".mbox")
print "To send use:   formail -s /usr/sbin/sendmail -ti -f registration@debconf.org < %s" % (DC_MSG_ID + ".mbox")

