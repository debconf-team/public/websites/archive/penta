#!/usr/bin/python
# -*- coding: utf8 -*-
#coding: utf8

# based on person-diff.py from darst, 2010


"""
Run with
- len(sys.argv) < 2: Run, don't update last state.
- sys.argv[1]=='update'  --  if you don't do this, it won't save people.last.pickle
- sys.argv[1]=='new' -- don't try to load people.last.pickle, AND create people.last.pickle
- sys.argv[1]=='diff' -- diff sys.argv[2] and sys.argv[3] pickle files.
"""


import sys
import os
import time
import datetime
import pickle
import pytz

import dc_data

LOC_TZ="US/Pacific"  # local timezone of DebConf

WHERE = os.environ.get('WHERE', 'attend AND reconfirm IS NOT true')

dc_data.init(debconf_number=14, timezone=LOC_TZ)

dirname = '/home/pentaread/saved-data/dc14/'


def stringfy(obj):
    if isinstance(obj, unicode):
        return obj
    elif isinstance(obj, basestring):
	return unicode(obj, "utf-8", "ignore")
    else:
	return unicode(obj)

person, person_order = dc_data.get_persons(WHERE)

if len(sys.argv) < 2:
    mode = 'normal'
else:
    mode = sys.argv[1]

if mode in ('normal', 'update'):
    f = open(dirname+'people.last.pickle')
    old_person = pickle.load(f)
    f.close()
elif mode == 'diff':
    f = open(sys.argv[2])
    old_person = pickle.load(f)
    f.close()
else:
    oldPeople = {}

old_ids = old_person.keys()

if mode != 'diff':
    person, person_order = dc_data.get_persons(WHERE='up.attendee_ptr_id IS NOT NULL')
elif mode == 'diff':
    f = open(sys.argv[3])
    person = pickle.load(f)
    f.close()
else:
    raise Exception("Unknown mode")


def summary(p):
    print "ID:          ", p.get('id', None)
    print "Name:          ", p.get('badge_full', None), "  // nickname: ", p.get('badge_nick', None)
    print "Contact email: ", p.get('contact_email', None)
    print "Public email:  ", p.get('public_email', None)
    print "Attend:        ", p.get('attend', None)
    print "Reconfirmed:   ", p.get('reconfirm', None)
    print "Category:      ", p.get('registration_level', None)
    print "Debian-role:   ", p.get('debian_role', None)
    print "DebConf-role:  ", p.get('debconf_role', None)
    print "Food:          ", p.get('diet_preference', None)
    print "Daytrip:       ", p.get('daytrip', None)
    print "T-shirt:       ", p.get('tshirt_size', None)
    print "Arrival-date:  ", p.get('arrival_string', None)
    print "Departure-date:", p.get('departure_string', None)
    if p.get('child_care', None):
         print "*** Child care: *** ", p.get('child_care', None)
    if p.get('arrival_needs', None):
         print "*** Arrival needs: *** ", p.get('arrival_needs', None)
    if p.get('departure_needs', None):
         print "*** Departure needs: *** ", p.get('departure_needs', None)


def differences(pOld, p):
    lines = []
    for key in ( 'badge_full', 'badge_nick', 'contact_email', 'public_email',
                 'attend', 'reconfirm',
                 'registration_level', 'debian_role', 'debconf_role',
                 'diet_preference', 'daytrip', 'tshirt_size',
                 'child_care', 'arrival_needs', 'departure_needs',
                 'arrival_string', 'departure_string'):
        if not pOld.has_key(key):
            continue
        if pOld.get(key, None) != p.get(key, None):
            lines.append("--> %-14s: %s -> %s"%(key,
                                                pOld.get(key, None),
                                                p.get(key, None)))
    return lines


for person_id in person:
    p = person[person_id]
    if person_id not in old_ids:
        print "NEW PERSON:"
        summary(p)
        print "\n\n"+"="*20
        continue
    pOld = old_person[person_id]
    diff = differences(pOld, p)
    if len(diff) > 0:
        summary(p)
        print "\n".join(diff)
        print "\n\n"+"="*20
        continue

# Do we need to look for deleted people?  No, you can't delete them
# from the table.


if mode in ('update', 'new'):
    f = open(dirname+'people.last.pickle', 'w')
    pickle.dump(person, f)
    f.close()
f = open(dirname+'people.%s.pickle'%time.strftime("%F.%T"), 'w')
pickle.dump(person, f)
f.close()

