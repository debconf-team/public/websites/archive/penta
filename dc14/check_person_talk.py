#!/usr/bin/python
# -*- coding: utf8 -*-

# usage (without WHERE for all people):
# WHERE='up.attendee_id=35' WHICH='talk.id>0' python talks.py
# formail -s /usr/sbin/sendmail -ti < dc14reconfirm-first


import os
import datetime
import pytz
import email
import email.mime.text
import email.header
import email.utils
import mailbox

import dc_data

DC_MSG_ID="dc14talk-1"
LOC_TZ="US/Pacific"  # local timezone of DebConf


WHERE = os.environ.get('WHERE', None)
WHICH = os.environ.get('WHIHC', None)


dc_data.init(debconf_number=14, timezone=LOC_TZ)

# let prefer quoted-printable instead of default BASE64 on emails
email.Charset.add_charset('utf-8', email.Charset.QP, email.Charset.QP, 'utf-8')


def generateEmailAddress(name, email_address):
    "format email address for message headers (allowing i18n)"
    try:
        name = name.decode('us-ascii')
    except:
        name = email.header.Header(name, 'utf-8')
    if isinstance(name, (str, unicode)) and "," in name:
        name = name.replace('"', '')
        name = '"%s"'%name
    return '%s <%s>' % (name, email_address)


def bool2yesno(value):
    if value:
	return "Yes"
    return "No"


person, person_order = dc_data.get_persons(WHERE)
talk, talk_order = dc_data.get_talks(WHICH)
dc_data.link_person_talk(person, talk)


def check_talk(key, warning_msg):
    talks = p[key]
    if not talks:
	return ""
    pass


for pid in person_order:
    p = person[pid]

    if p["reconfirm"]:
        continue
    # we care only to non reconfirmed people

    warning = []
    if p["talks_as_scribe"]:
	warning.append("as scribe: %s" % p["talks_as_scribe"])
    if p["talks_as_approver"]:
        warning.append("as approver: %s" % p["talks_as_approver"])
    if p["talks_as_assignee"]:
        warning.append("as assignee: %s" % p["talks_as_assignee"])
    if p["talks_as_drafter"]:
        warning.append("as drafter: %s" % p["talks_as_drafter"])

    if warning:
	print pid, p["badge_full"], p["attend"], p["reconfirm"], "//".join(warning)



