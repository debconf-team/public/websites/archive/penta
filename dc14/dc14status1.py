#!/usr/bin/python
# -*- coding: utf8 -*-

# usage (without WHERE for all people):
# WHERE='sa.id=35' python dc14status1.py
# formail -s /usr/sbin/sendmail -ti < dc14reconfirm-first


import os
import datetime
import pytz
import email
import email.mime.text
import email.header
import email.utils
import mailbox

import dc_data

DC_MSG_ID="dc14reconfirm-first"
LOC_TZ="US/Pacific"  # local timezone of DebConf


#WHERE = os.environ.get('WHERE', 'attend AND NOT reconfirm')
WHERE = os.environ.get('WHERE', None)


dc_data.init(debconf_number=14, timezone=LOC_TZ)

# let prefer quoted-printable instead of default BASE64 on emails
email.Charset.add_charset('utf-8', email.Charset.QP, email.Charset.QP, 'utf-8')


def generateEmailAddress(name, email_address):
    "format email address for message headers (allowing i18n)"
    try:
        name = name.decode('us-ascii')
    except:
        name = email.header.Header(name, 'utf-8')
    if isinstance(name, (str, unicode)) and "," in name:
        name = name.replace('"', '')
        name = '"%s"'%name
    return '%s <%s>' % (name, email_address)

def bool2yesno(value):
    if value:
	return "Yes"
    return "No"


person, person_order = dc_data.get_persons(WHERE)


mbox = mailbox.mbox(DC_MSG_ID + ".mbox")
mbox.clear()
mbox.flush()


count = 0
for id in person_order:
    count +=1
    data = person[id]

    subject = "DebConf14 [%i]: registration check and reconfirmation" % data['attendee_id']

    # =====================================
    # =====================================
    body = u"""\
Dear %(badge_full)s

Thank you for registering for DebConf14.

This (automated) message is being sent to all attendees to remind them
about reconfirmation of DebConf14 attendance, to let you know of any
errors in your registration, and to provide details to help your DebConf14
attendance a positive experience.  Please look over the items below,
and correct any problems which may be present.  Some things are just
warnings or notes, and may not apply to you.  These are clearly
labeled; if one doesn't apply to you, please ignore it.

Since this is an automated message, it is possible this information
is not complete, or may contain error messages.  If you know that
something does not apply to you, please disregard.

For any corrections you need to make to your registration record, please
visit the system online at:
 https://summit.debconf.org/debconf14/registration/

"""

    # =====================================
    if not data['attend']:
        body += """
** You have registered in the DebConf14 registration system,
** but haven't marked yourself 'attend'.  This means we think you
** aren't attending DebConf.  This may be your intention, in
** which case, please ignore everything else in this email.
** You won't be mailed again.
**
** If you want to attend DebConf14, please mark it!
** ->  Registration  ->  Conference  ->  I want to attend DebConf14
"""

    # =====================================
    if not data['reconfirm']:
        subject += " / Please reconfirm by July 21, 2014"
        body += """
** Reconfirmation before July 21, 2014 IS REQUIRED for DebConf14
** attendance and sponsorship.
** ->  Registration  ->  Conference  ->  Reconfirm attendance
"""

#    # =====================================
#    if data['child_care']:
#        body += """
#* You have selected indicated the interest about group child care.
#* If you haven't yet contacted <@debconf.org> already, please do so ASAP.
#"""

    # =====================================
    if data['arrival_needs'] or data['departure_needs']:
        body += """
* You have indicated the need for assistance with transportation to or from
* the venue.  If you haven't already contacted <registration@debconf.org>
* about this, please do so ASAP.
* ->  Registration  ->  Conference  ->  Special arrival transport requirements?
* ->  Registration  ->  Conference  ->  Special departure transport requirements?
"""

    # =====================================
    body += """

At this time, we request that you reconfirm your attendenance in the
DebConf registration system at
https://summit.debconf.org/debconf14/registration/ .
Please go to your account and select "Confirm attendance".
Reconfirmation is required for all sponsored attendees, and necessary
to help calculations for final room and meal counts for non-sponsored
attendees.

We need you to reconfirm your attendance before July 21. Failure to
meet the deadline could result in limited distribution for sponsorship
funds and room unavailability.

For travel information on how to get to the venue from the airport,
please visit https://wiki.debconf.org/wiki/DebConf14
This page will also serve as attendee travel coordination for getting
trains and buses scheduled together. Please do not leave your travel
arrangements until the last minute.

If you have elected to not attend DebConf14, we will miss you, but
please let us know by logging into your account and
deselecting the "I want to attend this conference" box.
"""
    
    data['marked_attend'] = bool2yesno(data['attend'])
    data['marked_reconfirm'] = bool2yesno(data['reconfirm'])
    data['arrival_string'] = data['arrival'].astimezone(dc_data.local_time).strftime("%c") 
    data['departure_string'] = data['departure'].astimezone(dc_data.local_time).strftime("%c")

    body += """

Your data:
----------

Names on badge:           %(badge_full)s / %(badge_nick)s
Intend to attend:         %(marked_attend)s
Reconfirmed:              %(marked_reconfirm)s
Gender:                   %(sex)s       (for statistics on diversity)
T-Shirt size:             %(tshirt_size)s
Registration level:       %(registration_level)s
Food preferences:         %(diet_preference)s
Role in Debian:           %(debian_role)s
Role in DebConf:          %(debconf_role)s
Expected venue arrival:   %(arrival_string)s (local time)
Expected venue departure: %(departure_string)s (local time)

You can change most of your data in
https://summit.debconf.org/debconf14/registration/
"""


    body += """

If you have any questions or concerns, please don't hesitate to contact us at:
  IRC:          #debconf-team @ irc.oftc.net   (real-time questions)
  or mailing list:      debconf-team@lists.debconf.org (public list)
  or non-public email:  registration@debconf.org

Thank you,

-- 
The DebConf team
(debconf person_id: %(attendee_id)d)
"""

    # =====================================
    # =====================================

    body = body.replace("Accomodation", "Accommodation")
    body = body.replace("accomodation", "accommodation")

    body = body % data

    msg = email.mime.text.MIMEText(body, 'plain', 'utf-8')
    msg['Subject'] = subject
    msg['From'] = generateEmailAddress("DebConf Registration Team" , "registration@debconf.org")
    msg['Reply-To'] = generateEmailAddress("DebConf Registration Team" , "registration@debconf.org")
    msg['To'] = generateEmailAddress(data['badge_full'], data['contact_email'])
    msg['Cc'] = generateEmailAddress("DebConf Registration Team" , "registration@debconf.org")
    msg['Message-Id'] = email.utils.make_msgid(DC_MSG_ID + "_%i" % data['attendee_id'])
    msg["User-Agent"] = "debconf python scripts"
    msg["Date"] = email.utils.formatdate(localtime=True)

    mbox.add(msg.as_string(True))

mbox.flush()
mbox.close()

print "ready to sent %s mails in %s" % (count, DC_MSG_ID + ".mbox")

