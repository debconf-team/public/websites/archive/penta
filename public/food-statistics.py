# Richard Darst, May 2010

import datetime
import os
import time
import sys

import dc_objs

WHERE = os.environ.get('WHERE', "dcvn.attend")

allPeople = dc_objs.get_people(where=WHERE)
#first_date = min([dc_objs.DEBCAMP_START]+
#                 [p.arrival_date_d for p in allPeople if p.arrival_date_d])
first_date = dc_objs.DEBCAMP_START
#last_date = max([dc_objs.DEBCONF_END]+
#                 [p.departure_date_d for p in allPeople if p.departure_date_d])
last_date = dc_objs.DEBCONF_END


# Total
print time.ctime()
print
print """\
Assuming:
- only dinner on first day
- only breakfast on last day
- breakfast only to people with accommodation.
- After the 'none' column the number of people not eating with us (Reg//Veg/Vegan/Other//None)"""
print
print

#print sum(p.accommodation_days() for p in people)


def printStatistics(people):
    totalMealDays = sum(p.food_days() for p in people)
    mealsPerDay = 2 #dc_objs.COST_MEALS_PER_DAY
    costPerMeal = 0 #dc_objs.COST_FOOD_PER_MEAL
    print "Number of people:", len(people)
#    print "Total person-days:", totalMealDays
#    print "Assuming meals per day:", mealsPerDay
#    print "Total number of meals:", totalMealDays * mealsPerDay
#    print "Cost per meal:", costPerMeal
#    print "Total cost:", totalMealDays * costPerMeal * mealsPerDay
    print "Regular:   ", sum(1 for p in people if p.food_reg)
    print "Vegetarian:", sum(1 for p in people if p.food_veg)
    print "Vegan:     ", sum(1 for p in people if p.food_vegan)
    print "Other:     ", sum(1 for p in people if p.food_other)
    print "NotWithUs: ", sum(1 for p in people if p.food_no)
    print

    date = first_date
    dinner = {}
    lunch = {}
    breakfast = {}
    while True:
        if date > last_date:
            break
        tomorrow = date + datetime.timedelta(days=1)
        peopleTodayDinner = [p for p in people
                        if p.effective_dates()[0] <= date < p.effective_dates()[1] ]
        peopleTodayLunch = [p for p in people
                        if p.effective_dates()[0] < date < p.effective_dates()[1] ]
        peopleTodayBreakfast = [p for p in people
                        if p.accom_yes  and  p.effective_dates()[0] < date <= p.effective_dates()[1] ]

        dinner[date] = (
		sum(1 for p in peopleTodayDinner if p.food_reg   and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_veg   and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_vegan and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_other and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_no    and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_reg   and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_veg   and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_vegan and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_other and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayDinner if p.food_no    and not (p.food_select_paid or p.food_select_spons)))
        lunch[date] = (
                sum(1 for p in peopleTodayLunch  if p.food_reg   and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_veg   and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_vegan and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_other and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_no    and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_reg   and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_veg   and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_vegan and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_other and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayLunch  if p.food_no    and not (p.food_select_paid or p.food_select_spons)))
        breakfast[date] = (
                sum(1 for p in peopleTodayBreakfast if p.food_reg   and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_veg   and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_vegan and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_other and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_no    and     (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_reg   and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_veg   and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_vegan and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_other and not (p.food_select_paid or p.food_select_spons)),
                sum(1 for p in peopleTodayBreakfast if p.food_no    and not (p.food_select_paid or p.food_select_spons)))
        date += datetime.timedelta(days=1)


    date = first_date
    tot_regular, tot_veg, tot_vegan, tot_other, tot_nofood = 0,0,0,0,0
    print "BREAKFAST"
    print "%15s    Reg    Veg   Vegan  Other    Total       None -- Reg//Veg/Vegan/Other//None"%("")
    while True:
        if date > last_date:
            break
        regular, veg, vegan, other, nofood, out_regular, out_veg, out_vegan, out_other, out_nofood = breakfast[date]
        print "%15s   %4d   %4d   %4d   %4d     %4d       %4d -- %2d//%2d/%2d/%2d//%2d"%(
                      date.strftime('%F(%a)'),
                      regular, veg, vegan, other,
                      regular+veg+vegan+other,
                      nofood, out_regular, out_veg, out_vegan, out_other, out_nofood
                      )
        tot_regular += regular
        tot_veg += veg
        tot_vegan += vegan
        tot_other += other
        tot_nofood += nofood
        date += datetime.timedelta(days=1)
    print "-" * 70
    print "%15s   %4d   %4d   %4d   %4d     %4d       %4d"%(
                      'TOTAL',
                      tot_regular, tot_veg, tot_vegan, tot_other,
                      tot_regular+tot_veg+tot_vegan+tot_other,
                      tot_nofood,
                      )
    breakfast_tots = (tot_regular,tot_veg,tot_vegan,tot_other,tot_nofood)
    print
    print

    date = first_date
    tot_regular, tot_veg, tot_vegan, tot_other, tot_nofood = 0,0,0,0,0
    print "LUNCH"
    print "%15s    Reg    Veg   Vegan  Other    Total       None -- Reg//Veg/Vegan/Other//None"%("")
    while True:   
        if date > last_date:
            break 
        regular, veg, vegan, other, nofood,  out_regular, out_veg, out_vegan, out_other, out_nofood = lunch[date]
        print "%15s   %4d   %4d   %4d   %4d     %4d       %4d -- %2d//%2d/%2d/%2d//%2d"%(
                      date.strftime('%F(%a)'),  
                      regular, veg, vegan, other,
                      regular+veg+vegan+other,
                      nofood,  out_regular, out_veg, out_vegan, out_other, out_nofood
                      )
        tot_regular += regular
        tot_veg += veg
        tot_vegan += vegan
        tot_other += other
        tot_nofood += nofood
        date += datetime.timedelta(days=1)
    print "-" * 70
    print "%15s   %4d   %4d   %4d   %4d     %4d       %4d"%(
                      'TOTAL',
                      tot_regular, tot_veg, tot_vegan, tot_other,
                      tot_regular+tot_veg+tot_vegan+tot_other,
                      tot_nofood,
                      )
    lunch_tots = (tot_regular,tot_veg,tot_vegan,tot_other,tot_nofood)
    print
    print

    date = first_date
    tot_regular, tot_veg, tot_vegan, tot_other, tot_nofood = 0,0,0,0,0
    print "DINNER"
    print "%15s    Reg    Veg   Vegan  Other    Total       None -- Reg//Veg/Vegan/Other//None"%("")
    while True:   
        if date > last_date:
            break 
        regular, veg, vegan, other, nofood, out_regular, out_veg, out_vegan, out_other, out_nofood = dinner[date]
        print "%15s   %4d   %4d   %4d   %4d     %4d       %4d -- %2d//%2d/%2d/%2d//%2d"%(
                      date.strftime('%F(%a)'),  
                      regular, veg, vegan, other,
                      regular+veg+vegan+other,
                      nofood,  out_regular, out_veg, out_vegan, out_other, out_nofood
                      )
        tot_regular += regular
        tot_veg += veg
        tot_vegan += vegan
        tot_other += other
        tot_nofood += nofood
        date += datetime.timedelta(days=1)
    print "-" * 70
    print "%15s   %4d   %4d   %4d   %4d     %4d       %4d"%(
                      'TOTAL',
                      tot_regular, tot_veg, tot_vegan, tot_other,
                      tot_regular+tot_veg+tot_vegan+tot_other,
                      tot_nofood,
                      )
    dinner_tots = (tot_regular,tot_veg,tot_vegan,tot_other,tot_nofood)
    print
    print

    print "SUMMARY"
    print "%15s    Reg    Veg   Vegan  Other    Total       None"%("")
    regular, veg, vegan, other, nofood = breakfast_tots
    print "%15s   %4d   %4d   %4d   %4d     %4d       %4d"%(
                      'breakfast',
                      regular, veg, vegan, other,
                      regular+veg+vegan+other,
                      nofood,
                      )
    regular, veg, vegan, other, nofood = lunch_tots
    print "%15s   %4d   %4d   %4d   %4d     %4d       %4d"%(
                      'lunch',
                      regular, veg, vegan, other,
                      regular+veg+vegan+other,
                      nofood,
                      )
    regular, veg, vegan, other, nofood = dinner_tots
    print "%15s   %4d   %4d   %4d   %4d     %4d       %4d"%(
                      'dinner',
                      regular, veg, vegan, other,
                      regular+veg+vegan+other,
                      nofood,
                      )

print "="*20
#print "Statistics for all people"
printStatistics(allPeople)
print
print
print

sys.exit(0)

print "="*20
print "Statistics for people who requested accommodations"
printStatistics([ p for p in allPeople
                  if p.accom_yes])
print
print
print

print "="*20
print "Statistics for ONLY people who have both dates in Penta"
printStatistics([ p for p in allPeople
                  if p.arrival_date and p.departure_date ])
print
print
print


print "="*20
print "Statistics for people for whom we pay for food (S,P,C)"
printStatistics([ p for p in allPeople
                  if p.participant_category in dc_objs.CAT_SPONS|dc_objs.CAT_PRO|dc_objs.CAT_COR ])
print
print
print


print "="*20
print "Statistics for (food) sponsored people (S)"
printStatistics([ p for p in allPeople
                  if p.participant_category in dc_objs.CAT_SPONS ])
print
print
print


print "="*20
print "Statistics for (food) UNsponsored people  (B,None)"
printStatistics([ p for p in allPeople
                  if p.participant_category in dc_objs.CAT_B|dc_objs.CAT_NONE ])
print
print
print


#print "="*20
#print "Statistics for people paying for food day-by-day (B,BA,None)"
#printStatistics([ p for p in allPeople
#                  if p.participant_category in
#                              dc_objs.CAT_B|dc_objs.CAT_BA|dc_objs.CAT_NONE ])
#print
#print
#print


print "="*20
print "Statistics for Corp/Prof attendees (C,P)"
printStatistics([ p for p in allPeople
                  if p.participant_category in dc_objs.CAT_PRO|dc_objs.CAT_COR ])
print
print
print


print "="*40
print "Statistics for each individual category"
print
categories = set(p.participant_category for p in allPeople)
for category in sorted(categories):
    print "Statistics for", category
    printStatistics([ p for p in allPeople
                      if p.participant_category == category ])
    print
    print

