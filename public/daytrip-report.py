# Giacomo Catenazzi, July 2012

import os
import sys
import time
import datetime

import dc_config
from dc_config import *
import dc_objs
import dc_util

WHERE = os.environ.get('WHERE', 'dcvn.attend and dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)

today_date = datetime.date.today()
tomorrow_date = today_date + datetime.timedelta(days=1)

daytrip = {}


# check people

for p in People:
    trip = p.daytrip_option
    if not daytrip.has_key(trip):
        daytrip[trip] = 0
    daytrip[trip] += 1

# build report

keys = daytrip.keys()
keys.sort()

print "Generated (UTC time) :", time.ctime()
print

for k in keys:
    print "%3d %s" % (daytrip[k], k)

print

