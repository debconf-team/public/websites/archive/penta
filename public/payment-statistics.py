# Richard Darst, May 2010

import collections
import os
import time

import dc_objs

print time.ctime()
print

WHERE = os.environ.get('WHERE', "dcvn.attend")

allPeople = dc_objs.get_people(where=WHERE)


def printStatistics(people):
    paymentTypes = collections.defaultdict(lambda: [int(), int()])
    
    total_cost = 0
    for P in people:
        total_cost += P.get_total_cost()
        for item in P.payment_items():
            paymentTypes[item[0]][0] += 1        # Number of this
            paymentTypes[item[0]][1] += item[1]  # Income from this
    print "Total attendee fees:", total_cost
    print
    for name, value in sorted(paymentTypes.iteritems()):
        print "%5d  (%3d)  %s"%(value[1], value[0], name)


print "="*20
print "Statistics for all people"
printStatistics(allPeople)
print
print
print


print "="*20
print "Statistics for ONLY people who have both dates in Penta"
printStatistics([ p for p in allPeople
                  if p.arrival_date and p.departure_date ])
print
print
print


print "="*20
print "Statistics for sponsored people"
printStatistics([ p for p in allPeople
                  if p.participant_category in
                         dc_objs.CAT_BAF|dc_objs.CAT_BF|dc_objs.CAT_BA ])
print
print
print


print "="*20
print "Statistics for UNsponsored people"
printStatistics([ p for p in allPeople
                  if p.participant_category not in
                         dc_objs.CAT_BAF|dc_objs.CAT_BF|dc_objs.CAT_BA ])
print
print
print
