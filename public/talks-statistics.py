# Richard Darst, April 2010

import collections

import os
import sys
import dc_config
cid = int(os.environ.get("CONFERENCE_ID", dc_config.conference_id)) # conference_id

import pentaconnect
db = pentaconnect.get_conn()
c = db.cursor()

print "Total events submitted:"
c.execute("""select count(event_id)
from view_event
where conference_id=%(cid)d and translated='en'
group by event_id
;"""%locals())
print len(c.fetchall())


print
print
print "="*20
print "Event types:"
c.execute("""select event_type, count(event_type)
from view_event
where conference_id=%(cid)d and translated='en'
group by event_type
order by -count(event_type), event_type
;"""%locals())
for type, count in c.fetchall():
    print "%-18s %3d"%(type, count)


print
print
print "="*20
print "Event states:"
c.execute("""select event_state, count(event_state)
from view_event
where conference_id=%(cid)d and translated='en'
group by event_state
order by -count(event_state), event_state
;"""%locals())
for desc, count in c.fetchall():
    print "%-18s %3d"%(desc, count)

print
print
print "="*20
print "Event states and progress:"
c.execute("""select event_state, event_state_progress, count(event_id)
from view_event
where conference_id=%(cid)d and translated='en'
group by event_state, event_state_progress
order by event_state, event_state_progress;
;"""%locals())
for state, progress, count in c.fetchall():
    print "%-18s %-18s %3d"%(state, progress, count)


print
print
print "="*20
print "Number of talks per person:"
c.execute("""select name, count(name)
from view_event ve
    left join view_event_person vep using (event_id)
where ve.conference_id=%(cid)d and ve.translated='en' and vep.translated='en'
    and event_role='speaker'
group by name
order by -count(name), name;"""%locals())
# Can't list names publically...
#for name, count in c.fetchall():
#    print "%1d  %s"%(count, name)
counts = collections.defaultdict(int)
for name, count in c.fetchall(): counts[count] += 1
for count in range(max(counts), 0, -1):
    print "%2d %2d"%(count, counts[count])

sys.exit(0)

print
print
print "="*20
print "Number of talks by each person type:"
c.execute("""select dcpt.description, count(dcpt.description)
from view_event ve
    left join view_event_person vep using (event_id)
    left join debconf.dc_conference_person dcp using (person_id)
    left join debconf.dc_person_type dcpt using (person_type_id)
where ve.conference_id=%(cid)d and dcp.conference_id=%(cid)d and
    ve.translated='en' and vep.translated='en'
    and event_role='speaker'
group by dcpt.description
order by -count(dcpt.description);"""%locals())
for description, count in c.fetchall():
    print "%-35s %3d"%(description, count)


print
print
print "="*20
print "Number of talks, by DebCamp attendance"
c.execute("""select dd.debcamp_option, count(dd.debcamp_option)
from view_event ve
    left join view_event_person vep using (event_id)
    left join debconf.dc_conference_person dcp using (person_id)
    left join debconf.dc_person_type dcpt using (person_type_id)
    left join debconf.dc_debcamp dd using (debcamp_id)
where ve.conference_id=%(cid)d and dcp.conference_id=%(cid)d
    and ve.translated='en' and vep.translated='en'
    and event_role='speaker'
group by dd.debcamp_option
order by -count(dd.debcamp_option);"""%locals())
for desc, count in c.fetchall():
    print "%-40s %3d"%(desc[:40], count)
