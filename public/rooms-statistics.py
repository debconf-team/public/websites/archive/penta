# Richard Darst, 2009

import datetime
import pgdb

Date = pgdb.Date

confs = (
    {'title': 'DebConf 7',
     'dateStart': Date(2007, 6,  8),
     'dateEnd':   Date(2007, 6, 28),
     'conference_id': 1,
     'where': 'and reconfirmed and attend and cp.arrived',
     },
    {'title': 'DebConf 8',
     'dateStart': Date(2008, 8,  1),
     'dateEnd':   Date(2008, 8, 17),
     'conference_id': 2,
     'where': 'and reconfirmed and attend and cp.arrived',
     },
    {'title': 'DebConf 9',
     'dateStart': Date(2009, 7, 14),
     'dateEnd':   Date(2009, 7, 31),
     'conference_id': 3,
     'where': 'and reconfirmed and attend and cp.arrived',
     },
    {'title': 'DebConf 10',
     'dateStart': Date(2010, 7, 24),
     'dateEnd':   Date(2010, 8,  8),
     'conference_id': 4,
     'where': 'and reconfirmed and attend and cp.arrived',
     },
    {'title': 'DebConf 11',
     'dateStart': Date(2011, 7, 17),
     'dateEnd':   Date(2011, 7, 31),
     'conference_id': 5,
     'where': 'and reconfirmed and attend and cp.arrived',
     },
    {'title': 'DebConf 12',
     'dateStart': Date(2012, 7,  1),
     'dateEnd':   Date(2012, 7, 15),
     'conference_id': 6,
     'where': 'and reconfirmed and attend and cp.arrived',
     },
    {'title': 'DebConf 13',
     'dateStart': Date(2013, 8, 10),
     'dateEnd':   Date(2013, 8, 18),
     'conference_id': 7,
     'where': 'and reconfirmed and attend and cp.arrived',
     },
)

import pentaconnect
db = pentaconnect.get_conn()
c = db.cursor()

#print "number of people the night after this day:"
print "                 In-our-accomodation (total people)"

from dc_config import ACCOM_YES as VenueAccom
from dc_config import ACCOM_NO as OtherAccom
#VenueAccom = ("Regular Room",
#              "Regular room",
#              "Hostel, I need sponsorship",
#              "Hostel, I (or my company) will pay (about 20EUR per night)",
#              "Hotel, I (or my company) will pay",
#              "Junior Suite (extra payment required)",
#              "Single Room (extra payment required)",
#              "On-campus room")
#OtherAccom = ("I will arrange my own accommodation",
#              "I will arrange my own accomodation")

for conf in confs:
    total_room_nights = 0
    # Total number of attendees
    query = """
    select count(person_id)
    from conference_person cp
        join conference_person_travel cpt  using ( conference_person_id )
        join debconf.dc_conference_person dccp  using (person_id)
        join debconf.dc_accomodation dca  using (accom_id)
    where
        cp.conference_id=%(conference_id)s
        and dccp.conference_id=%(conference_id)s
        WHERE_LIMIT""".replace('WHERE_LIMIT', conf['where'])
    c.execute(query, {'conference_id':conf['conference_id']})
    total_attendees = c.fetchall()[0][0]

    query = """
    select count(person_id), accom
    from conference_person cp
        join conference_person_travel cpt  using ( conference_person_id )
        join debconf.dc_conference_person dccp  using (person_id)
        join debconf.dc_accomodation dca  using (accom_id)
    where
        cp.conference_id=%(conference_id)s
        and dccp.conference_id=%(conference_id)s
        WHERE_LIMIT
    group by accom""".replace('WHERE_LIMIT', conf['where'])
    c.execute(query, {'conference_id':conf['conference_id']})
    accom_types = c.fetchall()
    want_accom_attendees = sum(x[0] for x in accom_types if x[1] in VenueAccom)


    query = """
    select count(person_id), accom
    from conference_person cp
        join conference_person_travel cpt  using ( conference_person_id )
        join debconf.dc_conference_person dccp  using (person_id)
        join debconf.dc_accomodation dca  using (accom_id)
    where
        cp.conference_id=%(conference_id)s
        and dccp.conference_id=%(conference_id)s
        WHERE_LIMIT
        and (arrival_date is null or departure_date is null)
     group by accom
     """.replace('WHERE_LIMIT', conf['where'])
    c.execute(query, {'conference_id':conf['conference_id']})
    #missing_date_attendees = c.fetchall()[0][0]
    y = c.fetchall()
    missing_date_attendees = sum(x[0] for x in y)
    accom_missing_date_attendees = sum(x[0] for x in y if x[1] in VenueAccom)


    query = """
    select count(person_id), accom
    from conference_person cp
        join conference_person_travel cpt  using ( conference_person_id )
        join debconf.dc_conference_person dccp  using (person_id)
        join debconf.dc_accomodation dca  using (accom_id)
        join debconf.dc_debcamp ddc  using (debcamp_id)
    where
        cp.conference_id=%(conference_id)s
        and dccp.conference_id=%(conference_id)s
        WHERE_LIMIT
        and accom='On-campus room'
        and (arrival_date is null or departure_date is null)
        and debcamp_id!=1
    group by accom
        """.replace('WHERE_LIMIT', conf['where'])
    c.execute(query, {'conference_id':conf['conference_id']})
    #missing_date_debcamp_attendees = c.fetchall()[0][0]
    y = c.fetchall()
    missing_date_debcamp_attendees = sum(x[0] for x in y)
    accom_missing_date_debcamp_attendees = sum(x[0] for x in y if x[1] in VenueAccom)



    print conf['title'], "(%d)"%conf['conference_id']
    print "selection limit:", conf['where']
    print "%3d total attendees"%total_attendees
    print "%3d attendees want accom"%want_accom_attendees
    print "%3d missing dates"%missing_date_attendees
    print "%3d missing dates attending debcamp"%missing_date_debcamp_attendees
    print "%3d missing dates wanting rooms"%accom_missing_date_attendees
    print "%3d missing dates attending debcamp wanting rooms"%accom_missing_date_debcamp_attendees

    # Iterate through all days, selecting number of people there.
    d = conf['dateStart']
    while True:

        # Algorithm: Get a list of (count(accom), accom), then iterate
        # through this and count up the total number of people on that
        # night, and the number of people in VenueAccom that night.

        # Why do we go through all accommodation types here, instead
        # of bundling them into the SQL query?  Since future
        # accommodation types could be added later, and then they
        # would be left out.  This way we can make sure that there is
        # no unknown (and unhandled) accommodation types.

        query = """
        select count(accom), accom
        from conference_person cp
            join conference_person_travel cpt  using ( conference_person_id )
            join debconf.dc_conference_person dccp  using (person_id)
            join debconf.dc_accomodation dca  using (accom_id)
        where arrival_date <= %(date)s and departure_date > %(date)s
            WHERE_LIMIT
            and cp.conference_id=%(conference_id)s
            and dccp.conference_id=%(conference_id)s
        group by accom""".replace('WHERE_LIMIT', conf['where'])

        c.execute(query, {'date':d, 'conference_id':conf['conference_id']})
        accom_types = c.fetchall()
        # Make sure there are no unknown accommodation types, which
        # would indicate a programming error.
        for count, accom_type in accom_types:
            if accom_type not in VenueAccom|OtherAccom|set((' --- Please select one --- ', )):
                raise Exception("Unknown accom type %s"%repr(accom_type))

        # Add up numbers of accom types.
        venue_accom = sum(x[0] for x in accom_types if x[1] in VenueAccom)
        total_attend = sum(x[0] for x in accom_types)
        total_room_nights += venue_accom

        print d.strftime('%F(%a)  '), "%3d (%3d)"%(venue_accom, total_attend)

        if d >= conf['dateEnd']:
            break
        d += datetime.timedelta(days=1)
    print "Total room-nights (excluding no dates people):", total_room_nights
    print "DebConf no-dates room-nights:                 ", 8*missing_date_attendees
    print "DebCamp no-dates room-nights:                 ", 7*missing_date_debcamp_attendees
    print "Total room-nights:                            ", total_room_nights+8*missing_date_attendees+7*missing_date_debcamp_attendees
    print "Total cost:                            ", 53*(total_room_nights+8*missing_date_attendees+7*missing_date_debcamp_attendees)
    print
    print
