import collections
import os
import time

import dc_objs

print time.ctime()
print

WHERE = os.environ.get('WHERE', "dcvn.attend")

allPeople = dc_objs.get_people(where=WHERE)


def printStatistics(people):
    #paymentTypes = collections.defaultdict(lambda: [int(), int()])

    #roomFees = 0
    #foodFees = 0
    #otherFees = 0
    #debcampFees = 0
    profFees = 0
    corpFees = 0
    otherFees = 0
    totalFees = 0
    #total_fees_real = 0

    roomCost = 0
    foodCost = 0

    for P in people:
        #fees = P.get_total_cost()
        #fees_noFood = P.get_total_cost_noFood()
        #food_fees = fees - fees_noFood
        #if 
        for item in P.payment_items():
            #if 'food' in item[0].lower():
            #    foodFees += item[1]
            #elif 'accom' in item[0].lower() or 'room' in item[0].lower():
            #    roomFees += item[1]
            #elif 'corporate' in item[0].lower():
            #    corpFees += item[1]
            #elif 'professional' in item[0].lower():
            #    profFees += item[1]
            #elif 'debcamp' in item[0].lower():
            #    debcampFees += item[1]
            #else:
            #    print item
            #    otherFees += item[1]
            if P.participant_category in dc_objs.CAT_PRO:
                profFees += P.get_total_cost()
            elif P.participant_category in dc_objs.CAT_COR:
                corpFees += P.get_total_cost()
            else:
                otherFees += P.get_total_cost()
        totalFees += P.get_total_cost()
        #if P.amount_to_pay:
        #    total_fees_real += P.amount_to_pay

        roomCost += P.accommodation_cost()
        foodCost += P.food_cost()

        #if P.get_total_cost() != P.accommodation_cost()+P.food_cost():
        #    print P.get_total_cost(), P.accommodation_cost()+P.food_cost(), \
        #          P.payment_items()


    print "Number of people:     ", len(people)
    print "Total nights:         ", \
          sum((P.effective_dates()[1]-P.effective_dates()[0]).days
              for P in people)
    print
    print "Total room-nights:    ", \
          sum(P.accommodation_days() for P in people)
    print "Total room cost:      ", roomCost
    print "Total food-days:      ", \
          sum(P.food_days() for P in people)
    print "Total food cost:      ", foodCost
    print
    print "Total cost:           ", roomCost + foodCost
    print
    #print "Fees, food:           ", foodFees
    #print "Fees, accom:          ", roomFees
    print "Fees, professional:   ", profFees
    print "Fees, corporate:      ", corpFees
    print "Fees, other:          ", otherFees
    print
    print "Total fees:           ", totalFees
    #print "Total fees (real):    ", total_fees_real
    print
    print "Net cost of attendees:", roomCost+foodCost - totalFees

    #for name, value in sorted(paymentTypes.iteritems()):
    #    print "%5d  (%3d)  %s"%(value[1], value[0], name)


print "="*20
print "Statistics for all people"
printStatistics(allPeople)
print
print
print


print "="*20
print "Statistics for sponsored people (BAF, BA, BF)"
printStatistics([ p for p in allPeople
                  if p.participant_category in
                         dc_objs.CAT_SPONS ])
print
print
print


print "="*20
print "Statistics for unsponsored people (CORP, PROF)"
printStatistics([ p for p in allPeople
                  if p.participant_category in
                         dc_objs.CAT_PAID])
print
print
print

print "="*20
print "Statistics for other people !(B, BAF, BA, BF, CORP, PROF)"
printStatistics([ p for p in allPeople
                  if p.participant_category not in
                         dc_objs.CAT_PAID|dc_objs.CAT_SPONS ])
print
print
print


