# Richard Darst, May 2010

import datetime
import os
import time
import sys

import dc_objs
import dc_config

WHERE = os.environ.get('WHERE', "dcvn.attend")

allPeople = dc_objs.get_people(where=WHERE)

# Total
print time.ctime()
print
print """\
Assuming anyone without an arrival date arrives on %s (or %s
for DebCamp) and anyone without a departure date leaves on %s."""%(
    dc_objs.DEBCONF_START,
    dc_objs.DEBCAMP_START,
    dc_objs.DEBCONF_END   )
print
print

#print sum(p.accommodation_days() for p in people)

def printStatistics(people):
    totalAccommodationDays = sum(p.accommodation_days() for p in people)
#    roomCostPerNight = 53
    print "Number of people:", len(people)
    print "Number of people with accom:", \
          len([p for p in people if p.accom_yes])
    print "Number of people without accom:", \
          len([p for p in people if not p.accom_yes])
    print "Total person-room-days:", totalAccommodationDays
#    print "Room cost per night:", roomCostPerNight
#    print "Cost of rooms:", totalAccommodationDays * roomCostPerNight
#    print "Regular:   ", sum(1 for p in people if p.food in dc_objs.FOOD_REG)
#    print "DebCamp-week rooms:", \
#          sum(1 for p in people
#              if p.effective_dates()[0] <= date < p.effective_dates()[1] ]

    print

    earliest_date = min([dc_objs.DEBCAMP_START]
                        + [p.effective_dates()[0] for p in people])
    #date = min(dc_objs.DEBCAMP_START, earliest_date)
    earliest_date -= datetime.timedelta(days=1)

    latest_date = max([dc_objs.DEBCONF_END]
                      + [p.effective_dates()[1] for p in people])
    latest_date = max(dc_objs.DEBCONF_END, latest_date)

    
    print "%15s   Room   OwnAccom"%("")
    date = earliest_date
    while True:
        if date > latest_date:
            break
        tomorrow = date + datetime.timedelta(days=1)
        peopleToday = [p for p in people
                        if p.effective_dates()[0] <= date < p.effective_dates()[1] ]
        room   =sum(1 for p in peopleToday if p.accom_yes)
        noRoom =sum(1 for p in peopleToday if not p.accom_yes)
    #    from fitz import interactnow
        print "%15s   %3d      %3d"%(date.strftime('%F(%a)'),
                      room, noRoom,
                      )
    
        date += datetime.timedelta(days=1)


print "="*20
print "Statistics for all people"
printStatistics(allPeople)
print
print
print


sys.exit(0)

print "="*20
print "Statistics for ONLY people who have both dates in Penta"
printStatistics([ p for p in allPeople
                  if p.arrival_date and p.departure_date ])
print
print
print


print "="*20
print "Statistics for (accom) sponsored people (BAF,BA)"
printStatistics([ p for p in allPeople
                  if p.cat_baf or p.cat_ba])
print
print
print


print "="*20
print "Statistics for UNsponsored people (!S)"
printStatistics([ p for p in allPeople
                  if not (p.cat_baf or p.cat_ba)])
print
print
print

print "="*20
print "Statistics for (PRO, COR, BAF, BA)"
printStatistics([ p for p in allPeople
                  if p.participant_category in dc_objs.CAT_PRO|dc_objs.CAT_COR|dc_objs.CAT_BAF|dc_objs.CAT_BA ])
print
print
print

#
#
#print "="*20
#print "Statistics for people paying for accommodations by day (B,BF,None)"
#printStatistics([ p for p in allPeople
#                  if p.participant_category in
#                               dc_objs.CAT_B|dc_objs.CAT_BF|dc_objs.CAT_NONE])
#print
#print
#print
#
#
#print "="*20
#print "Statistics for Corp/Prof/blank attendees (P,C,None)"
#printStatistics([ p for p in allPeople
#                  if p.participant_category in dc_objs.CAT_PRO|dc_objs.CAT_COR ])
#print
#print
#print

print "="*40
print "Statistics for each individual category"
print
categories = set(p.participant_category for p in allPeople)
for category in sorted(categories):
    print "Statistics for", category
    printStatistics([ p for p in allPeople
                      if p.participant_category == category ])
    print
    print

