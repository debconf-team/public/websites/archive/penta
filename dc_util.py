# Richard Darst, April 2010

import datetime
import textwrap
import time

class NotAnError(Exception):
    pass

def isNull(x):
    if x == None or x == '':  return True
    else:                     return False
def parseDate(x):
    if not x: return x
    return datetime.date(*time.strptime(x, "%Y-%m-%d")[:3])

def wrap_text(text):
    """Wrap a bit of text by paragraph, wrapping each paragraph only
    if it isn't already wrapped."""
    if text is None: return None
    # Normalize line endings to \n
    text = text.replace('\r\n', '\n')
    # Break it into paragraphs
    paragraphs = [ ]
    for par in text.split('\n\n'):
        par = '\n'.join(l.rstrip() for l in par.split('\n'))
        maxlinelen = max(len(l) for l in par.split('\n'))
        # If already wrapped, do nothing
        if maxlinelen <= 79:
            paragraphs.append(par)
            continue
        # Other paragraphs, actually wrap them.
        par = textwrap.fill(par, width=79)
        paragraphs.append(par)
    return "\n\n".join(paragraphs)


def rowToDict(rows, c):
    rownames = [ x[0] for x in c.description ]
    data = [ dict(zip(rownames, row)) for row in rows ]
    return data

def gender(p):
    if p.gender is True: return "male"
    if p.gender is False: return "female"
    return ""
