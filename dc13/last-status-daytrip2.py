# Richard Darst, April 2010

import os
import sys
import datetime
import csv
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


beginning = """\
Hello %(name)s,

This (automated) message is being sent to give you some information
about DebConf, to remind you about reconfirmation of your attendance,
and to let you know of any errors in your registration, or other
things which you should know about.  Please look over the details
below, and correct any problems which may be present in your Penta
account.

Day trip: we are organizing a day trip and Conference Dinner on
Wednesday, 14 August 2013:
  * In the morning you can choose to visit the Clock Museum, an
    Asphalt Mine, or an Absinthe distillery; or to take part in one
    of two hikes: one shorter (2-3 hours walking) and one longer,
    the latter starting directly from Le Camp (and suitable for
    mountain bikes).
  * We will have a picnic lunch all together near the Creux-du-Van
    (a famous scenic landscape).
  * In the afternoon we will go to Neuchatel (looking around the
    city, shopping, enjoying the street music festival).
  * In the evening we will take a boat back to Vaumarcus; we will
    have the conference dinner on board.

You will find more details on https://wiki.debconf.org/wiki/DebConf13/DayTrip .
The places on different activities are limited, so you are required to
register your day trip preference in Penta as soon as possible:
https://penta.debconf.org/penta/submission/dc13/person

%(accommodation)s
%(food)s%(invoice)s

We are still looking for volunteers to staff the bar, as well as for
setup on 10 August and teardown on 18 August. If you are willing to
help in these areas please check and put your name in
https://wiki.debconf.org/wiki/DebConf13/VolunteerCoordination .

We are also looking for people willing to write a short article for the
DebConf13 Final Report. The following parts are best written from an
attendee perspective:
 * Reports from DebCamp
 * Attendees' impressions
 * Day trip
 * Free-time activities
Please sign up on https://wiki.debconf.org/wiki/DebConf13/FinalReport
if you are willing to write an article.

We have collected important and useful information for your travel
preparation on our website at
http://debconf13.debconf.org/location.xhtml

If you are coming from abroad, please bring a power plug adaptor.
Switzerland has its own standard which is described in SEC 1011. It
will mostly work for type C connectors (also known as "Europlug")
European connectors. It will not work with type E (French earthed)
or type F (also known as "Schuko").

If your plans have changed (not attending, arriving late, etc.),
please send a mail to registration@debconf.org .

If you have any questions, please contact us via
  IRC: #debconf-team @ irc.oftc.net (real-time questions)
  or mailing list: debconf-team@lists.debconf.org (public list)
  or non-public email: registration@debconf.org
  also check the conference website: http://debconf13.debconf.org

The registration system URL is
  https://penta.debconf.org/penta/submission/dc13/person
"""


reconfirm_warning = """

** You must reconfirm and provide the required information by 30 June
** or your registration will be canceled.  Please don't wait and do it
** TODAY."""

error_warning = """

** You have some errors, please fix your registration
** details. (see at the bottom of this mail)
"""

if_request_sponsorship = """\

We are sorry not to have finished sponsorship review earlier, but we
promise you to review your sponsorship status by Sunday 30 June.  For
this reason we postpone the reconfirmation deadline to Sunday 7 July.
You can also already reconfirm now, and in case of non-granted
sponsorship, you still have the possibility to change your attendence
status or change the attendence days.

"""

if_need_to_pay = """\

We will send the invoice within next two weeks.  If you have questions
about the invoice or payment options, check out
<http://debconf13.debconf.org/payments.xhtml> or please contact
registration@debconf.org .

"""


ending = """\
Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body = """%(beginning)s

Information about your registration
===================================

%(info)s

Registration errors and warnings
================================

%(errors)s


%(ending)s
"""

# Get our list of errors:
Errors = errorcheck.Errors
# this don't work: Errors have not yet a name (not yet instantiated)
#Errors = [e for e in Errors
#          if e.__class__.__name__ not in ('ThirteenYearOldAgeLimit',
#                                          'DoubleCheckTravelSponsorshipNumbers',
#                                          'NewlyEnabledFieldsInfo',
#					  'NoRoomPreference') ]
Errors = [e for e in Errors
          if 'travel' not in e._class ]
errorcheck.printErrors(Errors)
print
print

# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
WHERE = os.environ.get('WHERE', 'dcvn.attend')
People = dc_objs.get_people(where=WHERE)
get_people_by_id = dc_objs.get_people_by_id
[ P.add_errors(Errors) for P in People ]
People = [ P for P in People if len(P.errors) > 0 ]
mbox = mailbox.mbox("dc13daytrip.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


### ROOMS ###
#============

def parse_date(x):
    dt = datetime.datetime.strptime(x, '%Y-%m-%d')
    return dt.date()

# persons[id] -> [ (hotel, room, arrive, depart) ]
room_persons = {}
room_person_name = {}
# rooms[hotel+":"+room] -> [ (person_id, arrive, depart) ]
rooms = {}


def parse_spreadsheet(fname):
    import readsxc
    f = readsxc.OOSpreadData(fname)

    names = None
    numpeople = 0
    for row in f:
        # Automatically create columns
        if not names:
            names = [ n.lower().replace(' ', '_').replace('-', '_')
                      for n in row ]
            continue
        if not row or row[0].lower() == "end" or (not row[0] and not row[1]):
            break
        numpeople += 1
        # Row is a dict of row labels
        row = dict(zip(names, row))
        #print row
        #print row['person_id']

        # Ignore empty names (free bed)
        if not row.has_key('name') and not row.has_key('person_id'):
            continue
        person_id = int(row['person_id'])

        if not room_persons.has_key(person_id):
            room_persons[person_id] = []
            room_person_name[person_id] = row['name']

        # Get room information
        roomName = row['room']  # row['room_alloc']
        if roomName.startswith('n--') and person_id != 34:
            continue

        roomNameCamp = row['roomcamp']

        hotelName = "DebConf"
        hotelNameCamp = "DebCamp"

        arrive = parse_date(row['arrival_date'])
        depart = parse_date(row['departure_date'])
        
        if roomNameCamp:
            room_persons[person_id].append( (hotelNameCamp, roomNameCamp, arrive, min(dc_objs.DEBCONF_START, depart)) )
            if not rooms.has_key(hotelNameCamp + ":" + roomNameCamp):
                rooms[hotelNameCamp + ":" + roomNameCamp] = []
            rooms[hotelNameCamp + ":" + roomNameCamp].append( (person_id, arrive, min(dc_objs.DEBCONF_EARLIEST_ARRIVAL, depart)) )

        room_persons[person_id].append( (hotelName, roomName, max(arrive, dc_objs.DEBCONF_EARLIEST_ARRIVAL), depart) )
        if not rooms.has_key(hotelName + ":" + roomName):
            rooms[hotelName + ":" + roomName] = []
        rooms[hotelName + ":" + roomName].append( (person_id, max(arrive, dc_objs.DEBCONF_EARLIEST_ARRIVAL), depart) )

    print "People processed:", numpeople
    return

parse_spreadsheet(sys.argv[1])


### PAYMENTS ###
#===============
payments = {}
header_line = True
with open('./hug.invoice/attendee_payments.csv', 'rb') as csvfile:
  for line in csv.reader(csvfile):
    if not line:
        continue
    if header_line:
        header_line = False
        continue

    person_id, name, email, invoice_nr, invoice_total, invoice_paid, invoice_url = line
    person_id = int('0' + person_id)

    invoice_total = float(invoice_total)
    invoice_paid = float(invoice_paid)
    if invoice_total == 0:
        # no need to pay
        continue
    if person_id == 180:
        continue
    if invoice_total <= invoice_paid:
        payments[person_id] = "\n\nWe have received your payment. Thank you.\n\n"
    else:
        payments[person_id] = """\n
We have not received your payment yet. Please check the invoice mail we
sent you, and pay immediately or contact us (if you cannot pay
immediately).

"""



for person in People:
    # This parts deals with printing.

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    AllErrors = person.errors
    AllErrors = [e for e in AllErrors
              if e.__class__.__name__ not in ('ThirteenYearOldAgeLimit',
                                              'DoubleCheckTravelSponsorshipNumbers',
                                              'NewlyEnabledFieldsInfo',
					      'NoRoomPreference',
					      'NoCountry') ]
    AllErrors = [e for e in AllErrors
              if 'travel' not in e._class ]
    person.errors = AllErrors

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    no_errors = False
    if not errors.strip():
	no_errors = True
	errors = "    (none)"

    person_id = person.person_id
    if person_id != 34: ############################################################
	continue
    if person.accom_no:
        person.accommodation = "No accommodation provided."
    else:
	print room_persons[person_id]
        if len(room_persons[person_id]) > 1:
            person.accommodation = "You have several rooms allocated (at different days):\n\n"
        else:
            person.accommodation = ""
        for hotel, room, arrive, depart in room_persons[person_id]:
            hotel = hotel.encode('ascii', 'ignore')
            room = room.encode('ascii', 'ignore')
            
            hr = hotel + ":" + room
            from_date = "from %s to %s (in %s)" % (arrive.strftime("%d %B"), depart.strftime("%d %B"), hr)
            roomates = ""
            for roommate_id, rm_arrive, rm_depart in rooms[hr]:
		if get_people_by_id.has_key(roommate_id):
		    roommate_name = get_people_by_id[roommate_id].name
		else:
		    roommate_name = room_person_name[roommate_id].encode('ascii', 'ignore')
                roomates += "      %s (%s - %s)\n" % (roommate_name, rm_arrive, rm_depart)
            if room[0] == "s":
                person.accommodation += """\
You get a room %s.  You are required to bring a sleeping bag and a towel.
Your roommates will be:\n%s\n""" % (from_date, roomates)
            elif room[0] == "b":
               person.accommodation += """\
You get a room %s. You are required to bring a towel (bedding/linen are
provided). Your roommates will be:\n%s\n""" % (from_date, roomates)
            elif room[0] == "t":
                person.accommodation += """\
You get a place for tent / van %s. You are required to bring a towel, we
will provide you with WCs, showers and breakfast. On arrival ask front
desk where to put your tent or van. Don't just put your tent somewhere
without talking to us first.\n\n""" % from_date

    person.food = ""
    if not person.food_yes and not (person.food_select_paid or person.food_select_spons):
        person.food = """\n
Please select your dietary restrictions (even if you are eating
outside Le Camp): we need the information for the conference dinner
and coffee breaks.
If you intend to eat at Le Camp on some days, please contact
registration@debconf.org or the front desk 48 hours before the
requested meal(s).
"""

    person.invoice = payments.get(person.person_id, "")

    Body = body%locals()
    Body = Body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf13: Daytrip and status mail [%d]" % person.person_id,
        Body=Body,
        extraMsgid="dc13daytrip",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


errorcheck.printPeopleErrors(People)
print
print
errorcheck.printErrorsByClass(People)
