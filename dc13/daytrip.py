 # -*- coding: utf-8 -*-

import sys
import os
import mailbox
from textwrap import fill

import emailutil
from emailutil import Email, Message

import pentaconnect

header = """Hi %s

Tomorrow (Wednesday) is the day trip.

You have selected the following DayTrip option and we have compiled
the relevant information for you:

"""

daytrip_text = {5:'''A: International Clock Museum

Meeting time: 08:45 behind Zwingli on the street

What to bring:
    * Lunch bag (see below)
    * Water
    * Sun screen, hat (for Neuchâtel and the boat)

Your  "tour guide" is Franklin Piat. Entrance fee will be paid by him.

''',
                6:'''B: Asphalt Mine

Meeting time: 09:00 behind Zwingli on the street

What to bring:
  * Lunch bag (see below)
  * Water
  * Good shoes
  * Warm clothes (It\'s 8°C in there)
  * Your own torch (recommended, not mandatory)
  * Sun screen, hat (for Neuchâtel and the boat)

Your "tour guide" is Moray Allan. Entrance fee will be paid by him.

''',
                7:'''C: Absinthe distillery

Meeting time: 10:00 behind Zwingli on the street

What to bring:
  * Lunch bag (see below)
  * Water
  * Sun screen, hat (for Neuchâtel and the boat)

Your "tour guide" is Raphaël Walther. Entrance fee will be paid by him.

''',
                8:'''D1: Hiking from Les Rochats

Meeting time: 9:00 behind Zwingli on the street

What to bring:
    * Lunch bag (see below)
    * Water
    * Towel and soap if you want to shower at the lunch site (see below)
    * Hiking shoes
    * Backpack
    * Sun screen, hat

Your "tour guide" is Philipp Hug. We intend to walk in 2 or 3 groups.
If you are interested you can find the itinary and a GPX track below. 
We  will have some printouts of the itinerary as well.

Route http://map.wanderland.ch/?layers=Wanderwegnetz&trackId=1413283
GPX Track: http://media.debconf.org/dc13/daytrip/d1_les_rochats_soliat.gpx

Shower:
There is a communal 3 person shower at Soliat. We can use it for free, 
but there might not be enough warm water for everyone, so please be 
considerate. An alternative is to take a bath in Lake Neuchatel later 
in the afternoon.

''',
                9:'''D2: Long hiking from Le Camp

Meeting time: 07:45 in front of the main building (where lunch is served)

What to bring:
    * Lunch bag (see below)
    * Water
    * Towel and soap if you want to shower at the lunch site (see below)
    * Hiking shoes
    * Backpack
    * Sun screen, hat

Your "tour guides" are Gaudenz and OdyX. We intend to walk in 2 or 3
groups. If you are interested you can find the itinary and a GPX track
below. We will have some printouts of the itinerary as well.

Itinerary: http://map.wanderland.ch/?layers=Wanderwegnetz&trackId=1399453
GPX Track: http://media.debconf.org/dc13/daytrip/d2_le_camp_soliat.gpx

Shower:
There is a communal 3 person shower at Soliat. We can use it for free, 
but there might not be enough warm water for everyone, so please be 
considerate. An alternative is to take a bath in Lake Neuchatel later 
in the afternoon.

''',
                10:'''O: conference dinner only 

You have to arrange your own transportation to Neuchâtel. The easiest
option is to take the bus to either Gorgier-St-Aubin or Yverdon and 
then train to Neuchâtel from there. The boat journey for the
conference dinner will end at Vaumarcus, so taking your car is
probably not a good idea. See http://www.sbb.ch/ for the timetable.

Be sure to be at the Neuchâtel harbour at 7pm. Please be on time or 
we will leave without you. 

Consider bringing sun screen and hat if you are spending time at Neuchâtel

''',
                11:'''X: I\'ll neither take part in DayTrip nor Conference Dinner

Just as a reminder: there will be no dinner at Le Camp. You can pick
up a lunch bag form the picnic buffet which will be available starting
from 7:30am. You will have to take care of dinner yourself.

'''
,
                }
invalid = """You have an invalid selection for the daytrip in the registration
system. Please contact us as soon as possible if you want to take part 
in the daytrip. If not, we will assume that you only want to come to
the conference dinner.

In that case, you have to arrange your own transportation to
Neuchâtel. The easiest option is to take the bus to either
Gorgier-St-Aubin or Yverdon and then train to Neuchâtel from there. 
The boat journey for the conference dinner will end at Vaumarcus, 
so taking your car is probably not a good idea. See http://www.sbb.ch/ 
for the timetable.

Be sure to be at the Neuchâtel harbour at 7pm. Please be on time or we
will leave without you.
"""

footer = """
Conference Dinner:
The conference dinner will be on a boat on lake Neuchâtel. You have to
be at the harbour at 7pm to board the boat. WE CANNOT WAIT so be
punctual.

The boat journey will finish in Vaumarcus. From there we will have to
walk about 20 minutes back up to Le Camp.

Openstreetmap link to the harbour of Neuchâtel: http://www.opentstreetmap.org/?lat=46.99174404144287&lon=6.931772232055664&zoom=16

Conference Dinner menus:
There will be two main courses to chose for people without particular 
restrictions: trout fillet or chicken (Vegetarians and vegans have
their own tasty menus, of course). As the numbers of each menu are
fixed, and to ease the service on the boat, we will be handing out 
colourful food tickets during boarding. If you have an exceptionally 
large preference for either chicken or trout, talk to FrontDesk and
get a food ticket of the correct colour in advance. Make sure to 
have it with you tomorrow evening or you shall remain hungry.

Swimming in Lake Neuchâtel:
We will spend the afternoon in Neuchâtel. Coaches will drop everyone
off at the harbour, at the same place where the boat leaves for the 
conference dinner. It's possible to swim in the lake nearby. If you 
want to swim, take your swimming trunks and towel with you.

Breakfast:
Breakfast will be provided from 7:00 until 9:30. 

Lunch bags:
There will be a picnic buffet in the main building starting from 7:30. 
You have to pack your own lunch bag for the picnic at lunch.

Contact:
If against all odds you get lost during the DayTrip. Please contact 
Front Desk at +41 76 640 91 11.

Boarding coaches:
As the boarding of coaches can take quite a lot of time, please make 
sure to be on time. We are on a tight schedule.

If you want to change your daytrip option, please contact us as soon
as possible. Please don't change your option in penta yourself.

See you tomorrow,
OdyX, Raphael and Gaudenz on behalf of the DebConf Team
"""

db = pentaconnect.get_conn()
cur = db.cursor()
cur.execute("select person.person_id, coalesce(public_name, first_name || ' ' || last_name) as name, person.email, daytrip_id from conference_person_travel join conference_person using (conference_person_id) join person using (person_id) join debconf.dc_conference_person using (person_id) join debconf.dc_daytrip using (daytrip_id) where debconf.dc_conference_person.conference_id=7 and conference_person.conference_id=7 and ((attend and reconfirmed and arrival_date in ('2013-08-13', '2013-08-14') ) or (conference_person.arrived)) and departure_date >= '2013-08-14' order by daytrip_id, name")

# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
mbox = mailbox.mbox("dc13daytrip.mbox")
mbox.clear() ; mbox.flush()

for row in cur:
    (person_id, name, email, daytrip_id) = row

    if len(sys.argv) > 1 and not person_id == int(sys.argv[1]):
        continue

    body = (header % name) + daytrip_text.get(daytrip_id, invalid) + footer

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(name, email),
        Subject="Daytrip information [%d]" % person_id,
        Body=body,
        extraMsgid="daytrip",
        ReplyTo=Email("DebConf Team", "debconf-team@lists.debconf.org"),
        Cc=Email("DebDonf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


