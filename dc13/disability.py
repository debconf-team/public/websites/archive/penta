# Richard Darst, April 2010

import os
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


body = """\
Dear %(name)s,

[Note: This is an automatic mail]

You marked that we should contact you about disability accommodation [0].
So we are. Could you tell us what special need do you need, in order
to provide you a suitable room, and to program the Day Trip.

Additionally, if you need special help for travel or any other request
or information, don't hesitate to contact us (registration@debconf.org).
You will find some more information about travelling in:
http://www.sbb.ch/en/station-services/passengers-with-a-handicap.html

See you in Switzerland,
    DebConf Team

[0] https://penta.debconf.org/penta/submission/dc13/person

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

WHERE = os.environ.get('WHERE', 'dcvn.attend AND dccp.disabilities')
People = dc_objs.get_people(where=WHERE)
#[ P.add_errors(Errors) for P in People ]
mbox = mailbox.mbox("dc13disabilities.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    # This part deals with mailing.

    subject_extra = ""

    # Body = body%locals()
    Body = body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf: Disabilities and other special requests",
        Body=Body,
        extraMsgid="dc13disabilities",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


print
