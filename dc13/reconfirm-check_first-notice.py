# Richard Darst, April 2010

import os
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


beginning = """\
Dear %(name)s,

Thank you for registering for DebConf13.%(mail_banner)s

This (automated) message is being sent to all attendees to remind them
about reconfirmation of DebConf13 attendance, to let you know of any
errors in your registration, and instructions to make your DebConf13
attendance a positive experience.  Please look over the items below,
and correct any problems which may be present.  Some things are just
warnings or notes, and may not apply to you.  These are clearly
labeled; if one doesn't apply to you, please ignore it.

Because this is an automated message, it is possible this information
is not complete, or may contain error messages.  If you know that
something does not apply to you, please disregard.

Attendees paying for their stay should get an email in the next day
or two explaining the exact amount they have to pay and any further
information you need for your invoicing.

The registration system URL is
 https://penta.debconf.org/penta/submission/dc13/person
"""

sponsoring_warning = """

** The DebConf team is reviewing the attendees signed up for food
** or accommodation sponsorship.  At this time, we can't promise
** food and accommodation sponsorship to you. We will make
** sponsorship decisions as soon as we can."""

reconfirm_warning = """

** Reconfirmation before 30 June 2013 IS REQUIRED for DebConf13
** attendance and sponsorship."""

ending = """\

At this time, we request that you reconfirm your attendenance in Penta:
https://penta.debconf.org/penta/submission/dc13/person .
Please go to your account and select "Confirm attendance".
Reconfirmation is required for all sponsored attendees, and necessary
to help calculations for final room and meal counts for non-sponsored
attendees.

We need you to reconfirm your attendance before 30 June. Failure to
meet the deadline could result in limited distribution for sponsorship
funds and room unavailability. If you are waiting to hear back about
your sponsorship status, please be aware that sponsorship decisions
should be available within the next week.

For travel information on how to get to Vaumarcus from the airport,
please visit https://wiki.debconf.org/wiki/DebConf13/TravelCoordination
This page will also serve as attendee travel coordination for getting
trains and buses scheduled together. Please do not leave your travel
arrangements until the last
minute.

If you have elected to not attend DebConf13, we will miss you, but
please let us know by logging into your account in Penta and
deselecting the "I want to attend this conference" box.

If you have any questions or concerns, please don't hesitate to contact us at:
  IRC:          #debconf-team @ irc.oftc.net   (real-time questions)
  or mailing list:      debconf-team@lists.debconf.org (public list)
  or non-public email:  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body = """%(beginning)s

Information about your registration
===================================

%(info)s

Registration errors and warnings
================================

%(errors)s


%(ending)s
"""

# Get our list of errors:
Errors = errorcheck.Errors
Errors = [e for e in Errors
          if e.__class__.__name__ not in ('RegistrationCheck',
                                          'ThirteenYearOldAgeLimit',
                                          'DoubleCheckTravelSponsorshipNumbers',
                                          'NewlyEnabledFieldsInfo') ]
Errors = [e for e in Errors
          if 'travel' not in e._class ]
errorcheck.printErrors(Errors)
print
print

# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
WHERE = os.environ.get('WHERE', 'dcvn.attend and dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)
[ P.add_errors(Errors) for P in People ]
People = [ P for P in People if len(P.errors) > 0 ]
mbox = mailbox.mbox("dc13reconfirm-first.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    #Errors = [e for e in person.errors
    #          if e.__class__.__name__ not in ('RegistrationCheck',
    #                                          'ThirteenYearOldAgeLimit',
    #                                          'DoubleCheckTravelSponsorshipNumbers',
    #                                          'NewlyEnabledFieldsInfo') ]
    #Errors = [e for e in Errors
    #          if 'travel' not in e._class ]
    #person.errors = Errors

    #if len(Errors) > 0:
    #    print '='*40
    #    print person.person_id, person.name
    #    print person._info()


    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    subject_extra = ""
    if not person.reconfirmed:
	subject_extra += ", please reconfirm NOW"

    person.mail_banner = ""
    if person.cat_spons:
	person.mail_banner += sponsoring_warning
    if not person.reconfirmed:
        person.mail_banner += reconfirm_warning

    Body = body%locals()
    Body = Body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf13 registration check" + subject_extra,
        Body=Body,
        extraMsgid="dc13reconfirm-first",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


errorcheck.printPeopleErrors(People)
print
print
errorcheck.printErrorsByClass(People)
