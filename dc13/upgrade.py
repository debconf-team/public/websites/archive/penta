# Richard Darst, April 2010

import os
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


body = """\
Dear %(name)s,

You marked that you will NOT accept sponsored communal accommodation
(12 or more beds per room).  Unfortunately with the large number of
registered attendees, we cannot accommodate all sponsored attendees in
smaller rooms.

However, we can offer you an upgrade (20 CHF/day, ~21 USD/day,
~16 EUR/day) to a room with maximum of 4 roommates.
See https://wiki.debconf.org/wiki/DebConf13/Accommodation
Please note that the smaller rooms have bedding provided.

If you want to upgrade, please mark it in the Accommodation section of
your Pentabarf account [0]. We will then invoice you for your upgrade.
Note that small rooms, and thus upgrades, are limited and assigned as
a first-come, first-served basis.

If communal accommodation sponsorship is not acceptable and you cannot
afford the upgrade to a smaller room, please contact
registration@debconf.org so we can try to find a way to assist you.

See you in Switzerland,
    DebConf Team

[0] https://penta.debconf.org/penta/submission/dc13/person

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

WHERE = os.environ.get('WHERE', 'dcvn.attend AND NOT dcvn.com_accom')
People = dc_objs.get_people(where=WHERE)
#[ P.add_errors(Errors) for P in People ]
mbox = mailbox.mbox("dc13upgrade.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    if not person.accom_sponsored:
	continue
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    # This part deals with mailing.

    subject_extra = ""
    if not person.reconfirmed:
	subject_extra += ", please reconfirm NOW"

    # Body = body%locals()
    Body = body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf: Room upgrades",
        Body=Body,
        extraMsgid="dc13upgrade",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


print
