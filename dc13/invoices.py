# Giacomo Catenazzi, July 2013

# formail -s /usr/sbin/sendmail -ti < dc13invoices.mbox

import csv
import mailbox
from textwrap import dedent

import emailutil
from emailutil import Email, Message

from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders

body = """\
Hello %(name)s,

Please find a link to your invoice below:
<%(invoice_url)s>

You can pay with Paypal on the link provided or do a bank transfer to
our account which is also listed on the invoice page.

If you live in the UK or an other supported country
you could look at http://transferwise.com/ as an
alternative, cheaper transfer option.

Here are our bank details (don't forget to add you reference number
which is on the invoice):
Reference: %(invoice_nr)s
Recipient E-mail: info@debconf13.ch
Recipient Name/Address: DebConf13 association, 8400 Winterthur, Switzerland 
Bank: Swiss Post, Postfinance, 3030 Bern, Switzerland 
BIC: POFICHBEXXX 
IBAN: CH53 0900 0000 6019 5325 6

If you have any questions, please contact us via
IRC:
  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""



mbox = mailbox.mbox("dc13invoices.mbox")
mbox.clear() ; mbox.flush()

header_line = True
with open('attendee_payments.csv', 'rb') as csvfile:
  for line in csv.reader(csvfile):
    if not line:
        continue
    if header_line:
	header_line = False
	continue

    person_id, name, email, invoice_nr, invoice_total, invoice_paid, invoice_url = line
    name = name.replace("_", " ")
    person_id = int(person_id)

    Body = body%locals()

    msg = emailutil.Message( # emailutil.MessageWithAttachments(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(name, email),
        Subject="DebConf13 attendee invoice",
        Body=Body,
        extraMsgid="dc13invoice",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    # if we have an attachment
    # part = MIMEBase('application', "pdf")
    # part.set_payload( open(filename,"rb").read() )
    # Encoders.encode_base64(part)
    # part.add_header('Content-Disposition', 'attachment; filename="%s"' % filename)
    # msg.attach(part)

    mbox.add(str(msg))

mbox.flush()

