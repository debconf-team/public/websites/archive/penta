# Richard Darst, April 2010

import os
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


body = """\
Dear %(name)s,

Finally we can confirm that we will host DebCamp just before DebConf,
at the same venue of DebConf.

You marked "I would attend DebCamp if it was held", so if you still
plan to attend DebCamp, please mark your working plan for DebCamp.

Notes:
- The place for DebCamp is limited, as we will have access to fewer
  buildings, so only attendees with good working plan will be hosted.
- The prices for non sponsored people will be the same as DebConf
  (food and accommodation, but without the option to pay an
  additional attendance fee)
- The registration system URL is
  https://penta.debconf.org/penta/submission/dc13/person


See you in Switzerland,
    DebConf Team

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

WHERE = os.environ.get('WHERE', 'dcvn.attend and dcvn.debcampdc13')
People = dc_objs.get_people(where=WHERE)
#[ P.add_errors(Errors) for P in People ]
mbox = mailbox.mbox("dc13debcamp.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    # This part deals with mailing.

    subject_extra = ""
    if not person.reconfirmed:
	subject_extra += ", please reconfirm NOW"

    # Body = body%locals()
    Body = body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="Reminder: DebCamp will be held from Tuesday 6 August to Saturday 10 August",
        Body=Body,
        extraMsgid="dc13debcamp",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


print
