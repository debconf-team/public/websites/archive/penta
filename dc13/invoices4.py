# Giacomo Catenazzi, July 2013

# formail -s /usr/sbin/sendmail -ti < dc13invoices.mbox

import csv
import mailbox
from textwrap import dedent

import emailutil
from emailutil import Email, Message

from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders

body_first = """\
Hello %(name)s,%(extra_notice)s

Please find a link to your invoice below:
<%(invoice_url)s>

You can pay with Paypal on the link provided or do a bank transfer to
our account which is also listed on the invoice page.

If you live in the UK or an other supported country
you could look at http://transferwise.com/ as an
alternative, cheaper transfer option.

Here are our bank details (don't forget to add you reference number
which is on the invoice):
Reference: %(invoice_nr)s
Recipient E-mail: info@debconf13.ch
Recipient Name/Address: DebConf13 association, 8400 Winterthur, Switzerland 
Bank: Swiss Post, Postfinance, 3030 Bern, Switzerland 
BIC: POFICHBEXXX 
IBAN: CH53 0900 0000 6019 5325 6

If you have any questions, please contact us via
IRC:
  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body_reminder = """\
Hello %(name)s,%(extra_notice)s

We haven't received your payment for DebConf13 yet. Perhaps it is
still "in transit". Otherwise, we kindly remind you to pay your
invoice within the next few days.  Please find a link to your invoice
below:
<%(invoice_url)s>.

** Concretely, we request that you PLEASE pay by Wednesday, 31 July,
or contact registration@debconf.org if you cannot send the payment by
then. Failing this, your room could be reallocated. If you no longer
intend to attend, please send us a notice (registration@debconf.org) **.

Please do a bank transfer to our account as described below (and
which is also listed on the invoice page) or pay using Paypal with
the link provided.

If you live in the UK or an other supported country
you could look at http://transferwise.com/ as an
alternative, cheaper transfer option.

Here are our bank details (don't forget to add you reference number
which is on the invoice):
Reference: %(invoice_nr)s
Recipient E-mail: info@debconf13.ch
Recipient Name/Address: DebConf13 association, 8400 Winterthur, Switzerland
Bank: Swiss Post, Postfinance, 3030 Bern, Switzerland
BIC: POFICHBEXXX
IBAN: CH53 0900 0000 6019 5325 6

If you have any questions, please contact us via
IRC:
  #debconf-team @ irc.oftc.net   (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

mbox = mailbox.mbox("dc13invoices4.mbox")
mbox.clear() ; mbox.flush()

already_sent = set([])
for sent_payments in ['./hug.invoice/attendee_payments_v1.csv', './hug.invoice/attendee_payments_v2.csv', './hug.invoice/attendee_payments_v3.csv']:
  header_line = True
  with open(sent_payments, 'rb') as csvfile:
    for line in csv.reader(csvfile):
      if not line:
          continue
      if header_line:
          header_line = False
          continue
      person_id, name, email, invoice_nr, invoice_total, invoice_paid, invoice_url = line
      name = name.replace("_", " ")
      person_id = int('0'+person_id)
      already_sent.add(person_id)


header_line = True
with open('./hug.invoice/attendee_payments.csv', 'rb') as csvfile:
  for line in csv.reader(csvfile):
    if not line:
        continue
    if header_line:
	header_line = False
	continue

    person_id, name, email, invoice_nr, invoice_total, invoice_paid, invoice_url = line
    person_id = int('0' + person_id)

    invoice_total = float(invoice_total)
    invoice_paid = float(invoice_paid)  
    if invoice_paid != invoice_total:
        print "%4d,%4.2f,%4.2f,%4.2f,%s" % (person_id, invoice_total, invoice_paid, invoice_total-invoice_paid, name)
    if invoice_total <= invoice_paid:
        # no need to send a mail
	continue
    name = name.replace("_", " ")
    if person_id == 180:
	continue
    if invoice_total-invoice_paid < 100:
	extra_notice = "\n\n** Small amount notice: You can also pay the missing %2d CHF on site.\n" % (invoice_total-invoice_paid)
    else:
	extra_notice=""

    if person_id in already_sent:
	Body = body_reminder%locals()
        subject = "DebConf13 - payment reminder [%d]" % person_id
    else:
        Body = body_first%locals()
        subject = "DebConf13 attendee invoice [%d]" % person_id

    msg = emailutil.Message( # emailutil.MessageWithAttachments(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(name, email),
        Subject=subject,
        Body=Body,
        extraMsgid="dc13invoice4",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    # if we have an attachment
    # part = MIMEBase('application', "pdf")
    # part.set_payload( open(filename,"rb").read() )
    # Encoders.encode_base64(part)
    # part.add_header('Content-Disposition', 'attachment; filename="%s"' % filename)
    # msg.attach(part)

    mbox.add(str(msg))

mbox.flush()

