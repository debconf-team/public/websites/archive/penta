# Richard Darst, April 2010

import os
import mailbox
from textwrap import fill

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


beginning = """\
Dear %(name)s,

You selected either "self-paid camping" or "I prefer camping over
communal accommodation" in your accommodation preferences for DebConf13.
We are currently allocating camping spaces to everyone that would like
to camp. We are happy to provide you with camping space if you agree
to the arrangements outlined below:

Camping at DebConf:
* You must bring your own tent. We cannot provide any camping gear.
* You can (and should) use the toilets and showers of the building
  next to your camping space.
* We have about 16m2 available per tent, including the space around
  your tent. If you need more space for your tent, please inform us.
* Eating (breakfast, lunch and dinner) will be provided at Le Camp for
  you, as for all other attendees. So there is no need to bring any
  cooking gear.
* Camping will be in 2-3 small camping areas with 5-10 tents each.
  Each camping area will be associated with an accommodation building
  for bathroom facilities.

Things you will need to bring for camping (in addition to the usual
stuff you need for DebConf):
* tent
* sleeping-bag
* mattress
* towel
"""

upgrade_offer_sponsored = """\
As an alternative to camping, we still have upgrades to a 4 person
room for sponsored attendees. This costs CHF 20/day. If you'd rather
stay in a 8-bed room with your friends, they're available for the same
fee. Both upgrade options include bedding. You can of course also
change to communal accommodation. If you want to change your
accommodation option, please mail registration@debconf.org. You can
find more information about the available accommodation options at
https://wiki.debconf.org/wiki/DebConf13/Accommodation.
"""

upgrade_offer_8bed = """\
As an alternative to camping we do still have some smaller rooms left
if you'd like to upgrade. The <8-bed rooms cost CHF10/day more than
what you are paying now. If you'd prefer a 4-bed room, please mention
this in the comment field and we'll try to put you into a 4-bed rooms
if available. You can also change to communal accommodation at no
additional costs. If you want to change your accommodation option,
please mail registration@debconf.org. You can find more information
about the available accommodation options at
https://wiki.debconf.org/wiki/DebConf13/Accommodation.

You should already have received an invoice for your campsite.
Unfortunately the invoices says "communal accommodation". This is
because both options have the same price and we only use one category
for billing purposes. In your case this is the invoice for camping.
"""

information_needed = """\
Please answer the following questions so we can complete the camping 
allocation:
* Are you coming with a tent, or with a camper van or trailer?
* Approximately how large is your tent, camper van or trailer?
* Are you staying alone, or are you sharing your tent/camper van
  with someone? All persons should register for camping.

Please add this information to the room preference field in the
registration system as soon as possible. We already have the following
comments from you:

------
%(room_preference)s
------

The registration system URL is
 https://penta.debconf.org/penta/submission/dc13/person
"""

ending = """\
If you have any questions or concerns, please don't hesitate to contact us at:
  IRC:          #debconf-team @ irc.oftc.net   (real-time questions)
  or mailing list:      debconf-team@lists.debconf.org (public list)
  or non-public email:  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body = """%(beginning)s
%(upgrade_offer)s
%(information_needed)s
%(ending)s
"""


# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
WHERE = os.environ.get('WHERE', 'dcvn.attend and dcvn.reconfirmed and (dccp.accom_id=24 OR (dccp.accom_id=22 AND dccp.camping=true))')
People = dc_objs.get_people(where=WHERE)
mbox = mailbox.mbox("dc13camping-first.mbox")
mbox.clear() ; mbox.flush()

for person in People:

    upgrade_offer = person.accom.startswith('self-paid camping') and upgrade_offer_8bed or upgrade_offer_sponsored
    Body = body%locals()
    Body = Body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="Camping at DebConf13 [%d]" % person.person_id,
        Body=Body,
        extraMsgid="dc13camping-first",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


