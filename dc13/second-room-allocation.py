# Richard Darst, April 2010

import os
import sys
import datetime
import csv
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


beginning = """\
Hello %(name)s,

This (automated) message is being sent to give you some information
about your room at DebConf.

%(accommodation)s
"""


reconfirm_warning = """

** You must reconfirm and provide the required information by 30 June
** or your registration will be canceled.  Please don't wait and do it
** TODAY."""

error_warning = """

** You have some errors, please fix your registration
** details. (see at the bottom of this mail)
"""

if_request_sponsorship = """\

We are sorry not to have finished sponsorship review earlier, but we
promise you to review your sponsorship status by Sunday 30 June.  For
this reason we postpone the reconfirmation deadline to Sunday 7 July.
You can also already reconfirm now, and in case of non-granted
sponsorship, you still have the possibility to change your attendence
status or change the attendence days.

"""

if_need_to_pay = """\

We will send the invoice within next two weeks.  If you have questions
about the invoice or payment options, check out
<http://debconf13.debconf.org/payments.xhtml> or please contact
registration@debconf.org .

"""


ending = """\
Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body = """%(beginning)s
%(ending)s
"""

# Get our list of errors:
Errors = errorcheck.Errors
# this don't work: Errors have not yet a name (not yet instantiated)
#Errors = [e for e in Errors
#          if e.__class__.__name__ not in ('ThirteenYearOldAgeLimit',
#                                          'DoubleCheckTravelSponsorshipNumbers',
#                                          'NewlyEnabledFieldsInfo',
#					  'NoRoomPreference') ]
Errors = [e for e in Errors
          if 'travel' not in e._class ]
#errorcheck.printErrors(Errors)
#print
#print

# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
WHERE = os.environ.get('WHERE', 'dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)
get_people_by_id = dc_objs.get_people_by_id
[ P.add_errors(Errors) for P in People ]
People = [ P for P in People if len(P.errors) > 0 ]
mbox = mailbox.mbox("dc13second_room_allocation.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


### ROOMS ###
#============

def parse_date(x):
    dt = datetime.datetime.strptime(x, '%Y-%m-%d')
    return dt.date()

# persons[id] -> [ (hotel, room, arrive, depart) ]
room_persons = {}
room_person_name = {}
# rooms[hotel+":"+room] -> [ (person_id, arrive, depart) ]
rooms = {}


def parse_spreadsheet(fname):
    import readsxc
    f = readsxc.OOSpreadData(fname)

    names = None
    numpeople = 0
    for row in f:
        # Automatically create columns
        if not names:
            names = [ n.lower().replace(' ', '_').replace('-', '_')
                      for n in row ]
            continue
        if not row or row[0].lower() == "end" or (not row[0] and not row[1]):
            break
        numpeople += 1
        # Row is a dict of row labels
        row = dict(zip(names, row))
        #print row
        #print row['person_id']

        # Ignore empty names (free bed)
        if not row.has_key('name') and not row.has_key('person_id'):
            continue
        person_id = int(row['person_id'])

        if not room_persons.has_key(person_id):
            room_persons[person_id] = []
            room_person_name[person_id] = row['name']

        # Get room information
        roomName = row['room']  # row['room_alloc']
        if roomName.startswith('n--') and person_id != 34:
            continue

        roomNameCamp = row['roomcamp']

        hotelName = "DebConf"
        hotelNameCamp = "DebCamp"

        arrive = parse_date(row['arrival_date'])
        depart = parse_date(row['departure_date'])
        
        if roomNameCamp:
            room_persons[person_id].append( (hotelNameCamp, roomNameCamp, arrive, min(dc_objs.DEBCONF_START, depart)) )
            if not rooms.has_key(hotelNameCamp + ":" + roomNameCamp):
                rooms[hotelNameCamp + ":" + roomNameCamp] = []
            rooms[hotelNameCamp + ":" + roomNameCamp].append( (person_id, arrive, min(dc_objs.DEBCONF_EARLIEST_ARRIVAL, depart)) )

        room_persons[person_id].append( (hotelName, roomName, max(arrive, dc_objs.DEBCONF_EARLIEST_ARRIVAL), depart) )
        if not rooms.has_key(hotelName + ":" + roomName):
            rooms[hotelName + ":" + roomName] = []
        rooms[hotelName + ":" + roomName].append( (person_id, max(arrive, dc_objs.DEBCONF_EARLIEST_ARRIVAL), depart) )

    print "People processed:", numpeople
    return

parse_spreadsheet(sys.argv[1])

room_descr = {}
def code_to_description():
    for line in open("cate.penta/dc13/room.names").readlines():
        hotel, code, building, floor, room_number, size, comment = line.strip().split(None, 6)
        room_descr[hotel + "-" + code] = (hotel, code, building, floor, room_number, size, comment)
code_to_description()

for person in People:
    # This parts deals with printing.

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    AllErrors = person.errors
    AllErrors = [e for e in AllErrors
              if e.__class__.__name__ not in ('ThirteenYearOldAgeLimit',
                                              'DoubleCheckTravelSponsorshipNumbers',
                                              'NewlyEnabledFieldsInfo',
					      'NoRoomPreference',
					      'NoCountry') ]
    AllErrors = [e for e in AllErrors
              if 'travel' not in e._class ]
    person.errors = AllErrors

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    no_errors = False
    if not errors.strip():
	no_errors = True
	errors = "    (none)"

    person_id = person.person_id
    if person_id == 2609:
	continue
    if person.accom_no:
        person.accommodation = "No accommodation provided."
	continue
    else:
	print room_persons[person_id]
        person.accommodation = ""
        for hotel, room, arrive, depart in room_persons[person_id]:
	    if hotel != "DebConf":
		continue
	    #hotel_, code, building, floor, room_number, size, comment = room_descr[hotel + "-" + room]

            hotel = hotel.encode('ascii', 'ignore')
            room = room.encode('ascii', 'ignore')
	    if room[0] == "t" or room[0] == "n":
		continue

            hotel_, code, building, floor, room_number, size, comment = room_descr[hotel + "-" + room]

            hr = hotel + ":" + room
            from_date = "from %s to %s (in room %s in %s)" % (arrive.strftime("%d %B"), depart.strftime("%d %B"), room_number, building)
            roomates = ""
            for roommate_id, rm_arrive, rm_depart in rooms[hr]:
		if get_people_by_id.has_key(roommate_id):
		    roommate_name = get_people_by_id[roommate_id].name
		else:
		    roommate_name = room_person_name[roommate_id].encode('ascii', 'ignore')
                roomates += "      %s (%s - %s)\n" % (roommate_name, rm_arrive, rm_depart)
            if room[0] == "s":
                person.accommodation += """\
You get a room %s.  You are required to bring a sleeping bag and a towel.
Your roommates will be:\n%s\n""" % (from_date, roomates)
            elif room[0] == "b":
               person.accommodation += """\
You get a room %s. You are required to bring a towel (bedding/linen are
provided). Your roommates will be:\n%s\n""" % (from_date, roomates)
            elif room[0] == "t":
                person.accommodation += """\
You get a place for tent / van %s. You are required to bring a towel, we
will provide you with WCs, showers and breakfast. On arrival ask front
desk where to put your tent or van. Don't just put your tent somewhere
without talking to us first.\n\n""" % from_date

    if not person.accommodation:
	continue    

    Body = body%locals()
    Body = Body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf13: Room allocation [%d]" % person.person_id,
        Body=Body,
        extraMsgid="dc13second_room_allocation",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


errorcheck.printPeopleErrors(People)
print
print
errorcheck.printErrorsByClass(People)
