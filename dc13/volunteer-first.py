# Richard Darst, April 2010

import os
import mailbox
from textwrap import fill

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


body = """\
Hi %(name)s,

You have registered for DebConf13 either as a volunteer or as an
organizer. As the conference is approaching fast, we'd like to start
coordination of various volunteer efforts.

For the following teams we need volunteers during the whole week:
* Bar
* Front-Desk
* Talkmeister
* Videoteam

Feel free to add your name to any listed team[0], but be aware that
each of the above-listed teams need a certain amount of training and
introduction, so we would like you to focus on only one of those teams
for the whole week.

We are especially looking for volunteers to help with Front Desk on
Arrival and Departure Day. To ensure enough volunteers on Arrival Day,
we will offer free accommodations from Friday to Saturday night and
free dinner on Friday for those arriving early to help.

We also need volunteers for dishwashing in the evenings. As this is
probably at task nobody would like to do for a whole week and as it
does not need a lot of training, we would like to spread to load among
as many people as possible for this. Please add your name if you are
willing to help on at least one shift.

We will certainly also need volunteers for various other tasks during
the week. We'll issue other calls as soon as we know more. So stay
tuned.

Coordination is done through a wiki page. Please, add your name on the following page:
[0] https://wiki.debconf.org/wiki/DebConf13/VolunteerCoordination

If you have any questions, please feel free to contact us at
debconf-team@lists.debconf.org or on IRC at
#debconf-team@irc.debian.org.

Thanks for volunteering at DebConf13,
Gaudenz
"""


# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
WHERE = os.environ.get('WHERE', 'dcvn.attend and dcvn.reconfirmed and (dcvn.debconf_role IN (\'Volunteer\', \'Organizer\'))')
People = dc_objs.get_people(where=WHERE)
mbox = mailbox.mbox("dc13volunteer-first.mbox")
mbox.clear() ; mbox.flush()

for person in People:

    Body = body%person.__dict__

    msg = emailutil.Message(
        From=Email("Gaudenz Steinlin", "gaudenz@debconf.org"),
        To=Email(person.name, person.email),
        Subject="Volunteering at DebConf13 [%d]" % person.person_id,
        Body=Body,
        extraMsgid="dc13volunteer-first",
        ReplyTo=Email("DebConf Team", "debconf-team@lists.debconf.org"),
        Cc=Email("Gaudenz Steinlin", "gaudenz@debian.org"),
        )
    mbox.add(str(msg))


