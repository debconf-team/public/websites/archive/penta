# Richard Darst, April 2010

import csv
import os
import sys
import time

import dc_objs
import dc_util

WHERE = os.environ.get('WHERE', 'dcvn.attend AND dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)

def formatnum(x, fmt, width=7):
    if x is None:
        return " "*len(fmt%0)
    else:
        return fmt%x

def _print_people(People):
    for P in People:
        amount_paid = 0 # P.amount_paid
        amount_to_pay = P.total_costs # P.amount_to_pay
        print P.regSummary(), \
		"  ", P.personHeader()
#              formatnum(amount_paid, "%7.2f") if not P.promised_to_pay else "   P   ", \
#              formatnum(amount_to_pay, "%7s"), \
#              "  ", P.personHeader()


#People = [p for p in People if
#          p.cat_spons or p.cat_pro or p.cat_cor) ]
#People = [ p for p in People if p.accom_yes ]

People.sort(key=lambda p: p.last_name)

_print_people(People)

lang={}

import pentaconnect
db = pentaconnect.get_conn()
c = db.cursor()

query = """SELECT person_id, array_agg(language)
FROM person_language
GROUP BY person_id
"""
c.execute(query)
for row in c.fetchall():
    id, l = row
    lang[id] = l[1:-1].replace(",", " ")

if len(sys.argv) > 1:
    fname = sys.argv[1]
else:
    fname = "reg-data.csv"
f = open(fname, "w")
csvfile = csv.writer(f) #,csv.Dialect.quoting)

csvfile.writerow(('person_id',
                   'name',
		   'nickname',
		   'debconf_role',
                   'food',
		   'food_select',
		   'language',
                   ))


# people with "yes"
for person in People:
    if   person.food_reg:   food = ""
    elif person.food_veg:   food = "veg"
    elif person.food_vegan: food = "vegan"
    elif person.food_other: food = "other"
    else: food = ""

    if person.food_no:
        food2="no"
    else:
	food2="yes"

    csvfile.writerow((person.person_id,
                      person.name,
		      person.nickname,
		      person.debconf_role,
                      food,
		      food2,
		      lang.get(person.person_id, "")
                      ))


