# Richard Darst, April 2010

import os
import sys
import datetime
import csv
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message

first_date = dc_objs.DEBCAMP_START
arrival_day = dc_objs.DEBCONF_EARLIEST_ARRIVAL
last_date = dc_objs.DEBCONF_END


Errors = errorcheck.Errors
Errors = [e for e in Errors
          if 'travel' not in e._class ]

# Make the initial people objects, and add errors to them.
WHERE = os.environ.get('WHERE', 'dcvn.arrived')
People = dc_objs.get_people(where=WHERE)
get_people_by_id = dc_objs.get_people_by_id
[ P.add_errors(Errors) for P in People ]

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


### ROOMS ###
#============

def parse_date(x):
    dt = datetime.datetime.strptime(x, '%Y-%m-%d')
    return dt.date()

# persons[id] -> [ (hotel, room, arrive, depart) ]
room_persons = {}
room_person_name = {}
# rooms[hotel+":"+room] -> [ (person_id, arrive, depart) ]
rooms = {}


def parse_spreadsheet(fname):
    import readsxc
    f = readsxc.OOSpreadData(fname)

    names = None
    numpeople = 0
    for row in f:
        # Automatically create columns
        if not names:
            names = [ n.lower().replace(' ', '_').replace('-', '_')
                      for n in row ]
            continue
        if not row or row[0].lower() == "end" or (not row[0] and not row[1]):
            break
        numpeople += 1
        # Row is a dict of row labels
        row = dict(zip(names, row))
        #print row
        #print row['person_id']

        # Ignore empty names (free bed)
        if not row.has_key('name') and not row.has_key('person_id'):
            continue
        person_id = int(row['person_id'])

        if not room_persons.has_key(person_id):
            room_persons[person_id] = []
            room_person_name[person_id] = row['name']

        # Get room information
        roomName = row['room']  # row['room_alloc']
        if roomName.startswith('n--') and person_id != 34:
            continue

        roomNameCamp = row['roomcamp']

        hotelName = "DebConf"
        hotelNameCamp = "DebCamp"

        arrive = parse_date(row['arrival_date'])
        depart = parse_date(row['departure_date'])
        
        if roomNameCamp:
            room_persons[person_id].append( (hotelNameCamp, roomNameCamp, arrive, min(dc_objs.DEBCONF_START, depart)) )
            if not rooms.has_key(hotelNameCamp + ":" + roomNameCamp):
                rooms[hotelNameCamp + ":" + roomNameCamp] = []
            rooms[hotelNameCamp + ":" + roomNameCamp].append( (person_id, arrive, min(dc_objs.DEBCONF_EARLIEST_ARRIVAL, depart)) )

        room_persons[person_id].append( (hotelName, roomName, max(arrive, dc_objs.DEBCONF_EARLIEST_ARRIVAL), depart) )
        if not rooms.has_key(hotelName + ":" + roomName):
            rooms[hotelName + ":" + roomName] = []
        rooms[hotelName + ":" + roomName].append( (person_id, max(arrive, dc_objs.DEBCONF_EARLIEST_ARRIVAL), depart) )

    print "People processed:", numpeople
    return

parse_spreadsheet(sys.argv[1])

room_descr = {}
def code_to_description():
    for line in open("cate.penta/dc13/room.names").readlines():
        hotel, code, building, floor, room_number, size, comment = line.strip().split(None, 6)
        room_descr[hotel + "-" + code] = (hotel, code, building, floor, room_number, size, comment)
code_to_description()


rooms_b = {}
rooms_z = {}
rooms_s = {}
rooms_t = {}
date = first_date
while True:
    if date > last_date:
        break
    rooms_b[date] = 0
    rooms_z[date] = 0
    rooms_s[date] = 0
    rooms_t[date] = 0
    date += datetime.timedelta(days=1)


for person in People:
    # This parts deals with printing.

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    AllErrors = person.errors

    person_id = person.person_id
    if person.accom_no or person.person_id in set((426,2609)):
	continue
    else:
        
	person.accommodation = ""
        for hotel, room, arrive, depart in room_persons[person_id]:

            hotel = hotel.encode('ascii', 'ignore')
            room = room.encode('ascii', 'ignore')
	    if room[0] == "n":
		continue

            hotel_, code, building, floor, room_number, size, comment = room_descr.get(hotel + "-" + room, (hotel, room, 'camping', '???', '???', '???', '???'))

            hr = hotel + ":" + room

            date = first_date
            while True: 
                if date >= arrival_day:
                    break
		if hotel == "DebCamp"  and  (person.effective_dates()[0] <= date < person.effective_dates()[1]):
		    if room[0] == 'b' and not building == 'Zwingli':  
                        rooms_b[date] += 1
		    elif room[0] == 'b':
                        rooms_z[date] += 1
		    elif room[0] == 's':
                        rooms_s[date] += 1
		    elif room[0] == 't':
		        rooms_t[date] += 1

                date += datetime.timedelta(days=1)

            while True:
                if date > last_date:
                    break
                if hotel == "DebConf"  and  (person.effective_dates()[0] <= date < person.effective_dates()[1]):
                    if room[0] == 'b' and not building == 'Zwingli':
                        rooms_b[date] += 1
                    elif room[0] == 'b':
                        rooms_z[date] += 1
                    elif room[0] == 's':
                        rooms_s[date] += 1
                    elif room[0] == 't':
                        rooms_t[date] += 1

                date += datetime.timedelta(days=1)


print "%15s   linen   Zwingli  sl.bag   tent" % ""
date = first_date
while True:
    if date > last_date:
        break

    print "%15s %5i   %5i   %5i   %5i" % (date.strftime('%F(%a)'), rooms_b[date], rooms_z[date], rooms_s[date], rooms_t[date])
    date += datetime.timedelta(days=1)


