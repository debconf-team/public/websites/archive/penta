# Richard Darst, April 2010

import os
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message

Subject="Debcamp: room cleaning and room change" 

body = """\

Hello,

DebCamp is nearly finished.  By now things are going well, thank you!
For DebConf we are expecting many more attendees, and all buildings
will be available to us, including a new bar, new main hacklab, new
front desk, etc.

Tomorrow morning, many of you will be moving room, and we will also be
changing the bedding and pillow covers.
By 10am on Saturday, you should remove used bedding, pillow covers,
etc. from your beds, and put them the corridors.  Volunteers will
collect and replace them.  You should also put the room's waste bin in
the corridor (please recycle PET, paper and glass).

Tonight you will receive a mail with the name and the location of your
DebConf room.

PS: If you are camping, please ignore this mail.

Thank you
     DebConf team 
"""

WHERE = os.environ.get('WHERE', 'dcvn.attend AND dcvn.reconfirmed')
People = dc_objs.get_people(where=WHERE)
#[ P.add_errors(Errors) for P in People ]
mbox = mailbox.mbox("dc13enddebcamp.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.
    AllErrors = person.errors

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    if not errors.strip(): errors = "    (none)"

    if person.debcamp_yes:
        Body = body % person.__dict__
        subject = "Debcamp: room cleaning and room change [%d]" % person.person_id
    else:
	continue

    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject=subject,
        Body=Body,
        extraMsgid="dc13enddebcamp",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


print
