# Richard Darst, May 2010

import os
import mailbox
from textwrap import dedent

import dc_objs
import emailutil
from emailutil import Email, Message

body = """\
Hello %(name)s,

%(items)s

The registration system URL is
  https://penta.debconf.org/penta/submission/dc13/person

If you have any questions, please contact us via IRC:
  #debconf-team @ irc.oftc.net (real-time questions)
or mailing list:
  debconf-team@lists.debconf.org (publically archived mail list)
or non-public email:
  registration@debconf.org

Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

# Each of these is a pair: (message_text, condition_for_text_to_be_relevant)
classes = {
    # Case A: grant "food+accom"
    "A":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 13 in Le Camp, Switzerland!

You have also been approved for food sponsorship.""",
         lambda p: ((p.accom_sponsored or p.accom_upgrade) and p.food_select_spons and not p.debcamp_yes)
         ),
    # Case B: grant "food+accom also for DebCamp"
    "B":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 13 and DebCamp in Le Camp, Switzerland!

You have also been approved for food sponsorship.""",
         lambda p: ((p.accom_sponsored or p.accom_upgrade) and p.food_select_spons and p.debcamp_yes)
         ),

    # Case C: asked only food, got it
    "C":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored food for the upcoming
DebConf 13 and in Le Camp, Switzerland!""",
         lambda p: (p.food_select_spons and not p.debcamp_yes)
         ),
    # Case E: asked only food, got it + DebCamp
    "E":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored food for the upcoming
DebConf 13 and DebCamp in Le Camp, Switzerland!""",
         lambda p: (p.food_select_spons and p.debcamp_yes)
         ),
    # Case D: asked only accom, got it
    "D":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 13 and DebCamp in Le Camp, Switzerland!""",
         lambda p: ((p.accom_sponsored or p.accom_upgrade) and not p.debcamp_yes)
         ),
    # Case F: asked only accom, got it + DebCamp
    "F":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 13 and DebCamp in Le Camp, Switzerland!""",
         lambda p: ((p.accom_sponsored or p.accom_upgrade) and p.debcamp_yes)
         ),
    # Case H: yes debcamp, not sponsored
    "H":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to approve your DebCamp plan, for the 
upcoming DebConf 13 and DebCamp in Le Camp, Switzerland!""",
         lambda p: (not (p.accom_sponsored or p.accom_upgrade) and p.debcamp_yes)
         ),
     # Case I: yes debconf, no debcamp
   "I":("""\
We are happy to inform you that after reviewing the information you
provided, we are able to grant you sponsored accommodation for the
upcoming DebConf 13 in Le Camp, Switzerland!

You have also been approved for food sponsorship during DebConf
itself.

We are sorry to say that we can not grant you the requested
sponsored food and accommodation during DebCamp which precedes DebConf
itself.""",
         lambda p: (p.debcamp_yes)
         ),
    # Case Z: denied
    "Z":("""\
We are sorry to inform you that after reviewing the information you
provided, we are not able to grant you sponsored food and accommodation
for the upcoming DebConf 13 in Le Camp, Switzerland.

We do hope you still will be able to attend DebConf. If this is still
the case, please go to your pentabarf page (link below) and update the
\"Category\" field with the correct information (if you will get
accommodation and food on your own, or if you will pay and stay with
others attendees in selected hotels)""",
         lambda p: (p.cat_spons) ),

    
    }

WHERE = os.environ.get('WHERE', 'dcvn.attend')
people = dc_objs.get_people(where=WHERE)
mbox = mailbox.mbox("dc13-sponsorstatus.mbox")
mbox.clear() ; mbox.flush()

# defines person_classes: A dictionary that maps Class -> penta_ids in
# that class.  Classes define different types of sponsorship
# requested/granted.
person_classes = dict((c, set()) for c in 'ABCDEFGHIZ?')
for line in open("dc13/sponsorship-status.dat"):
    if not line.strip(): continue
    c, p_id = line.split()
    c = ( c + "?")[0]
    p_id = int(p_id)
    person_classes[c].add(p_id)

people_processed = set()

for person in people:
    # skip unsponsored and prof/corp.
    items = [ ]
    for classname in classes:
        if person.person_id in person_classes[classname]:
            # Do we want to send this class of mails right now?
            if classname in ('G', '?'):
                continue

            items.append(classes[classname][0])
            # Check to make sure we are sending teh right messages.
            # This will print an error if we sent a message to a
            # person, but the message talks about sponsorhsip types
            # they did not request.  For example, sending an email
            # saying "You got no sponorship" to someone who did not
            # request sponsorship will print an error here.
            if not classes[classname][1](person):
                print "check", person.person_id, classname
    if len(items) > 1:
        print "Person is in too many classes:", person.person_id
    if len(items) == 0:
        continue
    people_processed.add(person.person_id)

    person.items = "\n\n".join(items)

    Body = body % person.__dict__
    
    msg = emailutil.Message(
        From=Email("DebConf Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf13: Sponsorship status",
        Body=Body,
        extraMsgid="dc13sponsorshipstatus1",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))

# This checks for people who are are in person_classes (they should
# get a message), but didn't actually get a message.  This usually
# means that get_people didn't return them - is conference_id wrong?
print "Checking for missed people:"
for classname in classes:
    if len(set(person_classes[classname]) - people_processed) > 0:
        print classname, "missed people", \
              set(person_classes[classname]) - people_processed

# Who did we not send an email to?
allPeople = set(p.person_id for p in people)
print "These people aren't in any class:", allPeople - (
    reduce(lambda x,y: x|y, person_classes.itervalues())
    | set(p.person_id for p in people if p.cat_unspons)
#    | set(p.person_id for p in people if p.cat_none)
    )
print "These people didn't get an email:", allPeople - people_processed

