# Richard Darst, April 2010

import os
import mailbox
from textwrap import dedent

import dc_objs
import errorcheck
import emailutil
from emailutil import Email, Message


beginning = """\
Hello %(name)s,%(mail_banner)s

This (automated) message is being sent to attendees to give you some
information about DebConf, to remind you about reconfirmation of your
attendance, and to let you know of any errors in your registration,
or other things which you should know about.  Please look over the
details below, and correct any problems which may be present in your
Penta account.  Some things are just warnings or notes, and won't
apply to most people. These are clearly labeled; if one doesn't apply
to you, please ignore it.

%(extra_news)sDebCamp will take place from Tuesday 6 August to Saturday 10 August,
in the same venue of DebConf. DebCamp is used for team meetings and
for important Debian related work.

It is possible to have room upgrades (into smaller room) for a daily
fee (thank you supporting Debian and DebConf).  Check it into (
<https://penta.debconf.org/penta/submission/dc13/person> ) or
contact us at registration@debconf.org. We will allocate the remaining
of the small rooms through a lottery.

Check your passport and travel information
( <http://debconf13.debconf.org/travel.xhtml> ).

It is still possible to submit talks and propose BoFs. (
<https://penta.debconf.org/penta/submission/dc13/person> and notify
talks@debconf.org of your presentation request )


Because this is an automated message, perhaps there are false
positives on the errors or warnings listed here.  If you know that
something does not apply to you or you have contacted us previously
regarding that error, please disregard.

If you have any questions, please contact us via
  IRC:          #debconf-team @ irc.oftc.net (real-time questions)
  or mailing list:      debconf-team@lists.debconf.org (public list)
  or non-public email:  registration@debconf.org
  also check the conference website: http://debconf13.debconf.org

The registration system URL is
  https://penta.debconf.org/penta/submission/dc13/person

Please check that everything is updated by the reconfirmation deadline
of Sunday 7 July, thus NOW!  Please see below for information
about food and lodging sponsorship.  Even if you aren't being
sponsored, please reconfirm, as registration estimates are required for
sponsorship, room allocation and budgeting planning."""

reconfirm_warning = """

** You must reconfirm and provide the required information by 30 June
** or your registration will be canceled.  Please don't wait and do it
** TODAY."""

error_warning = """

** You have some errors, please fix your registration
** details. (see at the bottom of this mail)
"""

if_request_sponsorship = """\

We are sorry not to have finished sponsorship review earlier, but we
promise you to review your sponsorship status by Sunday 30 June.  For
this reason we postpone the reconfirmation deadline to Sunday 7 July.
You can also already reconfirm now, and in case of non-granted
sponsorship, you still have the possibility to change your attendence
status or change the attendence days.

"""

if_need_to_pay = """\

We will send the invoice within next two weeks.  If you have questions
about the invoice or payment options, check out
<http://debconf13.debconf.org/payments.xhtml> or please contact
registration@debconf.org .

"""


ending = """\
Thank you,

-- 
The DebConf team
(person_id: %(person_id)4d)
"""

body = """%(beginning)s

Information about your registration
===================================

%(info)s

Registration errors and warnings
================================

%(errors)s


%(ending)s
"""

# Get our list of errors:
Errors = errorcheck.Errors
# this don't work: Errors have not yet a name (not yet instantiated)
#Errors = [e for e in Errors
#          if e.__class__.__name__ not in ('ThirteenYearOldAgeLimit',
#                                          'DoubleCheckTravelSponsorshipNumbers',
#                                          'NewlyEnabledFieldsInfo',
#					  'NoRoomPreference') ]
Errors = [e for e in Errors
          if 'travel' not in e._class ]
errorcheck.printErrors(Errors)
print
print

# Make the initial people objects, and add errors to them.
#people = dc_objs.get_people(where='dcvn.attend')
WHERE = os.environ.get('WHERE', 'dcvn.attend')
People = dc_objs.get_people(where=WHERE)
[ P.add_errors(Errors) for P in People ]
People = [ P for P in People if len(P.errors) > 0 ]
mbox = mailbox.mbox("dc13reconfirm-last.mbox")
mbox.clear() ; mbox.flush()

def make_items(messages):
    """Assemble a section out of the individual paragraphs.
    """
    messages = [e.email() for e in messages ]
    messages = [dedent(x).strip() for x in messages ]
    messages = "\n\n\n".join(messages)
    messages = [ "    "+line if line.strip() else ""
                 for line in messages.split("\n") ]
    messages = "\n".join(messages)
    return messages


for person in People:
    # This parts deals with printing.

    # hack - need to regen string messages first.
    [ e.email() for e in person.errors ]

    AllErrors = person.errors
    AllErrors = [e for e in AllErrors
              if e.__class__.__name__ not in ('ThirteenYearOldAgeLimit',
                                              'DoubleCheckTravelSponsorshipNumbers',
                                              'NewlyEnabledFieldsInfo',
					      'NoRoomPreference',
					      'NoCountry') ]
    AllErrors = [e for e in AllErrors
              if 'travel' not in e._class ]
    person.errors = AllErrors

    #if len(Errors) > 0:
    #    print '='*40
    #    print person.person_id, person.name
    #    print person._info()

    # This part deals with mailing.
    info   = [ e for e in AllErrors if e.level=='info' ]
    errors = [ e for e in AllErrors if e.level!='info' ]
    info   = make_items(info)
    errors = make_items(errors)
    no_errors = False
    if not errors.strip():
	no_errors = True
	errors = "    (none)"
	
    subject_extra = ""
    if not person.reconfirmed:
	subject_extra += ", please reconfirm NOW"

    person.mail_banner = ""
    if not person.reconfirmed:
        person.mail_banner += reconfirm_warning

    if not no_errors:
	person.mail_banner += error_warning
	if person.reconfirmed:
	    subject_extra += ", please fix your details"

    person.extra_news = ""
    if person.cat_spons:
	person.extra_news += if_request_sponsorship

    if person.total_costs > 0:
	person.extra_news += if_need_to_pay


    Body = body%locals()
    Body = Body%person.__dict__
    Body = Body.replace("Accomodation", "Accommodation")
    Body = Body.replace("accomodation", "accommodation")
    #print "=====\n"+Body

    msg = emailutil.Message(
        From=Email("DebConf Registration Team", "registration@debconf.org"),
        To=Email(person.name, person.email),
        Subject="DebConf13 updates and registration check, LAST CHANCE to change data" + subject_extra,
        Body=Body,
        extraMsgid="dc13reconfirm-last2",
        ReplyTo=Email("DebConf Registration Team", "registration@debconf.org"),
        Cc=Email("DebConf Registration Team", "registration@debconf.org"),
        )
    mbox.add(str(msg))


errorcheck.printPeopleErrors(People)
print
print
errorcheck.printErrorsByClass(People)
