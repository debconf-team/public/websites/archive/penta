# Richard Darst, 2009

import getpass
import os

# You need python-pygresql installed
import pgdb

# This module is designed to automate connections to the penta
# databases.  The problems here are: it might be non-local, and
# requires a password, and is different across all of our computers,
# and there are different databases (primary penta read-only, penta
# dev read-write, and perhaps other test ones).  To handle this, there
# are separate, local files that contain DBname, host:port, user, and
# password needed to log in.
#
# I (MrBeige) access penta via a ssh forwarded tunnel to the right
# machines.  Here are some hints:
# production penta instance:  pentabarf / skinner.debconf.org:5432
# dev instance:               pentabarf / cletus.debconf.org:5432
#
# Connection information is kept outside of the scripts, in separate
# files.
# For a given name, a list of paths is searched and the first file
# found is used.  A file looks like this (without leading pipes):
# | pentabarf
# | localhost:5432
# | rkd
# | PASSWORD
# where the first line is the postgres database name
# second line is the host and port
# third line is the postgres username. 'local' for local unix socket autentification
# fourth line is the postgres password
#
# In the scripts, there is a get_conn() function which returns the
# python DB API connector.  By default it looks for "penta", if you
# want to connect to the dev penta then change this to
# get_conn(name='pentatest') .

loginfiles = {
    # 'penta' is used if you want to connect to the primary penta database
    'penta': ['~/.pentalogin.txt',
              '~/proj/pentalogin.txt',
              # insert your files here.
              ],
    # 'pentatest' is for the dev instance of penta
    'pentatest': ['~/.pentalogintest.txt',
                  '~/proj/pentalogintest.txt',
                  ],
    'pentabeige': ['~/.pentaloginbeige.txt',
                  '~/proj/pentaloginbeige.txt',
                  ]
              }

def find_conn_info(name):
    if name not in loginfiles:
        raise Exception("Database name %s not found"%name)
    for f in loginfiles[name]:
        f = os.path.expanduser(f)
        if os.access(f, os.F_OK):
            data = open(f).read().split('\n')
            db, host, user, passwd = data[0:4]
            if passwd == '':
                passwd = getpass.getpass("Enter postgres password for %s: "%db)
            return db, host, user, passwd
    print "No connection information found for %s"%name
    return None, None, None, None

def get_conn(name="penta", db=None,host=None,user=None,passwd=None):
    db_, host_, user_, passwd_ = find_conn_info(name)
    if db     is None: db     = db_
    if host   is None: host   = host_
    if user   is None: user   = user_
    if passwd is None: passwd = passwd_
    if passwd is None:
        passwd = getpass.getpass("Enter postgres password for %s: "%db)

    if user == "local":
        conn = pgdb.connect(database=db)
    else:
	conn = pgdb.connect(database=db,host=host, user=user, password=passwd)
    return conn
